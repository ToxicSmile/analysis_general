%% Create "fake triggers" for baseline to get comparable cycles for stim and bs

% Adjust
clear; close all;

load('L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\Phase_analysis\Trigger_phases\BMS_stim_subj_pos_first_last_trig.mat');
num_Subj = dir('L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\Filtered_preprocessed\long_mat_files\');
savepath = 'L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\Phase_analysis\Trigger_phases\';
subjpath = 'L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\Filtered_preprocessed\long_mat_files\';
num_Subj(1:2)=[];
sheet = 'L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\pos_of_triggers_all.xls';


%% Execution

for s=1:length(num_Subj)

nSession = fieldnames(pos_first_trigger);
Session = cell2mat(nSession(s));
SessionEnd = Session(end);
SessionEnd = str2num(SessionEnd);

matpath = ([subjpath,num_Subj(s).name,'\Session' [num2str(Session(end))],'\longmat_average_ref_109Ele\']);
file = dir([matpath,'*.mat']);
load([matpath,file.name], 'vissymb' ,'artndxn');

vissymbbegin = vissymb(1:pos_first_trigger.(Session));

pattern = '2';
pattern2 = '3';
nN2 = strfind(vissymbbegin,pattern);
nN3 = strfind(vissymbbegin,pattern2);

N23 = numel(nN2)+numel(nN3);

nSession2 = fieldnames(pos_last_trigger_sound);
Session2 = cell2mat(nSession2(s));

cycle_length = (pos_last_trigger_sound.(Session2) - pos_first_trigger.(Session));

clear vis* artndxn fftot vissymbbegin nN2 nN3 vissymb

if SessionEnd == 2

Session(end)= '1';    
matpath = ([subjpath,num_Subj(s).name,'\Session' [num2str(Session(end))],'\longmat_average_ref_109Ele\']);
file = dir([matpath,'*.mat']);
load([matpath,file.name], 'vissymb' ,'artndxn');

nN2 = strfind(vissymb,pattern);
nN3 = strfind(vissymb,pattern2);

N23_bs = horzcat(nN2,nN3);
N23_bs = sort(N23_bs);

first_trig = N23_bs(N23);

cycle_length2 = first_trig + cycle_length;

first_trig_ps = ([ 'first_trigger_' Session(end-8:end)]);
pos_first_trigger_bs.(first_trig_ps) = first_trig; 

last_trig_sound_ps_bs = ([ 'last_trigger_sound_',Session(end-8:end)]);
pos_last_trigger_sound_bs.(last_trig_sound_ps_bs) = cycle_length2;

clear vis* artndxn fftot vissymbbegin nN2 nN3 vissymb first_trig cycle* N23 N23_bs 

else 

Session(end)= '2';    
matpath = ([subjpath,num_Subj(s).name,'\Session' [num2str(Session(end))],'\longmat_average_ref_109Ele\']);
file = dir([matpath,'*.mat']);
load([matpath,file.name], 'vissymb' ,'artndxn');

nN2 = strfind(vissymb,pattern);
nN3 = strfind(vissymb,pattern2);

N23_bs = horzcat(nN2,nN3);
N23_bs = sort(N23_bs);

first_trig = N23_bs(N23);

cycle_length2 = first_trig + cycle_length;

first_trig_ps = ([ 'first_trigger_' Session(end-8:end)]);
pos_first_trigger_bs.(first_trig_ps) = first_trig; 

last_trig_sound_ps_bs = ([ 'last_trigger_sound_',Session(end-8:end)]);
pos_last_trigger_sound_bs.(last_trig_sound_ps_bs) = cycle_length2;

clear vis* artndxn fftot vissymbbegin nN2 nN3 vissymb first_trig cycle* N23 N23_bs 
    
    

end
end

%% Create 2 trigger structures

for s=1:(length(num_Subj))
field2 = fieldnames(pos_first_trigger_bs);
fieldaim = field2(s);
fieldaim = cell2mat(fieldaim);
    
pos_first_trigger_all.(fieldaim) = pos_first_trigger_bs.(fieldaim);
clear fieldaim   
field = fieldnames(pos_first_trigger);
fieldaim = field(s);
fieldaim = cell2mat(fieldaim);
pos_first_trigger_all.(fieldaim) = pos_first_trigger.(fieldaim);
clear fieldaim    
   
end

pos_first_trigger_all = orderfields(pos_first_trigger_all);


for s=1:(length(num_Subj))
field = fieldnames(pos_last_trigger_sound);
field2 = fieldnames(pos_last_trigger_sound_bs);
fieldaim = field2(s);
fieldaim = cell2mat(fieldaim);
    
pos_last_trigger_sound_all.(fieldaim) = pos_last_trigger_sound_bs.(fieldaim);
clear fieldaim   

fieldaim = field(s);
fieldaim = cell2mat(fieldaim);
pos_last_trigger_sound_all.(fieldaim) = pos_last_trigger_sound.(fieldaim);
clear fieldaim    
   
end

pos_last_trigger_sound_all = orderfields(pos_last_trigger_sound_all);

%% Write to excel file

for s=1:(length(num_Subj)*2)
  
field = fieldnames(pos_first_trigger_all);
fieldaim = field(s);
fieldaim = cell2mat(fieldaim);

xlswrite(sheet,pos_first_trigger_all.(fieldaim),'Tabelle1', ['B' num2str(s+1)]);

field = fieldnames(pos_last_trigger_sound_all);
fieldaim = field(s);
fieldaim = cell2mat(fieldaim);
xlswrite(sheet,pos_last_trigger_sound_all.(fieldaim),'Tabelle1', ['C' num2str(s+1)]);


end

save([savepath,num_Subj(s).name(1:3),'_all_subj_pos_first_last_trig.mat'], 'pos_last_trigger_sound_all', 'pos_first_trigger_all');
    
   
    
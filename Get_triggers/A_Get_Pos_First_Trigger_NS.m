%% Get epoch of first trigger

%% Adjust

clear; close all;

trigger_adj_path = 'L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\Phase_analysis\art_corr\tPos_allCor\adj\';
trigger_adj_files = dir([trigger_adj_path,'*_tPos_allCor_adj.mat']);
trigger_pos = 'L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\Number_of_triggers.xlsx';

savepath = 'L:\Somnus-Data\Data01\BMS\adults\data\EGI\Niklas_Analysis\Phase_analysis\Trigger_phases\';

%% Execution

for s=1:length(trigger_adj_files)

load([trigger_adj_path,trigger_adj_files(s).name], 'tPos_allCor_adj');

last_trig_sound_pos = xlsread(trigger_pos,1,['C' num2str(s+2)]);

last_trig_sound = tPos_allCor_adj(last_trig_sound_pos);

last_trig_sound = last_trig_sound/500;
last_trig_sound_epoch = ceil(last_trig_sound/20);

first_trig = tPos_allCor_adj(1);
first_trig = first_trig/500;
first_trig_epoch = ceil(first_trig/20);


first_trig_ps = ([ 'first_trigger_',num2str(trigger_adj_files(s).name(7:15))]);
pos_first_trigger.(first_trig_ps) = first_trig_epoch; 

last_trig_sound_ps = ([ 'last_trigger_sound_',num2str(trigger_adj_files(s).name(7:15))]);
pos_last_trigger_sound.(last_trig_sound_ps) = last_trig_sound_epoch;

clear first_trig_epoch last_trig_sound_epoch last_trig_sound_pos last_trig_sound first_trig

end

%%

save([savepath,trigger_adj_files(s).name(7:9),'_stim_subj_pos_first_last_trig.mat'], 'pos_last_trigger_sound', 'pos_first_trigger');



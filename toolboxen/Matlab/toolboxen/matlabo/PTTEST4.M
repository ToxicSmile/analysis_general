% p=PTTest4(X,Y)
%
% Calculates paired t-Test (2-tailed) and returns probability.
%
% X and Y should have equal dimensions. First index enumerates observations
% belonging to the same test, second index enumerates different tests
% (e.g. 24x257 number of persons x number of frequency bins).
% Missing values are not handled for compatibility with Matlab 4
%
% Thomas 6/29/98, 4/19/99

function p=PTTest4(X,Y)

N=size(X,1);
for ii=1:size(X,2)
  dummy=cov([X(:,ii),Y(:,ii)]);
  SD(ii)=sqrt((dummy(1,1)+dummy(2,2)-2*dummy(1,2))/N);
end
T=mean(X-Y)./SD;
T=(N-1)./((N-1)+T.^2);
p=BETAINC(T,(N-1)/2,0.5);



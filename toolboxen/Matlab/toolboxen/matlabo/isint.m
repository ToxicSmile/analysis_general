function yn = isint(X)

% ISINT returns a vector/matrix with elements 0 or 1:
%       
%       0: the corresponding value is not integer
%       1: the corresponding value is integer
%_______________________________________________________
% Luca Finelli - 13.03.1997
% Last Update:   13.03.1997 lf - Nil

[m,n]=size(X);

DIFF    = abs(X-fix(X));
ind     = find(DIFF==0);
yn      = zeros(m,n);
yn(ind) = ones(1,length(ind));
function offs=getoffs(fname)
%GETOFFS	reads offset from file *.vis 
%
%
% 18.12.95,pa

fid=fopen(fname,'r');
line=fgets(fid);
offs=sscanf(line,'%d');
fclose(fid);

function [vistrack,vissymb,offs] = READTRAC(visfilename,track);

% READTRAC returns a matrix of binary numbers,  VISTRACK, 
%          where the j-th  element  of  the  i-th  column
%          (=i-th 20-s epoch) is 0, if the  corresponding
%          4-sepoch is included, 1 if it is discarded.
%          This selection refers to  the  selected  TRACK
%          (default: track 1) in  the VISFILENAME file.
%          VISFILENAME must be a string 'filename.vis'.
%          Optional output: VISSYMB, sleep stage scoring.
%          Optional Output: offs, offset obtained from getoffs(visname)
%
%          See also: READVIS
%________________________________________________________
% Luca Finelli - 02.05.1997

% Last Update:   08.05.1997 lf - Added track choice parameter
% Last Update:   24.03.1999 tg - Added offs as optional output

if nargin < 2
  track = 1;
end

% READ OFFSET
offs=getoffs(visfilename)

% READ SCORING FILE INFORMATION TO "visdata" (epochs+stages+tracks up to TRACK)
fidvis=fopen(visfilename,'r');
fgets(fidvis);
count=2+track;
i=1;
formatcode='%d%*c%c%*c';        % C format code to sscanf for 0 tracks 
for j=1:track
  formatcode=[formatcode '%c']; % changing C format code to read tracks
end                             % up to selected value (TRACK parameter)

while count>2
  visline=fgets(fidvis);
  [buffer,count]=sscanf(visline,formatcode,2+track);
  if count<2+track
   for j=count+1:2+track 
     buffer=[buffer;10]; 
   end
  end
  visdata(:,i)=buffer;
  i=i+1;
end;
fclose(fidvis);

% DECOMPRESSING SLEEP STAGES INFORMATION TO "vissymb"
[m n]=size(visdata);
for i=1:n-1                                % skipping last non-vis column (0 10 10 10)
   lower=visdata(1,i);
   higher=visdata(1,i+1);
   for k=lower:higher-1
     vissymb(k)=char(visdata(2,i));
   end;
   vissymb(visdata(1,n-1))=setstr(visdata(2,n-1));
end;
if offs~=0
  vissymb=[linspace('x','x',offs) vissymb]; % shifting epoch count from lights off
end                                         % to begin of file

% EXTRACTING 20-s EPOCHS WITH SOME EXCLUDED 4-s EPOCHS TO "excluded"
indexc=find(visdata(2+track,:)>=64 & visdata(2+track,:)<=95); % ASCII codes for exclusion is
                                                              % between 64 (none) and 95 (all)
excluded=visdata(1,indexc);

% CREATING MATRIX "vistrack" WITH 0 FOR INCLUDED 4-S EPOCH, 1 FOR EXCLUDED
%                 To row number corresponds 20-s epoch number in vis file
vistrack=zeros(length(vissymb),5);
for i=1:length(indexc)
  epochcode=dec2bin(visdata(2+track,indexc(i))-64); % HUMAN SCORING track code:
                                                    % visdata(2+track,indexc(i))
                                                    % Example: ASCII code value of X in "283 m X"    
                                                    % -> 88
  while length(epochcode)<5
    epochcode=[0,epochcode];                  % bringing binary version of track code
  end;                                        % to five bit length 

  vistrack(excluded(i)+offs,:)=epochcode(5:-1:1); % REVERSED! binary vector 
end;                                              % is 4-s excl.code 

disp(['Exclusion information concerns track ',num2str(track),'.'])
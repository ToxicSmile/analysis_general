%cohen's d, effect size
%rh, 11/05/06
%input:
%m1=mean first group, m2: mean second group
%s1=standard deviation first group, s2=standard deviation second group
%output:
%d=cohen's d, r=effect size

function [d r]=cohen(m1,m2,s1,s2);

d=(m1-m2)/sqrt((s1^2+s2^2)/2);

r=d/sqrt(d^2+4);


%%%%from Cohen (1988, pp. 21-23)
% Cohen's Standard  Effect Size  Percentile Standing  Percent of Nonoverlap 
%                      2.0           97.7               81.1% 
%                      1.9           97.1               79.4% 
%                      1.8           96.4               77.4% 
%                      1.7           95.5               75.4% 
%                      1.6           94.5               73.1% 
%                      1.5           93.3               70.7% 
%                      1.4           91.9               68.1% 
%                      1.3           90                 65.3% 
%                      1.2           88                 62.2% 
%                      1.1           86                 58.9% 
%                      1.0           84                 55.4% 
%                      0.9           82                 51.6% 
%     LARGE            0.8           79                 47.4% 
%                      0.7           76                 43.0% 
%                      0.6           73                 38.2% 
%     MEDIUM           0.5           69                 33.0% 
%                      0.4           66                 27.4% 
%                      0.3           62                 21.3% 
%     SMALL            0.2           58                 14.7% 
%                      0.1           54                  7.7% 
%                      0.0           50                    0% 

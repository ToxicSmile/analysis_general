function yy=yaxis(x,y)
%YAXIS set y axis
%
% 22:09 08.12.96, pa
% 21.04.99, tg

if nargin < 2
  y=x(2);
end
set(gca,'YLim',[x(1) y])


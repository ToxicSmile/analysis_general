% [Index,Aborted,Upper,Factor,NBins] = EXCLUDE(Low_f,High_f,FFT,InputNdx,Fact,NB,MaxEpoch,	,Txt,PlotAll,UpperIn);
%-----------------------------------------------------------------------------
% 
% Exclude artefacts based on a sliding mean of average power in a frequency band
%
% Arguments: Low_f,High_f: Limits for frequency band in Hz.
%            FFT: The frequency spectrum (0.25Hz resolution) (Frequency x Epochs)
%            InputNdx: An array with indices of epochs which are included in the calculation
%            Fact: Power < Sliding Mean * (1+Fact); initial value
%            NB: Nuber of bins for sliding mean; initial value
%            MaxEpoch: The number of epochs in FFT
%            VisNum: A numerical represantation of sleep stages (obtained with numvis)
%            Txt: A string used as title in Fig. 2
%            PlotAll: optional, 0: plot spectra of all epochs 1: plot envelop only, 2: no plots and no prompting
%            UpperIn: optional, upper limit for power, initial value  
%
%
% Output:    Index is an array with indices of epochs which are contained in InputNdx and
%            which have not been excluded in this routine.
%            Aborted is set to 1 if the user cancels the operation. In this case Index=InputNdx
%            Upper: Threshold for power
%            Factor: Sliding Mean + StdDev*Factor
%            NBins: Nuber of bins for sliding mean
%
%
% Th.G. 10.2.99; 20.2.99, pa; Th.G. 22.3.99
%-----------------------------------------------------------------------------
function [Index,Aborted,Upper,Factor,NBins] = EXCLUDE4s(Low_f,High_f,FFT,InputNdx,Fact,NB,MaxEpoch,VisNum,Txt,PlotAll,UpperIn);

if nargin<10
  PlotAll = 0;
end;

if size(InputNdx,1)<size(InputNdx,2)
	InputNdx=InputNdx';
end
t=(1:1:MaxEpoch*5)/15/60;
t20s=(1:1:MaxEpoch)/3/60;
f=0:0.25:(size(FFT,1)-1)*0.25;
RangeNdx=Low_f/0.25+1:1:High_f/0.25;

MeanSpectrum=mean(FFT(RangeNdx,:));
Upper=max(MeanSpectrum(InputNdx));
if nargin>=11
	Upper=UpperIn;  
end;  
Factor=Fact;
NBins=NB;
IsOk=3;

types = {'hi','factor','nbins'};
limit.hi=num2str(Upper);
Upper=str2num(limit.hi);

% positioning figures
if PlotAll<2
	bdwidth=5;
	topbdwidth=50;
	set(0,'Units','pixels')
	scnsize=get(0,'ScreenSize');
	pos1=[bdwidth,...
  	    scnsize(4)/3+bdwidth,...
    	  2/3*scnsize(3)-2*bdwidth,...
      	2/3*scnsize(4)-(topbdwidth+bdwidth)];
	pos2=[pos1(1)+2/3*scnsize(3),pos1(2),pos1(3)/2,pos1(4)];
	f1=figure('Position',pos1);
	zoom on;
	f2=figure('Position',pos2);
  zoom on;
end  

while IsOk>0
   Ndx1=find(MeanSpectrum<=Upper);
   WorkNdx=intersect(InputNdx,Ndx1');

   SlidingMean=zeros(1,MaxEpoch);
   Ndx1=WorkNdx;
   Range=floor(NBins/2);
   for i=1:size(WorkNdx,1),
		SlidingMean(WorkNdx(i))=mean(MeanSpectrum(WorkNdx(max(1,i-Range):min(size(WorkNdx,1),i+Range))));
		if (MeanSpectrum(WorkNdx(i))>=(1+Factor)*SlidingMean(WorkNdx(i)))
			Ndx1(i)=0;
		end;
	end
	Ndx1=intersect(Ndx1,WorkNdx);
  
  if PlotAll<2
	  figure(f1) 
     subplot(2,1,2); bar(t20s,VisNum);grid on;xlabel('Sleep stages');
	  xaxis([0 MaxEpoch/3/60]);
	  subplot(2,1,1); plot(t(Ndx1),MeanSpectrum(Ndx1));
		hold on
		temp=setxor(Ndx1,WorkNdx);
		plot(t(temp),MeanSpectrum(temp),'*m');
		plot(t(WorkNdx),SlidingMean(WorkNdx),'-r');
		xlabel([num2str(size(Ndx1,1)),'/',num2str(size(InputNdx,1)),' ',num2str(round(size(Ndx1,1)/size(InputNdx,1)*100)),'%']);
		title(['Range: ',num2str(Low_f),'-',num2str(High_f),' Hz']);
		grid on;
	  hold off;
	  xaxis([0 MaxEpoch/3/60])
   
	  figure(f2) 
	  subplot(211)
	  if PlotAll==1
	    semilogy(f,FFT(:,Ndx1))
      elseif length(Ndx1)>0
	    semilogy(f,mean(FFT(:,Ndx1)')',f,max(FFT(:,Ndx1)')',f,min(FFT(:,Ndx1)')')
	  end
	  xaxis([0 ((size(FFT,1)-1)*0.25)]);
	  grid;
		title(Txt)
	  subplot(212)
      if length(Ndx1)>0
          semilogy(f,mean(FFT(:,Ndx1)')',f,mean(FFT(:,InputNdx)')')
      end    
	  grid;
	  xaxis([0 ((size(FFT,1)-1)*0.25)]);
	  xlabel('frequency [Hz]')
	  ylabel('power [�V^2]')
	  dummy=get(gca,'YLim');
	  subplot(211);
	  set(gca,'YLim',dummy);
  
	  Toggle=1;
	  limit.hi=num2str(Upper);
	  Upper=str2num(limit.hi);
	  limit.factor=num2str(Factor);
	  limit.nbins=num2str(NBins);
	  while Toggle==1,
			prompt={'Upper limit (i.e. all higher values for power will be rejected):','Factor (i.e. Power < (1+Factor) * Sliding Mean):','Number of epochs used for sliding mean:'};
			titlestr='Input parameters for artefact removal';
			lines= 1;
			def={limit.hi,limit.factor,limit.nbins};
			[answer,Toggle]=excldlg(prompt,titlestr,lines,def,'on');
	    if Toggle==1
				limit=cell2struct(answer,types,1);
	      if PlotAll==1
	        PlotAll=0;
	      else
	        PlotAll=1;
	      end;
	      figure(f2) 
			  subplot(211)
			  if PlotAll==1
	    		semilogy(f,FFT(:,Ndx1))
			  else
	    		semilogy(f,mean(FFT(:,Ndx1)')',f,max(FFT(:,Ndx1)')',f,min(FFT(:,Ndx1)')')
			  end
			  xaxis([0 ((size(FFT,1)-1)*0.25)]);
			  subplot(212);
			  dummy=get(gca,'YLim');
			  subplot(211);
			  set(gca,'YLim',dummy);
	    end;
	  end;  
    
	  if size(answer)~=[0,0]
			types = {'hi','factor','nbins'};
			limit=cell2struct(answer,types,1);
			limit.hi=str2num(limit.hi);
			limit.factor=str2num(limit.factor);
			limit.nbins=str2num(limit.nbins);
			IsOk=0;
	    if limit.hi~=Upper
	      Upper=limit.hi;
				IsOk=IsOk+1;
			end;
			if limit.factor~=Factor
				Factor=limit.factor;
				IsOk=IsOk+1;
			end;
			if limit.nbins~=NBins
				NBins=limit.nbins;
				IsOk=IsOk+1;
			end;
		else         % Cancel was pressed
			IsOk=-1;
	  end;
  else  % no plots and no prompts
    IsOk=0;
  end;
end;  
if IsOk<0      % Cancel was pressed, restore input index 
  Index=InputNdx;
  Aborted=1;
else
  Index=Ndx1;
  Aborted=0;
end;
if PlotAll<2 
  close(f1);
  close(f2);
end
return

function [Answer,Toggle]=excldlg(Prompt, Titlestr, NumLines, DefAns,Resize)
%ExclDLG Input dialog box.
%  Answer = inputdlg(Prompt) creates a modal dialog box that returns
%  user input for multiple prompts in the cell array Answer.  Prompt
%  is a cell array containing the Prompt strings.
%
%  Answer = inputdlg(Prompt,Title) specifies the Title for the dialog.
%
%  Answer = inputdlg(Prompt,Title,LineNo) specifies the number of lines
%  for each answer in LineNo.  LineNo may be a constant value or a 
%  column vector having one element per Prompt that specifies how many
%  lines per input.  LineNo may also be a matrix where the first
%  column specifies how many rows for the input field and the second
%  column specifies how many columns wide the input field should be.
%
%  Answer = inputdlg(Prompt,Title,LineNo,DefAns) specifies the default
%  answer to display for each Prompt.  DefAns must contain the same
%  number of elements as Prompt and must be a cell array.
%
%  Answer = inputdlg(Prompt,Title,LineNo,DefAns,Resize) specifies whether
%  the dialog may be resized or not.  Acceptable values for Resize are 
%  'on' or 'off'.  If the dialog can be resized, then the dialog is
%  not modal.
%
%  Example:
%  prompt={'Enter the matrix size:','Enter the colormap name:'};
%  def={'20','hsv'};
%  title='Input for Peaks function';
%  lineNo=1;
%  answer=inputdlg(prompt,title,lineNo,def);
%
%  See also TEXTWRAP.

%  Loren Dean   May 24, 1995.
%  Copyright (c) 1984-98 by The MathWorks, Inc.
%  $Revision: 1.43 $

%%%%%%%%%%%%%%%%%%%%%
%%% General Info. %%%
%%%%%%%%%%%%%%%%%%%%%
Black      =[0       0        0      ]/255;
LightGray  =[192     192      192    ]/255;
LightGray2 =[160     160      164    ]/255;
MediumGray =[128     128      128    ]/255;
White      =[255     255      255    ]/255;

%%%%%%%%%%%%%%%%%%%%
%%% Nargin Check %%%
%%%%%%%%%%%%%%%%%%%%
if nargin == 1 & nargout == 0,
  if strcmp(Prompt,'InputDlgResizeCB'),
    LocalResizeFcn(gcbf)
    return
  end
end

if nargin<1,error('Too few arguments for INPUTDLG');end

if nargin==1,
  Titlestr=' ';
end

if nargin<=2, NumLines=1;end

if ~iscell(Prompt),
  Prompt={Prompt};
end

NumQuest=prod(size(Prompt));    

if nargin<=3, 
  DefAns=cell(NumQuest,1);
  for lp=1:NumQuest, DefAns{lp}=''; end
end

WindowStyle='modal';
if nargin<=4,
  Resize = 'off';
end

if strcmp(Resize,'on'),
  WindowStyle='normal';
end

if nargin>5,error('Too many input arguments');end

% Backwards Compatibility
if isstr(NumLines),
  warning(['Please see the INPUTDLG help for correct input syntax.' 10 ...
           '         OKCallback no longer supported.' ]);
  NumLines=1;
end

[rw,cl]=size(NumLines);
OneVect = ones(NumQuest,1);
if (rw == 1 & cl == 2)
  NumLines=NumLines(OneVect,:);
elseif (rw == 1 & cl == 1)
  NumLines=NumLines(OneVect);
elseif (rw == 1 & cl == NumQuest)
  NumLines = NumLines'
elseif rw ~= NumQuest | cl > 2,
  error('NumLines size is incorrect.')
end

if ~iscell(DefAns),
  error('Default Answer must be a cell array in INPUTDLG.');  
end

%%%%%%%%%%%%%%%%%%%%%%%
%%% Create InputFig %%%
%%%%%%%%%%%%%%%%%%%%%%%
FigWidth=300;FigHeight=100;
FigPos(3:4)=[FigWidth FigHeight];
FigColor=get(0,'Defaultuicontrolbackgroundcolor');
InputFig=dialog(                               ...
               'Visible'         ,'off'      , ...
               'Name'            ,Titlestr      , ...
               'Pointer'         ,'arrow'    , ...
               'Units'           ,'points'   , ...
               'UserData'        ,''         , ...
               'Tag'             ,Titlestr      , ...
               'HandleVisibility','on'       , ...
               'Color'           ,FigColor   , ...
               'NextPlot'        ,'add'      , ...
               'WindowStyle'     ,WindowStyle, ...
               'Resize'          ,Resize       ...
               );
  

%%%%%%%%%%%%%%%%%%%%%
%%% Set Positions %%%
%%%%%%%%%%%%%%%%%%%%%
DefOffset=5;
SmallOffset=2;

DefBtnWidth=50;
BtnHeight=20;
BtnYOffset=DefOffset;
BtnFontSize=get(0,'FactoryUIControlFontSize');
BtnWidth=DefBtnWidth;
TxtBackClr=FigColor;
TxtForeClr=Black;

StInfo.Style              ='text'     ;
StInfo.Units              ='points'   ;   
StInfo.FontSize           =BtnFontSize;
StInfo.HorizontalAlignment='left'     ;
StInfo.BackgroundColor    =TxtBackClr ;
StInfo.ForegroundColor    =TxtForeClr ;
StInfo.HandleVisibility   ='callback' ;


EdInfo=StInfo;
EdInfo.Style='edit';
EdInfo.BackgroundColor=White;

BtnInfo=StInfo;
BtnInfo.Style='pushbutton';
BtnInfo.HorizontalAlignment='center';

% Determine # of lines for all Prompts
ExtControl=uicontrol(StInfo, ...
                     'String'   ,''         , ...    
                     'Position' ,[DefOffset                  DefOffset  ...
                                 0.96*(FigWidth-2*DefOffset) BtnHeight  ...
                                ]            , ...
                     'Visible'  ,'off'         ...
                     );
                     
WrapQuest=cell(NumQuest,1);
QuestPos=zeros(NumQuest,4);

for ExtLp=1:NumQuest,
  if size(NumLines,2)==2
    [WrapQuest{ExtLp},QuestPos(ExtLp,1:4)]= ...
        textwrap(ExtControl,Prompt(ExtLp),NumLines(ExtLp,2));
  else,
    [WrapQuest{ExtLp},QuestPos(ExtLp,1:4)]= ...
        textwrap(ExtControl,Prompt(ExtLp),80);
  end
end % for ExtLp

delete(ExtControl);
QuestHeight=QuestPos(:,4);

TxtHeight=QuestHeight(1)/size(WrapQuest{1,1},1);
EditHeight=TxtHeight*NumLines(:,1);
EditHeight(NumLines(:,1)==1)=EditHeight(NumLines(:,1)==1)+4;

FigHeight=(NumQuest+2)*DefOffset    + ...
          BtnHeight+sum(EditHeight) + ...
          sum(QuestHeight);

TxtXOffset=DefOffset;
TxtWidth=FigWidth-2*DefOffset;
TxtForeClr=Black;
TxtBackClr=get(InputFig,'Color');

QuestYOffset=zeros(NumQuest,1);
EditYOffset=zeros(NumQuest,1);
QuestYOffset(1)=FigHeight-DefOffset-QuestHeight(1);
EditYOffset(1)=QuestYOffset(1)-EditHeight(1);% -SmallOffset;

for YOffLp=2:NumQuest,
  QuestYOffset(YOffLp)=EditYOffset(YOffLp-1)-QuestHeight(YOffLp)-DefOffset;
  EditYOffset(YOffLp)=QuestYOffset(YOffLp)-EditHeight(YOffLp); %-SmallOffset;
end % for YOffLp

QuestHandle=[];
EditHandle=[];
FigWidth =1;
for lp=1:NumQuest,
  QuestTag=['Prompt' num2str(lp)];
  EditTag=['Edit' num2str(lp)];
  if ~ischar(DefAns{lp}),
    delete(InputFig);
    error('Default answers must be strings in INPUTDLG.');
  end
  QuestHandle(lp)=uicontrol(InputFig  ,                         ...
                           StInfo     , ...
                           'Max'      ,size(Prompt{lp},1), ...
                           'Position' ,[ TxtXOffset QuestYOffset(lp) ...
                                         TxtWidth   QuestHeight(lp)  ...
                                       ]                      , ...
                           'String'   ,WrapQuest{lp}       , ...
                           'Tag'      ,QuestTag                 ...
                           );

  EditHandle(lp)=uicontrol(InputFig   ,EdInfo     , ...
                          'Max'       ,NumLines(lp,1)       , ...
                          'Position'  ,[ TxtXOffset EditYOffset(lp) ...
                                         TxtWidth   EditHeight(lp)  ...
                                       ]                    , ...
                          'String'    ,DefAns{lp}           , ...
                          'Tag'       ,QuestTag               ...
                          );
  if size(NumLines,2) == 2,
    set(EditHandle(lp),'String',char(ones(1,NumLines(lp,2))*'x'));
    Extent = get(EditHandle(lp),'Extent');
    NewPos = [TxtXOffset EditYOffset(lp)  Extent(3) EditHeight(lp) ];
    NewPos1= [TxtXOffset QuestYOffset(lp) Extent(3) QuestHeight(lp)];
    set(EditHandle(lp),'Position',NewPos,'String',DefAns{lp})
    set(QuestHandle(lp),'Position',NewPos1)
    
    FigWidth=max(FigWidth,Extent(3)+2*DefOffset);
  else
    FigWidth=max(175,TxtWidth+2*DefOffset);
  end


end % for lp

FigPos=get(InputFig,'Position');

Temp=get(0,'Units');
set(0,'Units','points');
ScreenSize=get(0,'ScreenSize');
set(0,'Units',Temp);

FigWidth=max(FigWidth,2*(BtnWidth+DefOffset)+DefOffset);
FigPos(1)=(ScreenSize(3)-FigWidth)/2;
FigPos(2)=2*BtnHeight; %(ScreenSize(4)-FigHeight)/2;
FigPos(3)=FigWidth;
FigPos(4)=FigHeight;
set(InputFig,'Position',FigPos);

CBString='set(gcf,''UserData'',''Cancel'');uiresume';

CancelHandle=uicontrol(InputFig   ,              ...
                      BtnInfo     , ...
                      'Position'  ,[ (FigWidth-BtnWidth)/2 DefOffset ...
                                    BtnWidth  BtnHeight  ...
                                   ]           , ...
                      'String'    ,'Abort'    , ...
                      'Callback'  ,CBString    , ...
                      'Tag'       ,'Cancel'      ...
                      );
                                   
CBString='set(gcf,''UserData'',''Toggle'');uiresume';
 
ToggleHandle=uicontrol(InputFig   ,              ...
                      BtnInfo     , ...
                      'Position'  ,[ FigWidth-BtnWidth-DefOffset DefOffset ...
                                    BtnWidth  BtnHeight  ...
                                   ]           , ...
                      'String'    ,'Toggle'    , ...
                      'Callback'  ,CBString    , ...
                      'Tag'       ,'Toggle'      ...
                      );
                    
                    
CBString='set(gcf,''UserData'',''OK'');uiresume';

OKHandle=uicontrol(InputFig    ,              ...
                   BtnInfo     , ...
                   'Position'  ,[ DefOffset DefOffset ...
                                  BtnWidth                    BtnHeight ...
                                ]           , ...
                  'String'     ,'Recalc/OK'        , ...
                  'Callback'   ,CBString    , ...
                  'Tag'        ,'OK'          ...
                  );
    
Data.OKHandle = OKHandle;
Data.CancelHandle = CancelHandle;
Data.ToggleHandle = ToggleHandle;
Data.EditHandles = EditHandle;
Data.QuestHandles = QuestHandle;
Data.LineInfo = NumLines;
Data.ButtonWidth = BtnWidth;
Data.ButtonHeight = BtnHeight;
Data.EditHeight = TxtHeight+4;
Data.Offset = DefOffset;
set(InputFig ,'Visible','on','UserData',Data);
set(findall(InputFig),'Units','normalized','HandleVisibility','callback');
set(InputFig,'Units','points')

uiwait(InputFig);

TempHide=get(0,'ShowHiddenHandles');
set(0,'ShowHiddenHandles','on');

Toggle=0;
if any(get(0,'Children')==InputFig),
  Answer={};
  if strcmp(get(InputFig,'UserData'),'OK'),
    Answer=cell(NumQuest,1);
    for lp=1:NumQuest,
      Answer(lp)=get(EditHandle(lp),{'String'});
    end % for
  elseif strcmp(get(InputFig,'UserData'),'Toggle'),
    Toggle=1;
    Answer=cell(NumQuest,1);
    for lp=1:NumQuest,
      Answer(lp)=get(EditHandle(lp),{'String'});
    end % for
  end % if strcmp
  delete(InputFig);
else,
  Answer={};
end % if any

set(0,'ShowHiddenHandles',TempHide);


function LocalResizeFcn(FigHandle)
  Data=get(FigHandle,'UserData');
  
  %Data.ButtonHandles = [ OKHandles CancelHandle];
  %Data.EditHandles = EditHandle;
  %Data.QuestHandles = QuestHandle;
  %Data.LineInfo = NumLines;
  %Data.ButtonWidth = BtnWidth;
  %Data.ButtonHeight = BtnHeight;
  %Data.EditHeight = TxtHeight;
  
  set(findall(FigHandle),'Units','points');
  
  FigPos = get(FigHandle,'Position');
  FigWidth = FigPos(3); FigHeight = FigPos(4);
  
  OKPos = [ FigWidth-Data.ButtonWidth-Data.Offset Data.Offset ...
	    Data.ButtonWidth                      Data.ButtonHeight ];
  CancelPos =[Data.Offset Data.Offset Data.ButtonWidth  Data.ButtonHeight];
  TogglePos =[(FigWidth-Data.ButtonWidth-Data.Offset)/2 Data.Offset Data.ButtonWidth  Data.ButtonHeight];
  set(Data.OKHandle,'Position',OKPos);
  set(Data.CancelHandle,'Position',CancelPos);
  set(Data.ToggleHandle,'Position',TogglePos);

  % Determine the height of all question fields
  YPos = sum(OKPos(1,[2 4]))+Data.Offset;
  QuestPos = get(Data.QuestHandles,{'Extent'});
  QuestPos = cat(1,QuestPos{:});
  QuestPos(:,1) = Data.Offset;
  RemainingFigHeight = FigHeight - YPos - sum(QuestPos(:,4)) - ...
                       Data.Offset - size(Data.LineInfo,1)*Data.Offset;
  
  Num1Liners = length(find(Data.LineInfo(:,1)==1));
  
  RemainingFigHeight = RemainingFigHeight - ...
      Num1Liners*Data.EditHeight;
  
  Not1Liners = find(Data.LineInfo(:,1)~=1);

  %Scale the 1 liner heights appropriately with remaining fig height
  TotalLines = sum(Data.LineInfo(Not1Liners,1));
  
  % Loop over each quest/text pair
  
  for lp = 1:length(Data.QuestHandles),
   %if size(Data.LineInfo,2) == 2,
   %  CurString = get(Data.EditHandles(lp),'String');
   %  set(Data.EditHandles(lp),'String', ...
   %	 char(ones(1,Data.LineInfo(lp,2))*'x'));
   %  Extent = get(Data.EditHandles(lp),'Extent');
   %  set(Data.EditHandles(lp),'String',CurString)
   %  if Extent(3) > FigWidth-2*Data.Offset,
   %    Extent(3) = FigWidth -2*Data.Offset;
   %  end
   %else
   %  Extent(3) = FigWidth -2*Data.Offset;
   %end
   CurPos = get(Data.EditHandles(lp),'Position');
   NewPos = [Data.Offset YPos  CurPos(3) Data.EditHeight ];
   if Data.LineInfo(lp,1) ~= 1,
     NewPos(4) = RemainingFigPos*Data.NumLines(lp,1)/TotalLines;
   end
    
   set(Data.EditHandles(lp),'Position',NewPos)
   YPos = sum(NewPos(1,[2 4]));
   QuestPos(lp,2) = YPos;QuestPos(lp,3) = NewPos(3);
   set(Data.QuestHandles(lp),'Position',QuestPos(lp,:));
   YPos = sum(QuestPos(lp,[2 4]))+Data.Offset;
 end
 
 if YPos>FigHeight - Data.Offset,
   FigHeight = YPos+Data.Offset;
   FigPos(4)=FigHeight;
   set(FigHandle,'Position',FigPos);  
   drawnow
 end
 set(FigHandle,'ResizeFcn','inputdlg InputDlgResizeCB');
 
 set(findall(FigHandle),'Units','normalized')
return; 

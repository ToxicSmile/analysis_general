function [vistrack,vissymb,offs] = READTRAC(visfilename,track);

% VIS file operations 
%___________________________________________________________________
% Luca Finelli - 02.05.1997
% Institute of Pharmacology - UNI Z�rich - Switzerland
% 
% READTRAC returns a matrix of binary numbers,  VISTRACK,  where the
%          j-th element of the i-th  column (=i-th 20-s epoch) is 0,
%          if the  corresponding 4-sepoch is included, 1  if  it  is
%          discarded. This selection refers to  the  selected  TRACK
%          (default: track 1) in  the VISFILENAME file.
%
%          See also: READVIS
%
% Input Parameters: 
%
%   visfilename =  string: disc path and file name for VIS file (*.vis)
%   
% Optional Input Parameters (0 -> default):
%   
%   track       =  integer: selected track (default = 1)
%
% Output:
%   
%   vistrack    = binary matrix
%   vissymb     = char vector: sleep stage scoring for 20-s epochs 
%
% Optional Output:
%   
%   offs        = number of epochs to be discarded at the beginning
%
% Synopsis:
%
%   [vistrack,vissymb,offs] = READTRAC(visfilename,track);

% Last Update:   08.05.1997 lf - Added track choice parameter
% Last Update:   24.03.1999 tg - Added offs as optional output
% Last Update:   14.04.1999 tg - Modifications to allow vis files with missing sleep stages
% Last Update:   24.07.1999 lf - Help edited and indexing of vistrack tested: ok

if nargin < 2
  track = 1;
end

% READ OFFSET
offs=getoffs(visfilename);

% READ SCORING FILE INFORMATION TO "visdata" (epochs+stages+tracks up to TRACK)
fidvis=fopen(visfilename,'r');
fgetl(fidvis);
formatcode='%d%*c%c%*c';        % C format code to sscanf for 0 tracks 
for j=1:track
  formatcode=[formatcode '%c']; % changing C format code to read tracks
end                             % up to selected value (TRACK parameter)

i=1;
Finished=0;
while Finished==0
  visline=fgetl(fidvis);
  if ~ischar(visline)
    Finished=1;
  else
    [buffer,count]=sscanf(visline,formatcode,2+track);
    if count>0
      if i>1 & buffer(1)<visdata(1,i-1)
        Finished=1;
      else  
	      if count==1
  			  buffer=[buffer;32]; 
    			count=count+1;
			  end;
  			if count<2+track
  				for j=count+1:2+track 
    				buffer=[buffer;10]; 
		  		end
	  		end
 				visdata(:,i)=buffer;
        i=i+1;
      end;   
    end 
  end   
end;
fclose(fidvis);

% DECOMPRESSING SLEEP STAGES INFORMATION TO "vissymb"
[m n]=size(visdata);
for i=1:n-1
  lower=visdata(1,i);
	higher=visdata(1,i+1);
  for k=lower:higher-1
  	vissymb(k)=char(visdata(2,i));
  end;
end;
vissymb(visdata(1,n))=setstr(visdata(2,n));
if offs~=0
  vissymb=[linspace('x','x',offs) vissymb]; % shifting epoch count from lights off
end                                         % to beginning of file

% EXTRACTING 20-s EPOCHS WITH SOME EXCLUDED 4-s EPOCHS TO "excluded"
indexc=find(visdata(2+track,:)>=64 & visdata(2+track,:)<=95); % ASCII codes for exclusion is
                                                              % between 64 (none) and 95 (all)
excluded=visdata(1,indexc);

% CREATING MATRIX "vistrack" WITH 0 FOR INCLUDED 4-S EPOCH, 1 FOR EXCLUDED
%                 To row number corresponds 20-s epoch number in vis file
vistrack=zeros(length(vissymb),5);
for i=1:length(indexc)
  epochcode=dec2binlf(visdata(2+track,indexc(i))-64); % HUMAN SCORING track code:
                                                    % visdata(2+track,indexc(i))
                                                    % Example: ASCII code value of X in "283 m X"    
                                                    % -> 88
  while length(epochcode)<5
    epochcode=[0,epochcode];                        % bringing binary version of track code
  end;                                              % to five bit length 
  
  vistrack(excluded(i)+offs,:)=epochcode(5:-1:1);   % REVERSED! binary vector is 4-s excl.code
end
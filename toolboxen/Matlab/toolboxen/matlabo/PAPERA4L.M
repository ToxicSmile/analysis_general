function papera4l(h)

%PAPERA4L
%
% A4 landscape w�hlen
% 15:15 05.12.96, pa
% 20:36 19.05.99, lf - optional input: figure handle h

if nargin<1, h=gcf;, end

set(h,'PaperType','a4letter')
set(h,'PaperOrientation','landscape')
set(h,'PaperUnits','centimeters')
set(h,'PaperPosition',[0.63452 0.63452 28.408 19.715]) %[1.5 1.5 27 16.5]

function  matnet128(ndxok)

load mat_net128.txt    %for topoplot from eeglab =>lables
mat_netn=mat_net128(ndxok,:);

mat_netn=mat_netn';
fid = fopen('test128.loc','w');
fprintf(fid,'%4.0f %6.2f %4.2f %5.0f\n',mat_netn);
fclose(fid);

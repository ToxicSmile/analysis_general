example: data_p is the data to plot

matnet128AM(isfinite(data_p)) 

data_p2=isfinite(data_p);

% without marker2
topoplottest3(data_p(isfinite(data_p)),[cd,'\test128.loc'],'maplimits','minmax','conv','on','intrad',0.5,'colormap','jet','style','both','electrodes','on','emarkersize',5,...
'gridscale',300)

% with marker2
topoplottest3(data_p(isfinite(data_p)),[cd,'\test128.loc'],'maplimits','minmax','conv','on','intrad',0.5,'colormap','jet','style','both','electrodes','on','emarkersize',5,...
'gridscale',200,'emarker2',{[(find(data_p(isfinite(data_p))>100))],'o','w'})
colorbar
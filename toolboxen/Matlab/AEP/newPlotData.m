function newPlotData(timeAxis,averageData,yRange)

fprintf('\nPlotting data...\n');

%function nxaPlotData(timeAxis,averageData,yRange)
%
%timeAxis - time scale in milliseconds
%averageData - data matrix in microvolts (channels x samples)
%yRange - in microVolts e.g. [-20 20]
%averageData2 - optional average data

for i=1:128
    subplot(13,10,i)
    plot(timeAxis,averageData(i,:))
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    set(gca,'XTickLabel',[])
    set(gca,'YTickLabel',[])
    axis([-100 500 yRange])
    text(-15,2.5,num2str(i),'FontSize',8,'Color','r')
    line([0 0],yRange)
end

% decide if will be resuming or not, then set up file saving

clear all;
close all;

toResume = 0;
while min(toResume ~= ['n' 'N' 'r' 'R'])
    toResume = input('(n)ew, (r)esume: ','s');
end

%File to get data from, name of save file determined automatically based on
%load file name
[fname, path] = uigetfile('D:\AEP_Ana\DATA\BMS_baseline\*.raw', 'Trial data file');
fname = fname(1:length(fname) - 4);
trialDataFName = sprintf('%s%s.acc',path,fname);
fprintf('Accepted trial data will be put in %s\n\n',trialDataFName);

filename=sprintf('%s%s.raw',path,fname);;


timeRange = input('Time Range ([-50 500]): ');
if isempty(timeRange)
    timeRange = [-50 500];
end
baselineRange = input('Baseline Range ([-50 0]): ');
if isempty(baselineRange)
    baselineRange = [-50 0];
end

ySeparation = input('Y Separation (20): ');
if isempty(ySeparation)
    ySeparation = 20;
end
eogLimit = input('Acceptable Eog Limit (50): ');
if isempty(eogLimit)
    eogLimit = 50;
end                                      

fprintf('\n\n\nReading trigger times...\n');
tPos = egiGetTriggersCorr(filename);
% tPos=tPos(1:50);

%Display averaged data so that bad channels can be rejected when selecting
%trials
fprintf('\nReading data...\n');
for i=1:length(tPos)
    [data,timeAxis] = newGetTrial(filename,tPos(i),timeRange,baselineRange);  
    if i == 1;
        ave = zeros(size(data));
    end
    ave = ave + data;
end

aveF=ave(1:128,:);
aveF = aveF / length(tPos);

figure;newPlotData(timeAxis,aveF, [-5 5]);

reject =[];
choice = input('Use current channel choices? (n)(y): ','s');
if isempty(choice)
    choice = 'n';
end
while choice ~= ['y' 'Y']
    reject = input('Channels to reject: ');
    testF = aveF;
    testF(reject,:) = 0;
    close all;
    figure;newPlotData(timeAxis,testF, [-5 5]); 
    choice = input('Use current channel choices? (n)(y): ','s');
    if isempty(choice)
        choice = 'n';
    end
end

if isempty(reject)
    reject = 0;
end

ch = [];
for i = 1:128;
    if i ~= reject
        ch(length(ch) + 1) = i;
    end
end
visibleChannels = ch;


% prepare data storage
acceptedPos = [];   %Store accepted trial positions
trialPos = [];      %Store trial positions, regardless of accept/reject
i=1;
if max(toResume == ['r' 'R'])
    eval(sprintf('load ''%s''',trialDataFName));
    i = find(trialPos(length(trialPos)) == tPos) + 1;
end

fprintf('\nAccept or reject the following trials\n\tMOUSE CLICK:  accept\n\tb:  go back one trial\n\tq:  quit\n\tANY OTHER KEY: reject');
fprintf('\nPress any key to begin\n');
pause;

close all;
more off;
while i<=length(tPos)
    trialPos(length(trialPos)+1) = tPos(i); % keep track of when each trial occurs, whether rejected or accepted
    
    [d,t] = newGetTrial(filename,tPos(i),timeRange,baselineRange);
    
    %Ave ref
    ave = mean(d(visibleChannels,:));
    for j = 1:128
    d(j,:) = d(j,:) - ave;
    end
    
    
    msePlotData(t,d,visibleChannels,ySeparation,eogLimit);
    
    [x,y,button] = ginput(1); % get input
    if button == 98; % go back one trial
        trialPos = trialPos(1:length(trialPos) - 1);
        i = find(trialPos(length(trialPos)) == tPos);
        
        if (acceptedPos(length(acceptedPos)) == tPos(i));
            acceptedPos = acceptedPos(1:length(acceptedPos)-1);
        end
        trialPos = trialPos(1:length(trialPos) - 1);
    elseif button==1;
        acceptedPos = [acceptedPos;tPos(i)];
        disp(['Trial ' int2str(i) ', Accepted']);
        i = i + 1;
    elseif button == 'q'
        break        
    else
        disp(['Trial ' int2str(i) ', Rejected']);
        i = i + 1;
    end
    eval(sprintf('save ''%s'' trialPos acceptedPos',trialDataFName));
end
close all;


fprintf('\nReading data...\n');
ave = zeros(size(data));
for i=1:length(tPos)
    if ~(max(trialPos(i) == acceptedPos))
        continue
    end
    [data,timeAxis] = newGetTrial(filename,tPos(i),timeRange,baselineRange);  
    ave = ave + data;
end

%Show all channels again and ask which to reject.  This second showing may
%be important if rejecting bad channels changes over all appearance.
aveF=ave(1:128,:);
aveF = aveF / length(tPos);
figure;newPlotData(timeAxis,aveF, [-5 5]);

choice = input('Use current channel choices? (n)(y): ','s');
if isempty(choice)
    choice = 'n';
end
if ~isstr(choice)
    choice = 'n';
end
while choice ~= ['y' 'Y']
    reject = input('Channels to reject: ');
    testF = aveF;
    testF(reject,:) = 0;
    close all;
    figure;newPlotData(timeAxis,testF, [-5 5]); 
    choice = input('Use current channel choices? (n)(y) : ','s');
    if isempty(choice)
        choice = 'n';
    end
end
rejectCh = reject;
eval(sprintf('save ''%s'' trialPos acceptedPos rejectCh',trialDataFName));
close all;

clear all;
close all;
% 

%% baseline

[avgdata_BMS_001_eb]=newGetExpAveRaf('BMS_001_1e201604151836');  %evening
[avgdata_BMS_001_mb]=newGetExpAveRaf('BMS_001_1m201604160711');  %morning
[avgdata_BMS_002_eb]=newGetExpAveRaf('BMS_002_2e201605201846');  %evening
[avgdata_BMS_002_mb]=newGetExpAveRaf('BMS_002_2m201605210651');  %morning
[avgdata_BMS_004_eb]=newGetExpAveRaf('BMS_004_1e201605271855');  %evening
[avgdata_BMS_004_mb]=newGetExpAveRaf('BMS_004_1m201605280700');  %morning
[avgdata_BMS_005_eb]=newGetExpAveRaf('BMS_005_1e201604301920');  %evening
[avgdata_BMS_005_mb]=newGetExpAveRaf('BMS_005_1m2016050107');  %morning
[avgdata_BMS_008_eb]=newGetExpAveRaf('BMS_008_3e201607291830');  %evening
[avgdata_BMS_008_mb]=newGetExpAveRaf('BMS_008_3m201607300710');  %morning
[avgdata_BMS_012_eb]=newGetExpAveRaf('BMS_012_1e201605282230');  %evening
[avgdata_BMS_012_mb]=newGetExpAveRaf('BMS_012_1m201605290705');  %morning
[avgdata_BMS_022_eb]=newGetExpAveRaf('BMS_022_2e201607151845');  %evening
[avgdata_BMS_022_mb]=newGetExpAveRaf('BMS_022_2m201607160652');  %morning
[avgdata_BMS_026_eb]=newGetExpAveRaf('BMS_026_1e201609232251');  %evening
[avgdata_BMS_026_mb]=newGetExpAveRaf('BMS_026_1m201609240704');  %morning
% [avgdata_BMS_028_eb]=newGetExpAveRaf('BMS_028_2e201610282225');  %evening
% [avgdata_BMS_028_mb]=newGetExpAveRaf('BMS_028_2m201610290735');  %morning
[avgdata_BMS_029_eb]=newGetExpAveRaf('BMS_029_2e201610141843');  %evening
[avgdata_BMS_029_mb]=newGetExpAveRaf('BMS_029_2m201610150753');  %morning
[avgdata_BMS_031_eb]=newGetExpAveRaf('BMS_031_2e201611111853');  %evening
[avgdata_BMS_031_mb]=newGetExpAveRaf('BMS_031_2m201611120705');  %morning
[avgdata_BMS_032_eb]=newGetExpAveRaf('BMS_032_2e201611252245');  %evening
[avgdata_BMS_032_mb]=newGetExpAveRaf('BMS_032_2m201611260631');  %morning
[avgdata_BMS_033_eb]=newGetExpAveRaf('BMS_033_1e201611052205');  %evening
[avgdata_BMS_033_mb]=newGetExpAveRaf('BMS_033_1m201611060746');  %morning
[avgdata_BMS_034_eb]=newGetExpAveRaf('BMS_034_1e201611182231');  %evening
[avgdata_BMS_034_mb]=newGetExpAveRaf('BMS_034_1m201611190611');  %morning
[avgdata_BMS_035_eb]=newGetExpAveRaf('BMS_035_2e201612102149');  %evening
[avgdata_BMS_035_mb]=newGetExpAveRaf('BMS_035_2m201612110612');  %morning
[avgdata_BMS_036_eb]=newGetExpAveRaf('BMS_036_1e201612032213');  %evening
[avgdata_BMS_036_mb]=newGetExpAveRaf('BMS_036_1m201612040652');  %morning


%% stimulation
% [avgdata_ST03_es]=newGetExpAveRaf('stimulation\ST03CR_2e201503192302');  %evening
% [avgdata_ST03_ms]=newGetExpAveRaf('stimulation\ST03CR_2m201503200700');  %morning
% [avgdata_ST07_es]=newGetExpAveRaf('stimulation\ST07MS_2e201502172017');  %evening
% [avgdata_ST07_ms]=newGetExpAveRaf('stimulation\ST07MS_2m201502180606');  %morning
% [avgdata_ST19_es]=newGetExpAveRaf('stimulation\ST19EH_2e201503032230');  %evening
% [avgdata_ST19_ms]=newGetExpAveRaf('stimulation\ST19EH_2m201503040716');  %morning
% [avgdata_ST23_es]=newGetExpAveRaf('stimulation\ST23GC_3e201503102020');  %evening
% [avgdata_ST23_ms]=newGetExpAveRaf('stimulation\ST23GC_3m201503110606');  %morning
% [avgdata_ST26_es]=newGetExpAveRaf('stimulation\ST26DK_3e201502192130');  %evening
% [avgdata_ST26_ms]=newGetExpAveRaf('stimulation\ST26DK_3m201502200708');  %morning
% [avgdata_ST31_es]=newGetExpAveRaf('stimulation\ST31LE_2e201503172245');  %evening
% [avgdata_ST31_ms]=newGetExpAveRaf('stimulation\ST31LE_2m201503180622');  %morning



%% mean over electrodes of interest morning baseline (mb) 

% mean_BMS_001_mb= nanmean(avgdata_BMS_001_mb([37 30 13],:)); %%%%Eckige klammer definiert variablen of interest der ersten Dimension((Elektroden) Komma trennt Dimensuionen, danach zweite Dimension. da alle vom interesse ':' nehmen 

%mean_BMS_001_mb= nanmean(avgdata_BMS_001_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); %80:120
mean_BMS_001_mb= nanmean(avgdata_BMS_001_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120));
mean_BMS_002_mb= nanmean(avgdata_BMS_002_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_004_mb= nanmean(avgdata_BMS_004_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_005_mb= nanmean(avgdata_BMS_005_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_008_mb= nanmean(avgdata_BMS_008_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_012_mb= nanmean(avgdata_BMS_012_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_022_mb= nanmean(avgdata_BMS_022_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_026_mb= nanmean(avgdata_BMS_026_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
% mean_BMS_028_mb= nanmean(avgdata_BMS_028_mb(:,:)); 
mean_BMS_029_mb= nanmean(avgdata_BMS_029_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_031_mb= nanmean(avgdata_BMS_031_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_032_mb= nanmean(avgdata_BMS_032_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_033_mb= nanmean(avgdata_BMS_033_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_034_mb= nanmean(avgdata_BMS_034_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_035_mb= nanmean(avgdata_BMS_035_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_036_mb= nanmean(avgdata_BMS_036_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 


mean_all_mb= nanmean([mean_BMS_001_mb;mean_BMS_002_mb;mean_BMS_004_mb;mean_BMS_008_mb;mean_BMS_012_mb;mean_BMS_022_mb;mean_BMS_026_mb;mean_BMS_029_mb;mean_BMS_031_mb;mean_BMS_032_mb;mean_BMS_033_mb;mean_BMS_034_mb;mean_BMS_035_mb;mean_BMS_036_mb]);

%% mean over electrodes of interest morning stimulation (ms)

% mean_ST03_ms= nanmean(avgdata_ST03_ms([37 30 13],:)); 
% mean_ST07_ms= nanmean(avgdata_ST07_ms([37 30 13],:));
% mean_ST19_ms= nanmean(avgdata_ST19_ms([37 30 13],:));
% mean_ST23_ms= nanmean(avgdata_ST23_ms([37 30 13],:));
% mean_ST26_ms= nanmean(avgdata_ST26_ms([37 30 13],:));
% mean_ST31_ms= nanmean(avgdata_ST31_ms([37 30 13],:));

% mean_all_ms= nanmean([mean_ST03_ms;mean_ST07_ms;mean_ST19_ms;mean_ST23_ms;mean_ST26_ms;mean_ST31_ms]);

%% mean of electrodes of interest evening baseline (eb) 

% mean_BMS_001_eb= nanmean(avgdata_BMS_001_eb([37 30 13],:)); %%%%Eckige klammer definiert variablen of interest der ersten Dimension((Elektroden) Komma trennt Dimenionen, danach zweite Dimension. da alle vom interesse ':' nehmen 

mean_BMS_001_eb= nanmean(avgdata_BMS_001_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); %80:120
mean_BMS_002_eb= nanmean(avgdata_BMS_002_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_004_eb= nanmean(avgdata_BMS_004_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_005_eb= nanmean(avgdata_BMS_005_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_008_eb= nanmean(avgdata_BMS_008_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_012_eb= nanmean(avgdata_BMS_012_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_022_eb= nanmean(avgdata_BMS_022_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_026_eb= nanmean(avgdata_BMS_026_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
% mean_BMS_028_eb= nanmean(avgdata_BMS_028_eb(:,:)); 
mean_BMS_029_eb= nanmean(avgdata_BMS_029_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_031_eb= nanmean(avgdata_BMS_031_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_032_eb= nanmean(avgdata_BMS_032_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_033_eb= nanmean(avgdata_BMS_033_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_034_eb= nanmean(avgdata_BMS_034_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_035_eb= nanmean(avgdata_BMS_035_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 
mean_BMS_036_eb= nanmean(avgdata_BMS_036_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],80:120)); 

mean_all_eb= nanmean([mean_BMS_001_eb;mean_BMS_002_eb;mean_BMS_004_eb;mean_BMS_005_eb;mean_BMS_008_eb;mean_BMS_012_eb;mean_BMS_022_eb;mean_BMS_026_eb;mean_BMS_029_eb;mean_BMS_031_eb;mean_BMS_032_eb;mean_BMS_033_eb;mean_BMS_034_eb;mean_BMS_035_eb;mean_BMS_036_eb]); %%%%%ohne ST03, da baseline evening kein AEP erkennbar


%% mean of electrodes of interest evening stimulation (es) 
% 
% mean_ST03_es= nanmean(avgdata_ST03_es([37 30 13],:));
% mean_ST07_es= nanmean(avgdata_ST07_es([37 30 13],:));
% mean_ST19_es= nanmean(avgdata_ST19_es([37 30 13],:));
% mean_ST23_es= nanmean(avgdata_ST23_es([37 30 13],:));
% mean_ST26_es= nanmean(avgdata_ST26_es([37 30 13],:));
% mean_ST31_es= nanmean(avgdata_ST31_es([37 30 13],:));
% 
% 
%  
% mean_all_es= nanmean([mean_ST07_es;mean_ST19_es;mean_ST23_es;mean_ST26_es;mean_ST31_es]);


%% plot mean over electrodes of interest per subjects %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


cd D:\AEP_Ana\results

timeAxis = ((0:size(avgdata_BMS_001_eb,2)-1)+(-100));

h1=figure
subplot(331)
plot(timeAxis,mean_BMS_001_eb,'b',timeAxis,mean_BMS_001_mb,'r') %%%%%avgdata 
title('BMS 001')

subplot(332)
plot(timeAxis,mean_BMS_002_eb,'b',timeAxis,mean_BMS_002_mb,'r') %%%%%avgdata 
title('BMS 002')

subplot(333)
plot(timeAxis,mean_BMS_004_eb,'b',timeAxis,mean_BMS_004_mb,'r') %%%%%avgdata 
title('BMS 004')

subplot(334)
plot(timeAxis,mean_BMS_005_eb,'b',timeAxis,mean_BMS_005_mb,'r') %%%%%avgdata 
title('BMS 005')

subplot(335)
plot(timeAxis,mean_BMS_008_eb,'b',timeAxis,mean_BMS_008_mb,'r') %%%%%avgdata 
title('BMS 008')

subplot(336)
plot(timeAxis,mean_BMS_012_eb,'b',timeAxis,mean_BMS_012_mb,'r') %%%%%avgdata 
title('BMS 012')

subplot(337)
plot(timeAxis,mean_BMS_022_eb,'b',timeAxis,mean_BMS_022_mb,'r') %%%%%avgdata 
title('BMS 022')

subplot(338)
plot(timeAxis,mean_BMS_026_eb,'b',timeAxis,mean_BMS_026_mb,'r') %%%%%avgdata 
title('BMS 026')

% subplot(339)
% plot(timeAxis,mean_BMS_028_eb,'b',timeAxis,mean_BMS_028_mb,'r') %%%%%avgdata 
% title('BMS 028')

h2=figure
subplot(331)
plot(timeAxis,mean_BMS_029_eb,'b',timeAxis,mean_BMS_029_mb,'r') %%%%%avgdata 
title('BMS 029')

subplot(332)
plot(timeAxis,mean_BMS_031_eb,'b',timeAxis,mean_BMS_031_mb,'r') %%%%%avgdata 
title('BMS 031')

subplot(333)
plot(timeAxis,mean_BMS_032_eb,'b',timeAxis,mean_BMS_032_mb,'r') %%%%%avgdata 
title('BMS 032')

subplot(334)
plot(timeAxis,mean_BMS_033_eb,'b',timeAxis,mean_BMS_033_mb,'r') %%%%%avgdata 
title('BMS 033')

subplot(335)
plot(timeAxis,mean_BMS_034_eb,'b',timeAxis,mean_BMS_034_mb,'r') %%%%%avgdata 
title('BMS 034')

subplot(336)
plot(timeAxis,mean_BMS_035_eb,'b',timeAxis,mean_BMS_035_mb,'r') %%%%%avgdata 
title('BMS 035')

subplot(337)
plot(timeAxis,mean_BMS_036_eb,'b',timeAxis,mean_BMS_036_mb,'r') %%%%%avgdata 
title('BMS 036')


savefig= ('plot_single_subjects(1-26)_Temporallappen_alle_Zeitpunkte')
saveas(h1,savefig,'jpg') 

savefig= ('plot_single_subjects(29-36)_Temporallappen_alle_Zeitpunkte')
saveas(h2,savefig,'jpg') 

%% plot mean across subjects


cd D:\AEP_Ana\results

h3=figure
plot(timeAxis,mean_all_eb,timeAxis,mean_all_mb,'r') %%%%%avgdata 
title('mean of group, n=2')

savefig= ('grandAverage_Temporallappen_�ber_alle_Zeitpunikte')
saveas(h3,savefig,'jpg')


%% statistics  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


eb=[mean_BMS_001_eb;mean_BMS_002_eb;mean_BMS_004_eb;mean_BMS_008_eb;mean_BMS_012_eb;mean_BMS_022_eb;mean_BMS_026_eb;mean_BMS_029_eb;mean_BMS_031_eb;mean_BMS_032_eb;mean_BMS_033_eb;mean_BMS_034_eb;mean_BMS_035_eb;mean_BMS_036_eb];
mb=[mean_BMS_001_mb;mean_BMS_002_mb;mean_BMS_004_mb;mean_BMS_008_mb;mean_BMS_012_mb;mean_BMS_022_mb;mean_BMS_026_mb;mean_BMS_029_mb;mean_BMS_031_mb;mean_BMS_032_mb;mean_BMS_033_mb;mean_BMS_034_mb;mean_BMS_035_mb;mean_BMS_036_mb]; %%%% baseline morning variable f�r alle subjects zu jedem Zeitpunkt (6x601) (601 sind die Zeitpunkte)

% ms=[mean_ST03_ms;mean_ST07_ms;mean_ST19_ms;mean_ST23_ms;mean_ST26_ms;mean_ST31_ms]; %%%%% stimulation morning
% es=[mean_ST07_es;mean_ST19_es;mean_ST23_es;mean_ST26_es;mean_ST31_es]%


% % % t-Test:
[h_ERP,p_ERP]=ttest(eb,mb);
[sig_timepoints_ERP which_sig_timepoints_ERP]= find(p_ERP <=0.05);



%% global mean field power baseline

% EVENING
gmfp_BMS_001_eb=sqrt(nanmean(avgdata_BMS_001_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_002_eb=sqrt(nanmean(avgdata_BMS_002_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_004_eb=sqrt(nanmean(avgdata_BMS_004_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_005_eb=sqrt(nanmean(avgdata_BMS_005_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_008_eb=sqrt(nanmean(avgdata_BMS_008_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_012_eb=sqrt(nanmean(avgdata_BMS_012_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_022_eb=sqrt(nanmean(avgdata_BMS_022_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_026_eb=sqrt(nanmean(avgdata_BMS_026_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
% gmfp_BMS_028_eb=sqrt(nanmean(avgdata_BMS_028_eb(:,:).^2));  %evening 
gmfp_BMS_029_eb=sqrt(nanmean(avgdata_BMS_029_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_031_eb=sqrt(nanmean(avgdata_BMS_031_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_032_eb=sqrt(nanmean(avgdata_BMS_032_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_033_eb=sqrt(nanmean(avgdata_BMS_033_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_034_eb=sqrt(nanmean(avgdata_BMS_034_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_035_eb=sqrt(nanmean(avgdata_BMS_035_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_036_eb=sqrt(nanmean(avgdata_BMS_036_eb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 


% % % % % % % % % % Beispiel f�r bestimmte Elektroden �ber alle Zeitpunkte:
% gmfp_BMS_001_eb=sqrt(nanmean(avgdata_BMS_001_eb([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %evening
% gmfp_BMS_02_eb=sqrt(nanmean(avgdata_BMS_002_eb([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %evening
% gmfp_ST19_eb=sqrt(nanmean(avgdata_ST19_eb([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %evening
% gmfp_ST23_eb=sqrt(nanmean(avgdata_ST23_eb([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %evening
% gmfp_ST26_eb=sqrt(nanmean(avgdata_ST26_eb([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %evening
% gmfp_ST31_eb=sqrt(nanmean(avgdata_ST31_eb([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %evening



% MORNING
gmfp_BMS_001_mb=sqrt(nanmean(avgdata_BMS_001_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_002_mb=sqrt(nanmean(avgdata_BMS_002_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_004_mb=sqrt(nanmean(avgdata_BMS_004_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_005_mb=sqrt(nanmean(avgdata_BMS_005_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_008_mb=sqrt(nanmean(avgdata_BMS_008_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_012_mb=sqrt(nanmean(avgdata_BMS_012_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_022_mb=sqrt(nanmean(avgdata_BMS_022_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_026_mb=sqrt(nanmean(avgdata_BMS_026_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
% gmfp_BMS_028_mb=sqrt(nanmean(avgdata_BMS_028_mb(:,:).^2));  %evening 
gmfp_BMS_029_mb=sqrt(nanmean(avgdata_BMS_029_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_031_mb=sqrt(nanmean(avgdata_BMS_031_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_032_mb=sqrt(nanmean(avgdata_BMS_032_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_033_mb=sqrt(nanmean(avgdata_BMS_033_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_034_mb=sqrt(nanmean(avgdata_BMS_034_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_035_mb=sqrt(nanmean(avgdata_BMS_035_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 
gmfp_BMS_036_mb=sqrt(nanmean(avgdata_BMS_036_mb([38 39 44 45 50 57 58 65 90 96 100 101 108 114 115 121],:).^2));  %evening 


%% gmfp stimulation
% 
% gmfp_ST03_es=sqrt(nanmean(avgdata_ST03_es([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST07_es=sqrt(nanmean(avgdata_ST07_es([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST19_es=sqrt(nanmean(avgdata_ST19_es([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST23_es=sqrt(nanmean(avgdata_ST23_es([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST26_es=sqrt(nanmean(avgdata_ST26_es([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST31_es=sqrt(nanmean(avgdata_ST31_es([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% 

% 
% gmfp_ST03_ms=sqrt(nanmean(avgdata_ST03_ms([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST07_ms=sqrt(nanmean(avgdata_ST07_ms([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST19_ms=sqrt(nanmean(avgdata_ST19_ms([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST23_ms=sqrt(nanmean(avgdata_ST23_ms([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST26_ms=sqrt(nanmean(avgdata_ST26_ms([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning
% gmfp_ST31_ms=sqrt(nanmean(avgdata_ST31_ms([6 112 105 87 79 54 37 30 13 5 118 11 104 93 86 78 62 61 53 42 36 29 20 12],:).^2));  %morning


%% plot GMFP single subjects
h4=figure
subplot(331)
plot(timeAxis,gmfp_BMS_001_eb,'b',timeAxis,gmfp_BMS_001_mb,'r') %%%%%avgdata 
title('BMS 001')

subplot(332)
plot(timeAxis,gmfp_BMS_002_eb,'b',timeAxis,gmfp_BMS_002_mb,'r') %%%%%avgdata 
title('BMS 002')

subplot(333)
plot(timeAxis,gmfp_BMS_004_eb,'b',timeAxis,gmfp_BMS_004_mb,'r') %%%%%avgdata 
title('BMS 004')

subplot(334)
plot(timeAxis,gmfp_BMS_005_eb,'b',timeAxis,gmfp_BMS_005_mb,'r') %%%%%avgdata 
title('BMS 005')

subplot(335)
plot(timeAxis,gmfp_BMS_008_eb,'b',timeAxis,gmfp_BMS_008_mb,'r') %%%%%avgdata 
title('BMS 008')

subplot(336)
plot(timeAxis,gmfp_BMS_012_eb,'b',timeAxis,gmfp_BMS_012_mb,'r') %%%%%avgdata 
title('BMS 012')

subplot(337)
plot(timeAxis,gmfp_BMS_022_eb,'b',timeAxis,gmfp_BMS_022_mb,'r') %%%%%avgdata 
title('BMS 022')

subplot(338)
plot(timeAxis,gmfp_BMS_026_eb,'b',timeAxis,gmfp_BMS_026_mb,'r') %%%%%avgdata 
title('BMS 026')

% subplot(339)
% plot(timeAxis,gmfp_BMS_028_eb,'b',timeAxis,gmfp_BMS_028_mb,'r') %%%%%avgdata 
% title('BMS 028')

h5=figure
subplot(331)
plot(timeAxis,gmfp_BMS_029_eb,'b',timeAxis,gmfp_BMS_029_mb,'r') %%%%%avgdata 
title('BMS 029')

subplot(332)
plot(timeAxis,gmfp_BMS_031_eb,'b',timeAxis,gmfp_BMS_031_mb,'r') %%%%%avgdata 
title('BMS 031')

subplot(333)
plot(timeAxis,mean_BMS_032_eb,'b',timeAxis,mean_BMS_032_mb,'r') %%%%%avgdata 
title('BMS 032')

subplot(334)
plot(timeAxis,gmfp_BMS_033_eb,'b',timeAxis,gmfp_BMS_033_mb,'r') %%%%%avgdata 
title('BMS 033')

subplot(335)
plot(timeAxis,gmfp_BMS_034_eb,'b',timeAxis,gmfp_BMS_034_mb,'r') %%%%%avgdata 
title('BMS 034')

subplot(336)
plot(timeAxis,gmfp_BMS_035_eb,'b',timeAxis,gmfp_BMS_035_mb,'r') %%%%%avgdata 
title('BMS 035')

subplot(337)
plot(timeAxis,gmfp_BMS_036_eb,'b',timeAxis,gmfp_BMS_036_mb,'r') %%%%%avgdata 
title('BMS 036')

% % % % % % % % % 

savefig=('GMFP_single_subjects(1-26)_Temporallappen_�ber_alle_Zeitpunikte')
saveas(h4,savefig,'jpg')

savefig=('GMFP_single_subjects(29-36)_Temporallappen_�ber_alle_Zeitpunikte')
saveas(h5,savefig,'jpg')


%% GMFP average over subjects (= mean GFP)

mgmfp_eb=nanmean([gmfp_BMS_001_eb;gmfp_BMS_002_eb;gmfp_BMS_004_eb;gmfp_BMS_008_eb;gmfp_BMS_012_eb;gmfp_BMS_022_eb;gmfp_BMS_026_eb;gmfp_BMS_029_eb;gmfp_BMS_031_eb;gmfp_BMS_032_eb;gmfp_BMS_033_eb;gmfp_BMS_034_eb;gmfp_BMS_035_eb;gmfp_BMS_036_eb]);
mgmfp_mb=nanmean([gmfp_BMS_001_mb;gmfp_BMS_002_mb;gmfp_BMS_004_mb;gmfp_BMS_008_mb;gmfp_BMS_012_mb;gmfp_BMS_022_mb;gmfp_BMS_026_mb;gmfp_BMS_029_mb;gmfp_BMS_031_mb;gmfp_BMS_032_mb;gmfp_BMS_033_mb;gmfp_BMS_034_mb;gmfp_BMS_035_mb;gmfp_BMS_036_mb]);



%%%% plot MGMFP
h6=figure
plot(timeAxis,mgmfp_eb,timeAxis,mgmfp_mb,'r') %%%%%avgdata
title('MGMFP N=5 all central electrodes')

savefig=('Grand_Average_GMFP_Temporallappen_�ber_alle_Zeitpunikte')
saveas(h6,savefig,'jpg')


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% Statistics (gleich wie oben aber "ERP" �ndern und auch nicht dieselben Files nehmen (nicht eb, mb)

gmfp_eb=[gmfp_BMS_001_eb;gmfp_BMS_002_eb;gmfp_BMS_004_eb;gmfp_BMS_008_eb;gmfp_BMS_012_eb;gmfp_BMS_022_eb;gmfp_BMS_026_eb;gmfp_BMS_028_eb;gmfp_BMS_029_eb;gmfp_BMS_031_eb;gmfp_BMS_032_eb;gmfp_BMS_033_eb;gmfp_BMS_034_eb;gmfp_BMS_035_eb;gmfp_BMS_036_eb];
gmfp_mb=[gmfp_BMS_001_mb;gmfp_BMS_002_mb;gmfp_BMS_004_mb;gmfp_BMS_008_mb;gmfp_BMS_012_mb;gmfp_BMS_022_mb;gmfp_BMS_026_mb;gmfp_BMS_028_mb;gmfp_BMS_029_mb;gmfp_BMS_031_mb;gmfp_BMS_032_mb;gmfp_BMS_033_mb;gmfp_BMS_034_mb;gmfp_BMS_035_mb;gmfp_BMS_036_mb];


[h_gmfp,p_gmfp]=ttest(gmfp_eb,gmfp_mb);
[sig_timepoints_gmfp which_sig_timepoints_gmfp]= find(p_gmfp <=0.05);







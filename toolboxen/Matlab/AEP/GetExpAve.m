function [aveF]=GetExpAve(filename);

timeRange = [-20 100];
baselineRange = [-20 0];

filenameraw=['C:\data\human\SEP\',filename,'.raw'];
filenameacc=['C:\data\human\SEP\',filename,'.acc'];

eval(['load ',filenameacc,' -mat']);

goodCh=1:1:193;
goodCh(rejectCh)=[];

for i=1:length(acceptedPos)
    [data,timeAxis] = nxeGetTrial(filenameraw,acceptedPos(i),timeRange,baselineRange);
    if i == 1;
        ave = zeros(size(data));
    end
    %Ave Ref
    aveRef=mean(data(goodCh,:));
    for j=1:193
        data(j,:)=data(j,:)-aveRef;
    end
    ave = ave + data;
end
aveF=ave(1:193,:);
aveF = aveF / length(acceptedPos);

aveF(rejectCh,:)=NaN;

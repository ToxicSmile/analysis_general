function A2_FiltNew(filename)
%clear all; close all;

%filename='SteEss200504121025001';
datapath='D:\AEP_Ana\DATA\BMS_baseline\';

locutoff=1;
hicutoff=40;

nch=128;

filenameraw=[datapath,filename,'.raw'];
filenameout=[datapath,filename,'.raf'];

fidout=fopen(filenameout,'wb');

[dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent, data] = loadEGIRaw(filenameraw);

for channel=1:nch
    channel

    dataf=eegfilt(data(channel,:),Samp_Rate,locutoff,0);
    dataf2=eegfilt(dataf,Samp_Rate,0,hicutoff);
    
    fwrite(fidout,dataf2,dataFormat);
end
fclose(fidout);

newsamp=length(dataf);
fsraf=Samp_Rate;
eval(['save ',datapath,filename,'.mat newsamp nch fsraf -mat']);
fclose all;

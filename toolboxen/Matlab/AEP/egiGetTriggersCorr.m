function trigPos = egiGetTriggersCorr(filename)

%trigPos = nxeGetTriggers(filename)
%
%trigPos - trigger positions in bytes
%filename - eXimia EEG raw data file

% clear all;
% filename='e:\data\egi\aep_adu\ag\AG200804022237002.raw';

fidhdr=fopen(filename,'rb','b');

[segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fidhdr);

event = loadEGIBigRaw(filename,130);
trigPos=find(event==1);

%%%%correction for double flags (i.e. triggers <5 ms appart)
ndxdouble=find(diff(trigPos)<5)+1;
trigPos(ndxdouble)=[];


fclose('all');

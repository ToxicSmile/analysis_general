function [data,timeAxis] = egiGetTrial(filename,filePosition,timeRange,baselineRange)

%function [data,timeAxis] = nxeGetTrial(filename,filePosition,timeRange,baselineRange)
%
%data - data matrix [channels samples]
%taxis - time axis in ms
%filename - eXimia EEG raw data filename
%filePosition - in bytes
%timeRange - in milliseconds e.g. [-100 500]
%baselineRange - in milliseconds e.g. [-100 0]

if nargin<4,end

fid=fopen(filename,'rb','b');
[segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fid);

% NSamp=1000;
% NChan=256;
% NEvent=2;

numChannels = NChan+NEvent;
timeSf = Samp_Rate/1000; 
tRange = round(timeRange.*timeSf);
bLine = round(baselineRange.*timeSf);
duration = diff(tRange)+1;

%fseek(fid,filePosition+2*numChannels*tRange(1),'cof');
fseek(fid,(filePosition+tRange(1))*2*numChannels,'cof');
data = fread(fid,[numChannels,duration],dataFormat); 			   

load insidenet;
data=data(insidenet,:);

%  figure;plot(data');
%  pause 
%  close all;

blStart = diff([tRange(1) bLine(1)])+1;
blStop = diff([tRange(1) bLine(2)])+1;
for i=1:193
    data(i,:) = data(i,:)-mean(data(i,blStart:blStop)); 
end

timeAxis = ((0:size(data,2)-1)+tRange(1))/timeSf;

fclose('all');

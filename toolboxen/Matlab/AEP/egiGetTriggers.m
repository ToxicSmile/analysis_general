function trigPos = egiGetTriggers(filename)

%trigPos = nxeGetTriggers(filename)
%
%trigPos - trigger positions in bytes
%filename - eXimia EEG raw data file

% clear all;
% filename='e:\data\egi\aep_adu\ag\AG200804022237002.raw';

fidhdr=fopen(filename,'rb','b');

[segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fidhdr);

event = loadEGIBigRaw(filename,130);
trigPos=find(event==1);

fclose('all');

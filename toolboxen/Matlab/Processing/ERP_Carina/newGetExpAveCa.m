function [aveF]=newGetExpAveCa(filename);

% clear all; close all;
% filename='EriLan200507080002004';

timeRange = [-100 500];
baselineRange = [-100 0];

% filenameraf=['D:\Data_AEP\test_mydata\',filename,'.raf'];
% filenameacc=['D:\Data_AEP\test_mydata\',filename,'.acc'];
filenamemat=['D:\Data_AEP\test_mydata\',filename,'*_afterComponentRejectionInterpAverefBLcorr.mat'];
filenamemat=['D:\Data_AEP\test_mydata\',filename,'*acceptedPos.mat'];

eval(['load ',filenameacc,' -mat']);
eval(['load ',filenamemat,' -mat']);

goodCh=1:1:128;

if rejectCh~=0
   goodCh(rejectCh)=[];
end

fidraf=fopen(filenameraf,'rb');
data=fread(fidraf,[newsamp,nch],'int16=>int16');

timeSf = fsraf/1000;
tRange = round(timeRange.*timeSf);
bLine = round(baselineRange.*timeSf);

acceptedPos(1)=[];  %%%%%%%%%%%%%%%%%%reject first Pos

for i=1:length(acceptedPos)
    dataPos=data(acceptedPos(i)+tRange(1):acceptedPos(i)+tRange(2),:);
    dataPos=double(dataPos');
    
    %Bl correct
    blStart = diff([tRange(1) bLine(1)])+1;
    blStop = diff([tRange(1) bLine(2)])+1;
    for j=1:128
        dataPos(j,:) = dataPos(j,:)-mean(dataPos(j,blStart:blStop));
    end

    if i == 1;
        ave = zeros(size(dataPos));
    end
    %Ave Ref
    aveRef=mean(dataPos(goodCh,:));
    for k=1:128
        dataPos(k,:)=dataPos(k,:)-aveRef;
    end
    ave = ave + dataPos;
end
aveF=ave(1:128,:);
aveF = aveF / length(acceptedPos);

if rejectCh~=0
    aveF(rejectCh,:)=NaN;
end



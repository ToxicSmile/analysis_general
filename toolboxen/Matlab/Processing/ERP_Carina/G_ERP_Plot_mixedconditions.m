
% Ch = '29_30_36';
cluster='Frontocentral 19 EL'
% i = 10;
% fname = sprintf('grandaverage_bm%d.mat', 'cluster', data_mn1);
% save(fname)

plot(squeeze(mean(baseline_evening(ChannelNb,1:700),1)),'color',[0 1 0])
        hold on
        plot(squeeze(mean(stimulation_evening(ChannelNb,1:700),1)),'color',[0 0 1])
        hx = graph2d.constantline(200,'Color',[ 0 0 0 ],'LineWidth',1);
        changedependvar(hx,'x');
        grid
       
        figurename=['allVP Evening ', cluster];
        title(figurename)
        xlabel('time [ms]')
        ylabel('microV')
set(gcf, 'Position', get(0,'Screensize')); % Maximize figure
legend('baseline evening','stimulation evening')


figurename=['AllVP_evening_conditions_',cluster,'_',num2str(date)];
 saveas(gcf,figurename,'jpeg');
close all

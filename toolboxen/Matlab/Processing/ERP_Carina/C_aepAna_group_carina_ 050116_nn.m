clear all;
close all;
% 
% 
% %%%%baseline display channels ohne average
[avgdata_ST31_be]=newGetExpCa('ST03CR\baseline\evening');  %evening
clear all;
[avgdata_ST31_bm]=newGetExpCa('ST03CR\baseline\morning');  %morning
clear all;
% [avgdata_ST07_e]=newGetExpRaf('baseline\ST07MS_3e201502241843');  %evening
% clear all;
% [avgdata_ST07_m]=newGetExpRaf('baseline\ST07MS_3m201502250603');  %morning
% clear all;
% [avgdata_ST17_e]=newGetExpRaf('baseline\ST17GW_2e201502172250');  %evening
% clear all;
% [avgdata_ST17_m]=newGetExpRaf('baseline\ST17GW_2m201502180639');  %morning
% clear all;
% [avgdata_ST19_e]=newGetExpRaf('baseline\ST19EH_3e201503102203');  %evening
% clear all;
% [avgdata_ST19_m]=newGetExpRaf('baseline\ST19EH_3m201503110711');  %morning
% clear all;
% [avgdata_ST20_e]=newGetExpRaf('baseline\ST20FG_2e201503171932');  %evening
% clear all;
% [avgdata_ST20_m]=newGetExpRaf('baseline\ST20FG_2m201503180549');  %morning
% clear all;
% [avgdata_ST23_e]=newGetExpRaf('baseline\ST23GC_2e201503031945');  %evening
% clear all;
% [avgdata_ST23_m]=newGetExpRaf('baseline\ST23GC_2m201503040635');  %morning
% clear all;
% [avgdata_ST26_e]=newGetExpRaf('baseline\ST26DK_2e201502102216');  %evening
% clear all;
% [avgdata_ST26_m]=newGetExpRaf('baseline\ST26DK_2m201502110713');  %morning
% clear all;
% [avgdata_ST30_e]=newGetExpRaf('baseline\ST30NH_2e201503192026');  %evening
% clear all;
% [avgdata_ST30_m]=newGetExpRaf('baseline\ST30NH_2m201503200626');  %morning
% clear all;
% [avgdata_ST31_e]=newGetExpRaf('baseline\ST31LE_3e201503242140');  %evening
% clear all;0
% [avgdata_ST31_m]=newGetExpRaf('baseline\ST31LE_3m201503250656');  %morning
% % 

% %%%%stimulation
% [avgdata_ST03_e]=newGetExpAveRaf('stimulation\ST03CR_2e201503192302');  %evening
% [avgdata_ST03_m]=newGetExpAveRaf('stimulation\ST03CR_2m201503200700');  %morning
% [avgdata_ST07_e]=newGetExpAveRaf('stimulation\ST07MS_2e201502172017');  %evening
% [avgdata_ST07_m]=newGetExpAveRaf('stimulation\ST07MS_2m201502180606');  %morning
% [avgdata_ST19_e]=newGetExpAveRaf('stimulation\ST19EH_2e201503032230');  %evening
% [avgdata_ST19_m]=newGetExpAveRaf('stimulation\ST19EH_2m201503040716');  %morning
% [avgdata_ST23_e]=newGetExpAveRaf('stimulation\ST23GC_3e201503102020');  %evening
% [avgdata_ST23_m]=newGetExpAveRaf('stimulation\ST23GC_3m201503110606');  %morning
% [avgdata_ST26_e]=newGetExpAveRaf('stimulation\ST26DK_3e201502192130');  %evening
% [avgdata_ST26_m]=newGetExpAveRaf('stimulation\ST26DK_3m201502200708');  %morning
% [avgdata_ST30_e]=newGetExpAveRaf('stimulation\ST30NH_3e201503262005');  %evening
% [avgdata_ST30_m]=newGetExpAveRaf('stimulation\ST30NH_3m201503270618');  %morning
% [avgdata_ST31_e]=newGetExpAveRaf('stimulation\ST31LE_2e201503172245');  %evening
% [avgdata_ST31_m]=newGetExpAveRaf('stimulation\ST31LE_2m201503180622');  %morning






%%%%topoplot, all electrodes
timeAxis = ((0:size(avgdata_ST31_e,2)-1)+(-100));
figure;newPlotData(timeAxis,avgdata_ST31_e, [-5 5]);

% %%%%single electrode
% el=30;   %%%%electrode of interest
% figure
% plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')


%%%% plot electrode cluster of interest
figure
subplot(331)
el=30;   %%%%electrode of interest
plot(timeAxis,avgdata_ST20_e(el,:),timeAxis,avgdata_ST20_m(el,:),'r')
title('stim el 30')

subplot(332)
el=36;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 36')

subplot(333)
el=29;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 29')

subplot(334)
el=35;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 35')

subplot(335)
el=37;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 37')

subplot(336)
el=41;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 41')

subplot(337)
el=42;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 42')

subplot(338)
el=47;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 47')

subplot(339)
el=54;   %%%%electrode of interest
plot(timeAxis,avgdata_ST19_e(el,:),timeAxis,avgdata_ST19_m(el,:),'r')
title('el 54')

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 




%%%%average across subjects baseline
% avgdata_e=[avgdata_ag_e;avgdata_xx_e;...]
% avgdata_m=[avgdata_ag_e;avgdata_xx_e;...]

%%%%average across subjects stimulation

% avgdata_e=[avgdata_ST03_e;avgdata_ST07_e;avgdata_ST19_e]
% avgdata_m=[avgdata_ST03_m;avgdata_ST07_m;avgdata_ST19_m]

% 
% 
% 
% 
% %%%%average
% mgmfp_c_e=mean(gmfp_c_e);
% mgmfp_c_m=mean(gmfp_c_m);
% 
% mgmfp_h_e=mean(gmfp_h_e);
% mgmfp_h_m=mean(gmfp_h_m);
% 
% 
% %%%%statistics
% p_c_em=pttest(mgmfp_c_e,mgmfp_c_m);
% p_h_em=pttest(mgmfp_h_e,mgmfp_h_m);
% 
% p_hc_e=uttest(mgmfp_c_e,mgmfp_h_e);
% p_hc_m=uttest(mgmfp_c_m,mgmfp_h_m);
% 
% 
% timeAxis = ((0:size(avgdata_ag_e,2)-1)+(-100));
% %%%%plot
% figure
% suplot(121)
% plot(timeAxis,gmfp_c_e,timeAxis,gmfp_c_m,'r')
% title('controls')
% grid
% % suplot(122)
% plot(timeAxis,gmfp_h_e,timeAxis,gmfp_h_m,'r')
% title('herzli')
% grid

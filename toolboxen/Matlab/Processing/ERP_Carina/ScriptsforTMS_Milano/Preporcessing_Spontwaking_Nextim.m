%% Load Data
cd (ssp_export.structure_1.Path);
str=fieldnames(ssp_export.structure_1.Sessions);
ok=listdlg('SelectionMode','single','ListString', str);
filename=ssp_export.structure_1.Sessions.(str{ok}).NXEfile;
dati=ssp_eeg_analysis_trialsrejection_loadEximia(filename,5:64,1450,64);
dati=dati';

eval(['bad_elec=ssp_export.structure_1.Sessions.',str{ok},'.Conditions.BadChannels-4;']);
eval(['goodtriggers=ssp_export.structure_1.Sessions.',str{ok},'.Conditions.Analyses.Trials_Triggers.ALLIndex(ssp_export.structure_1.Sessions.',str{ok},'.Conditions.Analyses.Trials_Triggers.IndexAccepted);']);
eval(['sr=ssp_export.structure_1.Sessions.',str{ok},'.AcqRate;']);
sr=str2num(sr);

%% Filter
hp=2; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
lp=80; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

w2=2*lp/sr;
w=[w2];
[B,A]=butter(3,w,'low');
w2=2*hp/sr;
w=[w2];
[C,D]=butter(3,w,'high');
for kk=1:size(dati,1)
    a=filtfilt(B,A,dati(kk,:));
    a=filtfilt(C,D,a);
    datifilt(kk,:)=a;
end
clear w w2 A B C D kk a dati
dati=datifilt;
clear datifilt

%% Split
epoch=sr; %INSERIRE VALORI SE FRAZIONE DI sr!!!!!!!

datisplit=[];
for i=1:length(goodtriggers)
    temp=dati(:,goodtriggers(i)-epoch:goodtriggers(i)+epoch);
    datisplit=cat(3,datisplit,temp);
end
dati=datisplit;
clear datisplit

%% Interpolate
locfilename='C:\Program Files\MATLAB\R2007b\work\SSP\EEG\nextim.sfp';

EEG=eeg_emptyset;

EEG.specdata=[];
EEG.icachansind=[];
EEG.specicaact=[];
EEG.reject=[];
EEG.stats=[];

EEG.nbchan=size(dati,1);
EEG.data=dati;
EEG.chanlocs=readlocs(locfilename);
EEG.trials=size(dati,3);
EEG.pnts=size(EEG.data,2);
EEG.srate=sr;

method='spherical';

if ~isempty(bad_elec)
    EEGOUT = eeg_interp_SS(EEG, bad_elec, method)
    datainterp=EEGOUT.data;
else
    datainterp=EEG.data;
end
clear EEGOUT

 figure;plottopo(mean(datainterp,3),EEG.chanlocs,0,[-600 600 -20 20],'',0,[.07 .07],{},1);

%% AVG ref
dataAVGref=[];
for i=1:size(datainterp,3)
    tempepoch=squeeze(mean(datainterp(:,:,i),1));
    tempepoch=repmat(tempepoch,60,1);
    epoch=datainterp(:,:,i)-tempepoch;
    dataAVGref=cat(3,dataAVGref,epoch);
end
clear tempepoch epoch

 figure;plottopo(mean(dataAVGref,3),EEG.chanlocs,0,[-800 800 -20 20],'',0,[.07 .07],{},1);

%% ICA (manual components removal)
eeglab

EEG.nbchan=size(dataAVGref,1);
EEG.data=dataAVGref;
EEG.chanlocs=readlocs(locfilename);
EEG.trials=size(dataAVGref,3);
EEG.pnts=size(EEG.data,2);
EEG.srate=sr;
EEG.ref='averef';

EEGICA=pop_runica(EEG,'icatype','runica');
EEG=EEGICA;
eeglab redraw

%% Load dateset containing the ICA pruned data
EEG=pop_loadset;
eeglab redraw
newdataAVGref=EEG.data;

%% FFT
ffttot=[];
for channel=1:size(newdataAVGref,1)
    channel
    for epochNum=1:size(newdataAVGref,3)
        epochNum;
        ffte=pwelch(newdataAVGref(channel,:,epochNum),[],[],2*sr,sr);
        ffte=ffte(1:81);
        ffttot(channel,:,epochNum)=ffte;
    end
end

%% Plots
FFT=squeeze(mean(ffttot,3));
FFT(14,:)=NaN;
f=0:.5:40;
figure,semilogy(f,FFT');

%%%Theta
figure,
topoplot(mean(FFT(:,10:16),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');
title('Theta')

%%% Delta Theta Alpha Beta
figure;
subplot(3,2,1);title('Delta (1-4 Hz)');
topoplot(mean(FFT(:,2:8),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
subplot(3,2,2);title('Theta (5-8 Hz)');
topoplot(mean(FFT(:,10:16),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
subplot(3,2,3);title('Alpha (8-13 Hz)');
topoplot(mean(FFT(:,16:26),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
subplot(3,2,4);title('Beta (13-20 Hz)');
topoplot(mean(FFT(:,26:40),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
subplot(3,2,5);title('Low-gamma (20-30 Hz)');
topoplot(mean(FFT(:,40:60),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
subplot(3,2,6);title('Hi-gamma (30-40 Hz)');
topoplot(mean(FFT(:,60:80),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar


save FFT.mat EEG ffttot FFT 
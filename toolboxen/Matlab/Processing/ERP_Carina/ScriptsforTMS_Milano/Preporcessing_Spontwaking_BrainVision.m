%% Load Data
clear all;clear all; close all;



[name path] = uigetfile('*.nxf', 'Select data file');
cd (path)

name=name(1:end-4);
load(name)
Samp_Rate=EEG.srate;
nchan=EEG.nbchan;
data=double(EEG.data);


filenameraw=[path,name,'.nxf'];
filenameacc=[path,name,'.acc'];
eval(['load ',filenameacc,' -mat']);
eval(['load ',name, '_add', '.mat newsamp']); 

if rejectCh==0
   rejectCh=[];
end


dati=data; clear data
goodtriggers=acceptedPos; clear acceptedPos
bad_elec=rejectCh; clear rejectCh
% clear rejectCh
sr=Samp_Rate;clear fsnxf

%% Filter
% hp=0.1; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% % lp=80; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% 
% % w2=2*lp/sr;
% % w=[w2];
% % [B,A]=butter(3,w,'low');
% w2=2*hp/sr;
% w=[w2];
% [C,D]=butter(3,w,'high');
% for kk=1:size(dati,1)
% %     a=filtfilt(B,A,dati(kk,:));
%     a=filtfilt(C,D,dati(kk,:));
% %     a=filtfilt(C,D,a);
%     datifilt(kk,:)=a;
% end
% clear w w2 C D kk a dati
% % clear w w2 A B C D kk a dati
% dati=datifilt;
% clear datifilt

%% Split
epoch=1500; %INSERT VALUES IN CASE THEY ARE FRACTION OF sr!!!!!!!
epochprepost= epoch;
datisplit=[];
for i=1:length(goodtriggers)
    temp=dati(:,goodtriggers(i)-epoch:goodtriggers(i)+epoch);
    datisplit=cat(3,datisplit,temp);
end
dati=datisplit;
clear datisplit

%% Interpolate
locfilename='E:\TEP_Matlabfiles\ScriptsforTMS_Milano\Brainamp62.elp';% CHANGE ACCORDING TO YOUR PATH

EEG=eeg_emptyset;

EEG.specdata=[];
EEG.icachansind=[];
EEG.specicaact=[];
EEG.reject=[];
EEG.stats=[];

dati([31 32],:,:)=[];%% !!!!!! REMOVES THE ADDITIONAL CHANNELS OUTSIDE TEH NET LAYOUT

for i =1:length(bad_elec); %%!!!! adjusts bad electrodes' number to new total number of electrodes
    if bad_elec(i) > 32
        bad_elec(i) = bad_elec(i) - 2;
    end
end

EEG.nbchan=size(dati,1);
EEG.data=dati;
EEG.chanlocs=readlocs(locfilename);
EEG.trials=size(dati,3);
EEG.pnts=size(EEG.data,2);
EEG.srate=sr;

method='spherical';

if ~isempty(bad_elec)
    EEGOUT = eeg_interp_SS(EEG, bad_elec, method)
    datainterp=EEGOUT.data;
else
    datainterp=EEG.data;
end
clear EEGOUT

%  figure;plottopo(mean(datainterp,3),EEG.chanlocs,0,[-100 100 -10 10],'',0,[.07 .07],{},1);


%% AVG ref
% dataAVGref=[];
% for i=1:size(datainterp,3)
%     tempepoch=squeeze(mean(datainterp(:,:,i),1));
%     tempepoch=repmat(tempepoch,size(datainterp,1),1);
%     epoch=datainterp(:,:,i)-tempepoch;
%     dataAVGref=cat(3,dataAVGref,epoch);
% end
% clear tempepoch epoch

%% Reref

datareref=datainterp;
for i = 1:size(datareref, 1)
    for j = 1:size(datareref, 3)
        datareref(i,:,j) = datareref(i,:,j) - mean(datareref(29:30,:,j),1);
    end
end


%% Baseline Correction

datareref_BLcorr = datareref;
baseline = epochprepost - 5;
for i = 1:size(datareref_BLcorr, 1)
    for j = 1:size(datareref_BLcorr, 3)
        datareref_BLcorr(i,:,j) = datareref_BLcorr(i,:,j) - mean(datareref_BLcorr(i, 1:baseline,j));
    end
end

figure;plottopo(mean(datareref_BLcorr(:,1000:2000,:),3),EEG.chanlocs,0,[-100 100 -10 10],'',0,[.07 .07],{},1);

figure;plottopo(mean(datareref_BLcorr(:,1000:2000,:),3),EEG.chanlocs,0,[-100 100 -20 20],'',0,[.07 .07],{},1);


milanofilename=[path,name,'_milano'];
save (milanofilename)


% %% ICA (manual components removal)
% eeglab
% 
% EEG.nbchan=size(dataAVGref,1);
% EEG.data=dataAVGref;
% EEG.chanlocs=readlocs(locfilename);
% EEG.trials=size(dataAVGref,3);
% EEG.pnts=size(EEG.data,2);
% EEG.srate=sr;
% EEG.ref='averef';
% % 
% EEGICA=pop_runica(EEG,'icatype','runica');
% EEG=EEGICA;
% eeglab redraw
% % 
% % % %% Load dateset containing the ICA pruned data
% % % EEG=pop_loadset;
% % % eeglab redraw
% % % newdataAVGref=EEG.data;
% % % %%
% % 
% % 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 





% %% FFT
% ffttot=[];
% for channel=1:size(newdataAVGref,1)
%     channel
%     for epochNum=1:size(newdataAVGref,3)
%         epochNum;
%         ffte=pwelch(newdataAVGref(channel,:,epochNum),[],[],2*sr,sr);
%         ffte=ffte(1:81);
%         ffttot(channel,:,epochNum)=ffte;
%     end
% end
% 
% %% Plots
% FFT=squeeze(mean(ffttot,3));
% FFT(14,:)=NaN;
% f=0:.5:40;
% figure,semilogy(f,FFT');
% 
% %%%Theta
% figure,
% topoplot(mean(FFT(:,10:16),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');
% title('Theta')
% 
% %%% Delta Theta Alpha Beta
% figure;
% subplot(3,2,1);title('Delta (1-4 Hz)');
% topoplot(mean(FFT(:,2:8),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
% subplot(3,2,2);title('Theta (5-8 Hz)');
% topoplot(mean(FFT(:,10:16),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
% subplot(3,2,3);title('Alpha (8-13 Hz)');
% topoplot(mean(FFT(:,16:26),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
% subplot(3,2,4);title('Beta (13-20 Hz)');
% topoplot(mean(FFT(:,26:40),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
% subplot(3,2,5);title('Low-gamma (20-30 Hz)');
% topoplot(mean(FFT(:,40:60),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
% subplot(3,2,6);title('Hi-gamma (30-40 Hz)');
% topoplot(mean(FFT(:,60:80),2),EEG.chanlocs, 'electrodes', 'on','maplimits','maxmin');colorbar
% 
% 
% save FFT.mat EEG ffttot FFT 
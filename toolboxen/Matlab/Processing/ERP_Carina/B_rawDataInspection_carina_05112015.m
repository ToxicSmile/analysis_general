%CV 05.11.15
%modifications for ICA: when inspecting epochs for bad channel
%identification, epochs with large artifacts should be removed and excluded
%from further analysis

clear all;close all;
eeglab
close all

%%Adjust according to your path:
path='D:\Data_AEP\ICA\VP\ST17GW\baseline\evening';


%File to get data from, name of save file determined automatically based on
%load file name
%ADJUST PATH!!!!

cd (path)
[fname path] = uigetfile('*.mat', 'Trial data file');


load(fname)
load('D:\Matlab\template_EEG_locfile\EEG_template_TFCE_eeglab')

%----------------------------------------------------------------------
%%%Load files%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% content_Trans = dir([filepath,subject,'\',session,'\','TransitionsSWANight*']);  
% load([filepath,subject,'\',session,'\',content_Trans(1).name],'MLinRiseMax_SWA_St3','SWA_NonREM_MNight')
% 
% load('C:\Matlab\alm\EEG_Templates\EEG_template_TFCE_eeglab')
% 
% load('D:\Matlab\template_EEG_locfile\EEG_template_TFCE_eeglab')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%adapt eeglab structure: 1 subject, 128 channels%%%%%%%%%%%%%%%%%%%%%%%%%

% Datafilt=double(Datafilt);


EEG.setname = [fname(1:end-16)];
EEG.data = Datafilt; 
EEG.times = 1;
EEG.nbchan = 128; %133 with all events, 129 without events + ref, 128 without ref
EEG.pnts = NSamp;
EEG.srate = Samp_Rate;
EEG.xmax   = (EEG.pnts - 1) / EEG.srate;
EEG.ref = 'common';
EEG.data2 = [];
EEG.chanlocs(129) = [];
test = pop_chanevent(EEG) % select event channel channel from eeg data (ch 130)
EEG.event = test.event %save event in EEG structure

%EEG.data = SWA_NonREM_MNight';
% EEG = readegilocs(EEG); 
% SIGTMPAVG = EEG.data';



%% exclude outer channels

Datafilt([129 130 131 132 133],:)=[];
Datafilt([43 48 49 56 63 68 73 81 88 94 99 107 113 119 120 125 126 127 128],:)=0; %remove eye channels from data structure
newsamp = length(Datafilt);
EEG.data = Datafilt;
 
% [ALLEEG EEG CURRENTSET] = eeg_store(ALLEEG, EEG);
pause;
eeglab redraw %%update of eegset
pause;

%%%%
%define bad channels:
badchan = input('Channels to reject: ');
chanlocs = EEG.chanlocs;

newfilename=[fname(1:end-4),'_RawDataInspect','.mat'];

cd(path)
save(newfilename,'EEG','badchan','chanlocs','-v7.3')

close all;
clear all;




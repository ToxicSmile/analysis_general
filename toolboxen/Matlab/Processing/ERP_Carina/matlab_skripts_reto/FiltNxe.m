function FiltNxe(name,session)

% clear all; close all;
% name='PAS_LTP_JR_2006_06_21';
% session='3';

Samp_Rate=1000;
locutoff=10;
hicutoff=100;
dataFormat='int16'; 

filenameraw=['\\fs-home\marig$\Eigene Dateien\MATLAB\',name,'_',session,'.mat'];
filenameout=['\\fs-home\marig$\Eigene Dateien\MATLAB\',name,'_',session,'_f','.mat'];

fidout=fopen(filenameout,'wb');

[data] = loadEximia(filenameraw,[5:64]); data=data';

for channel=1:60
    channel

    dataf=eegfilt(data(channel,:),Samp_Rate,locutoff,0);
    dataf2=eegfilt(dataf,Samp_Rate,0,hicutoff);
    
%     dataf2=eegfilt(data(channel,:),Samp_Rate,0,hicutoff);
    
    fwrite(fidout,dataf2,dataFormat);
end
fclose(fidout);

newsamp=length(dataf2);
fsnxf=Samp_Rate;
eval(['save \\fs-home\marig$\Eigene Dateien\MATLAB\',name,'_',session,'.acc newsamp fsnxf locutoff hicutoff -append']);
fclose all;

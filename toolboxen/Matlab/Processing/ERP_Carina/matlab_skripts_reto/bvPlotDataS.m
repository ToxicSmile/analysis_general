function nxaPlotDataS(timeAxis,averageData,yRange,averageData2)

%function nxaPlotData(timeAxis,averageData,yRange)
%
%timeAxis - time scale in milliseconds
%averageData - data matrix in microvolts (channels x samples)
%yRange - in microVolts e.g. [-20 20]
%averageData2 - optional average data

if nargin<3, end;
if nargin==3, averageData2=[];end;

eLocations = [
    0.3816    0.9200 %1
    0.4972    0.9141 %2
    0.6017    0.9200
    0.3678    0.8341
    0.4917    0.8460 %5
    0.6128    0.8341
    0.1669    0.8401
    0.2467    0.7690
    0.3595    0.7394
    0.4944    0.7424 %10
    0.6155    0.7483
    0.7091    0.7868
    0.7751    0.8667
    0.0100    0.7187
    0.0513    0.6654 %15
    0.1366    0.6388
    0.2274    0.6239
    0.3430    0.6180
    0.4944    0.6180
    0.6250    0.6210 %20
    0.7173    0.6299
    0.7944    0.6417
    0.8632    0.6684
    0.9045    0.7335
    0.0458    0.4966
    0.1366    0.4996
    0.2329    0.4966
    0.3348    0.5026
    0.4917    0.5026
    0.6348    0.5026
    0.7366    0.5085
    0.8274    0.5055
    0.9100    0.5114
    0.0238    0.2539
    0.0816    0.3131
    0.1476    0.3605
    0.2329    0.3753
    0.3375    0.3901
    0.4917    0.3930
    0.6320    0.3989
    0.7201    0.3871
    0.7834    0.3605
    0.8439    0.3249
    0.8990    0.2835
    0.1036    0.1473
    0.1559    0.2065
    0.2357    0.2480
    0.3430    0.2687
    0.4889    0.2716
    0.6265    0.2687
    0.7063    0.2450
    0.7696    0.2006
    0.8219    0.1384
    0.3540    0.1680
    0.4889    0.1680
    0.6155    0.1739
    0.3485    0.0733
    0.4834    0.0792
    0.6072    0.0792
    0.4751    0.0200];

clf;
set(gcf,'color',[1 1 1]);

k=0;
for i=1:60,
    
  k=k+1;
  h(k)=axes('position',[eLocations(i,1) eLocations(i,2) 0.07 0.05],...
            'ycolor',[0 0 0],...
            'xcolor',[0 0 0],...
            'ydir','normal',...
            'xlim',[min(timeAxis) max(timeAxis)],...
            'ylim',yRange,...
            'visible','off');
        
  l=line(timeAxis,averageData(i,:),...
      'clipping','off',...
      'color','b',...
      'userdata',i);   
  
  if i==1, set(l,'color','r'); end
  
  if ~isempty(averageData2),                 
    l=line(timeAxis,averageData2(i,:),...
           'clipping','off',...
           'linestyle','-',...
           'color',[1 0 0]);
  end     

  line([0 0],[-1000 1000],...
      'linestyle',':',...
      'color','k');
  
  
end

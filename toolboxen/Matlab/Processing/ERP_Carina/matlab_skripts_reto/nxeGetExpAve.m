function [aveF,timeAxis]=nxeGetExpAve(name,session,addbad);

% clear all; close all;
% name='PAS_LTD_SE_02_22_2006';
% session='2';

timeRange = [-50 400];
baselineRange = [-50 0];
% timeRange = [-20 100];
% baselineRange = [-20 0];

filenameraw=['F:\data\human\PAS\',name,'\',name,'_',session,'.nxe'];
filenameacc=['F:\data\human\PAS\',name,'\',name,'_',session,'.acc'];

eval(['load ',filenameacc,' -mat']);

goodCh=1:1:60;
if rejectCh~=0
    rejectCh=union(rejectCh,addbad);  %%%%remove additional bad channels
    goodCh(rejectCh)=[];
elseif ~isempty(addbad)
    rejectCh=addbad;  %%%%remove additional bad channels
    goodCh(rejectCh)=[];
end

for i=1:length(acceptedPos)
    [data,timeAxis] = nxeGetTrial(filenameraw,acceptedPos(i),timeRange,baselineRange);
    data=data(5:64,:);
    if i == 1;
        ave = zeros(size(data));
    end
    %Ave Ref
    aveRef=mean(data(goodCh,:));
    for j=1:60
        data(j,:)=data(j,:)-aveRef;
%         data(j,:)=data(j,:); %%%%no average referencing
    end
    ave = ave + data;
end
aveF=ave(1:60,:);
aveF = aveF / length(acceptedPos);

if rejectCh~=0
    aveF(rejectCh,:)=NaN;
end

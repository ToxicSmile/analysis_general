

function  A_FiltBv (path, fname);


cd (path);
load (fname);
% EEG=pop_loadbv
% 
% Samp_Rate=EEG.srate;
% nchan=EEG.nbchan;
% dataFormat='single';
% 
Samp_Rate=EEG.srate;
nchan=EEG.nbchan;
dataFormat='single';
% % 
locutoff=0.5;
% hicutoff=100;

% data=EEG.data;
data=double(EEG.data);

filenameout=[path ,fname,'.nxf'];
fidout=fopen(filenameout,'wb');


%% Filtering

hp=0.1; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% lp=80; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

% w2=2*lp/sr;
% w=[w2];
% [B,A]=butter(3,w,'low');
w2=2*hp/Samp_Rate;
w=[w2];
[C,D]=butter(3,w,'high');
for kk=1:size(data,1)
%     a=filtfilt(B,A,dati(kk,:));
    a=filtfilt(C,D,data(kk,:));
%     a=filtfilt(C,D,a);
    datifilt(kk,:)=a;
end
clear w w2 C D kk a data
% clear w w2 A B C D kk a dati
data=datifilt;
% clear datifilt


% fprintf('filtering of %s\n\n',fname);
% locutoff=0.5;
% % hicutoff=100;
% 
% for channel=1:nchan
%     channel
%     
%     datad=double(data(channel,:));
% 
%     dataf=eegfilt(datad,Samp_Rate,locutoff,0);
% %   
% %     dataf2=eegfilt(dataf,Samp_Rate,0,hicutoff);
% 
% %     dataf=dataf'; %???????????????????????????????????????????????????????????????
%     
%     fwrite(fidout,dataf,dataFormat);
%     
% end
% 
% fclose(fidout);

% no filtering, only data saving

for channel=1:nchan;
    channel
    
    fwrite(fidout,data(channel,:),dataFormat); 
 
end

fclose(fidout);

%% saving of additional info

% newsamp=length(dataf); %if data was filtered!!!!
% newsamp=length(datifilt); %if data was filtered!!!!
newsamp=length(data); %if data was not filtered!!!
fsnxf=Samp_Rate;

AddInfoFname = sprintf('%s%s_add',path,fname);

for i=1:length(EEG.urevent)
    events(i,:)=EEG.urevent(i).latency;
end

%eval(['save E:\data\Rositsa_test\test.acc', newsamp fsnxf locutoff hicutoff,' -mat']);
% save('C:\MATLAB_AM\tms_data\preproc\tep_cl_1.mat', 'newsamp','fsnxf', 'locutoff', 'events'); %if data was filtered!!!
% save('C:\MATLAB_AM\tms_data\preproc\tep_cl_5.mat', 'newsamp', 'fsnxf', 'events'); % if data was not filtered!!!
% save (AddInfoFname,'newsamp', 'fsnxf', 'events');
save (AddInfoFname,'newsamp', 'fsnxf', 'hp', 'events');

fclose all;
end

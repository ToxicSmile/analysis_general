function [aveF,timeAxis]=bvGetExpAve(filenameraw, filenameacc, addbad);

% clear all; close all;


timeRange = [-50 200];
baselineRange = [-50 0];
% timeRange = [-20 100];
% baselineRange = [-20 0];



eval(['load ',filenameacc,' -mat']);

goodCh=1:1:64;
if rejectCh~=0
    rejectCh=union(rejectCh,addbad);  %%%%remove additional bad channels
    goodCh(rejectCh)=[];
elseif ~isempty(addbad)
    rejectCh=addbad;  %%%%remove additional bad channels
    goodCh(rejectCh)=[];
end

for i=1:length(acceptedPos)
    [data,timeAxis] = bvGetTrial(filenameraw,acceptedPos(i),timeRange,baselineRange);
    data=data(1:64,:);
    if i == 1;
        ave = zeros(size(data));
    end
    %Ave Ref
%     aveRef=mean(data(goodCh,:));
    for j=1:64
%         data(j,:)=data(j,:)-aveRef;
        data(j,:)=data(j,:); %%%%no average referencing
    end
    ave = ave + data;
end
aveF=ave(1:64,:);
aveF = aveF / length(acceptedPos);

if rejectCh~=0
    aveF(rejectCh,:)=NaN;
end

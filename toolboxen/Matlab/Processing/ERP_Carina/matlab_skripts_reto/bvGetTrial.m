function [data,timeAxis] = bvGetTrial(filename,filePosition,timeRange,baselineRange)
% function [data,timeAxis] = nxeGetTrial(filename,filePosition,timeRange,baselineRange)

%function [data,timeAxis] = nxeGetTrial(filename,filePosition,timeRange,baselineRange)
%
%data - data matrix [channels samples]
%taxis - time axis in ms
%filename - eXimia EEG raw data filename
%filePosition - in bytes
%timeRange - in milliseconds e.g. [-100 500]
%baselineRange - in milliseconds e.g. [-100 0]

if nargin<4,end


numChannels = 64;
timeSf = 5.0; %adjust to SR in kHz!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
tRange = round(timeRange.*timeSf);
bLine = round(baselineRange.*timeSf);
duration = diff(tRange)+1;


fid = fopen(filename,'r','l');                   
fseek(fid,((filePosition*4*numChannels)+(4*numChannels*tRange(1))),'bof');
data = fread(fid,[numChannels duration],'single'); 

fclose(fid);


timeAxis = ((0:size(data,2)-1)+tRange(1))/timeSf;


fclose('all');

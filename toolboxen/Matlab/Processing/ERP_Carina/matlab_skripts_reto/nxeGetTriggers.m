function trigPos = nxeGetTriggers(filename,trigLine,trigEdge)


%trigPos = nxeGetTriggers(filename,trigLine,trigEdge)
%
%trigPos - trigger positions in bytes
%filename - eXimia EEG raw data file
%trigLine - either 1(GATE), 2(TRIG1) or 3(TRIG2)
%trigEdge - either 'rising' or 'falling'

fid=fopen(filename,'r','l');


numChannels = 64;
blockSamples = 1000;%1450 ge�ndert
trigThreshold = 2000;


fseek(fid,0,'eof');
numBytes = ftell(fid); 
numSamples = (numBytes/2)/numChannels;
numBlocks = ceil(numSamples/blockSamples);


fseek(fid,0,'bof');
trigPos=[];

for i = 1:numBlocks
    
    fPos = ftell(fid);
    data = fread(fid,[numChannels blockSamples+1],'int16');   
    switch trigEdge
      case 'rising',trigPos = [trigPos, find(diff(data(trigLine,:))>trigThreshold).*2*numChannels+fPos];
      case 'falling',trigPos = [trigPos, find(diff(data(trigLine,:))<-trigThreshold).*2*numChannels+fPos];
      otherwise, break;
    end        
    fseek(fid,-(2*numChannels),'cof');      
end


fclose(fid);

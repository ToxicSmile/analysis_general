function [data_seg,timeAxis] = bvGetTrialsamp(filename,newsamp,tPosi,timeRange,baselineRange)

%function [data,timeAxis] = nxeGetTrial(filename,filePosition,timeRange,baselineRange)
%
%data - data matrix [channels samples]
%taxis - time axis in ms
%filename - eXimia EEG raw data filename
%filePosition - in bytes
%timeRange - in milliseconds e.g. [-100 500]
%baselineRange - in milliseconds e.g. [-100 0]

if nargin<4,end

numChannels = 64;
timeSf = 5.00; 
tRange = round(timeRange.*timeSf);
bLine = round(baselineRange.*timeSf);
duration = diff(tRange)+1;

fid = fopen(filename,'r','l');                   
data = fread(fid,[newsamp numChannels],'single');
fclose(fid);

data_seg = data(tPosi+tRange(1):tPosi+tRange(2),:);  
data_seg = data_seg';


timeAxis = ((0:size(data_seg,2)-1)+tRange(1))/timeSf;

fclose('all');

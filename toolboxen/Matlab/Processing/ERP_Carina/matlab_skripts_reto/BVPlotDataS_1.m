function bvPlotDataS_1(path, filename, timeAxis,averageData,yRange,averageData2)

%function nxaPlotData(timeAxis,averageData,yRange)
%
%timeAxis - time scale in milliseconds
%averageData - data matrix in microvolts (channels x samples)
%yRange - in microVolts e.g. [-20 20]
%averageData2 - optional average data

if nargin<3, end;
if nargin==3, averageData2=[];end;

cd (path);


load (filename, '-mat')

set(gcf,'color',[1 1 1]);

k=0;
for i=1:64,
    
  k=k+1;
  h(k)=axes('position',[EEG.chanlocs(1,i).X EEG.chanlocs(1,i).Y 0.07 0.05],...
            'ycolor',[0 0 0],...
            'xcolor',[0 0 0],...
            'ydir','normal',...
            'xlim',[min(timeAxis) max(timeAxis)],...
            'ylim',yRange,...
            'visible','off');
        
  l=line(timeAxis,averageData(i,:),...
      'clipping','off',...
      'color','b',...
      'userdata',i);   
  
  if i==1, set(l,'color','r'); end
  
  if ~isempty(averageData2),                 
    l=line(timeAxis,averageData2(i,:),...
           'clipping','off',...
           'linestyle','-',...
           'color',[1 0 0]);
  end     

  line([0 0],[-1000 1000],...
      'linestyle',':',...
      'color','k');
  
  
end

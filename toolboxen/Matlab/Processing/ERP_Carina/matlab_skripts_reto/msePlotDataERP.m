function msePlotData(tAxis,data,channels,ySeparation,eogLimit)

%nxePlotData(tAxis,data,channels,ySeparation)
%
%tAxis - time scale in ms, e.g. -100...500ms
%data - data matrix (channels  x samples)
%channels - displayed channels, e.g. [1 3:60]
%ySeparation - Y axis separation in microvolts, e.g. 100 
if nargin<4, end

clf;hold on;
    
k=0;
for i=fliplr(channels),
    l = line(tAxis,data(i,:)+(k+0.5)*ySeparation);
%     l = line(tAxis,data(i,:)+(k+0.5)*ySeparation);
    switch i
        case 200, set(l,'color','k')    
        case 200, set(l,'color','r');
        otherwise, set(l,'color','b');
    end
    k=k+1;
end
j=0;
% if max(abs(data(4,:))) > eogLimit
%     l = line(tAxis,data(4,:)+(k+0.5)*ySeparation);
%     set(l,'color','r');
%     j=1;
% end
w=15;h=15.5;%adjust h and w if display is not fitting properly on screen
%set(gcf,'color',[1 1 1],'PaperPosition',[0 0 w h+h*(j*2/k)],'Position',[100 100-h*(j*2/k) w*100 h*100]);

set(gcf,'color',[1 1 1],'PaperPosition',[0 0 w h+h*(j*2/k)],'Position',[100 50-h*(j*2/k) w*120 h*60]);
set(gca,'pos',[0 0 1 1],...
        'ydir','reverse',...
        'visible','off',...
        'xlim',[min(tAxis) max(tAxis)],...
        'ylim',[0 (j*2+k)*ySeparation]);

l=line([0 0],[-1000000 1000000]);
set(l,'linestyle','--','color','k');

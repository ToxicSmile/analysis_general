function [npower]=speciallpas(filename,visfile,channelnr)

% clear all;
% filename='LTP_AW_07_06_2005_7_8';
% channelnr=9
trk=1;

eval(['load E:\data\PAS\',filename,'.mat'])

VisName=['E:\data\scoring\',visfile,'.VIS'];
[vistrack,vissymb,offset]=readtrac(VisName,trk);
num20=length(vissymb);
vissymb=vissymb(1:num20);
num20=length(vissymb);
num20_4=floor(size(artndxn,2)/5);
if num20>num20_4
    num20=num20_4;
end
vis20=vissymb(1:num20);

vis4s=[vis20; vis20; vis20; vis20; vis20];
vis4s=vis4s(1:size(vis4s,1)*size(vis4s,2)); 
vis4s=vis4s';
indexnr4s=find(vis4s=='2' | vis4s=='3' | vis4s=='4')';

ndxgoodch=find(sum(artndxn')>0);
nchgood=length(ndxgoodch);

artndxx=find(sum(artndxn(ndxgoodch,:))==nchgood);               %%%%artefacts rejection in all channel same
channelndx=intersect(artndxx,indexnr4s);

if length(channelndx)>300                  %only first 30 min
    channelndx=channelndx(1:300);
end

mfft=[];
for channel=1:60
    fft=squeeze(ffttot(channel,:,:));
    chbfft=mean(fft(:,channelndx)');
    mfft=[mfft;chbfft];
end

% figure
% plot(mfft(ndxgoodch,:)')
% title(filename)
% pause
% close all

channelnrgood=intersect(channelnr,ndxgoodch);

if isempty(channelnrgood)
    npower=NaN(1,161)
else
    for ch=1:size(channelnrgood,2)
%         chpower(ch,:)=mfft(channelnrgood(ch),:)./nanmean(mfft(ndxgoodch,:));         %normalize
        chpower(ch,:)=mfft(channelnrgood(ch),:);         
    end

    if size(channelnr,2)>1
        npower=mean(chpower);
    else
        npower=chpower;
    end
end

% npower=mfft(ndxchannelgood,:);


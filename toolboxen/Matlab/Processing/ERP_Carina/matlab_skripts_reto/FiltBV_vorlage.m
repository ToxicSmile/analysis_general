function  FiltBV (path, fname);



cd (path);
load (fname);

Samp_Rate=EEG.srate;
nchan=EEG.nbchan;
dataFormat='single';

locutoff=0.5;
hicutoff=100;

data=EEG.data;

filenameout=[path ,fname,'.nxf'];
fidout=fopen(filenameout,'wb');


for channel=1:nchan;
    channel;
    
%     datad=double(data(channel,:));
% 
%     dataf=eegfilt(datad,Samp_Rate,locutoff,0);
% %     dataf3=eegfilt(datad,Samp_Rate,0,hicutoff);
%     dataf2=eegfilt(dataf,Samp_Rate,0,hicutoff);
% %     dataf2=eegfilt(data(channel,:),Samp_Rate,0,hicutoff);
    
    fwrite(fidout,data(channel,:),dataFormat);
end
fclose(fidout);

AddInfoFname = sprintf('%s%s_add',path,fname);

newsamp=length(data);
fsnxf=Samp_Rate;

for i=1:length(EEG.urevent)
    events(i,:)=EEG.urevent(i).latency;
end

%eval(['save E:\data\Rositsa_test\test.acc', newsamp fsnxf locutoff hicutoff,' -mat']);
save (AddInfoFname,'newsamp', 'fsnxf', 'events');
% eval(sprintf('save ''%s'' newsamp, fsnxf, events',AddInfoFname));
% save('\\fl-daten\NOS_Forschungen\NOS-Schlaflabor\Forschung\BrainAmp\Tests\VEP_AM\test_IZ_0108\test_IZ_0108.mat', 'newsamp', 'fsnxf', 'locutoff', 'hicutoff', 'events');
% save test_filter
fclose all;

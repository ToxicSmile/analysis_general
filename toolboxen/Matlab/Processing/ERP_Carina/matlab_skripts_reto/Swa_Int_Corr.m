%rh, 22.3.07
%correlation between SWA and integral of TMS/EEG response
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

load swaLtp.mat -mat
npower1ltp=npower1;
load swaLtd.mat -mat
npower1ltd=npower1;
npower1=[npower1ltp;npower1ltd];

load Int50_100Ltp.mat -mat
diffintltp=diffint;
load Int50_100Ltd.mat -mat
diffintltd=diffint;
diffint=[diffintltp;diffintltd];


%%%%correlation
for ch=1:60
    ch;
    ndxgoodsubjects_swa=find(isfinite(npower1(:,ch))==1); 
    ndxgoodsubjects_int=find(isfinite(diffint(:,ch))==1); 
    ndxgoodsubjects=intersect(ndxgoodsubjects_swa,ndxgoodsubjects_int);
    if length(ndxgoodsubjects)>4
        [R,P]=corrcoef(npower1(ndxgoodsubjects,ch),diffint(ndxgoodsubjects,ch));
        rch(ch)=R(1,2);
        pch(ch)=P(1,2);
%         figure
%         plot(rpow(ndxgoodsubjects,ch),ogmfp2(ndxgoodsubjects),'ro')
%         title(['channel ',num2str(ch),'   r=',num2str(R(1,2)),'   p=',num2str(P(1,2))])
%         pause
%         close all
    else
        rch(ch)=NaN;
        pch(ch)=NaN;
    end
end

ndxokr=isfinite(rch);
ndxokp=isfinite(pch);

figure
subplot(2,1,1)
matnetneu(ndxokr);
topoplot(rch(ndxokr),'/Users/retohuber/e_user/wispic/human/matlab/tmseeg/pas/test.txt','maplimits','maxmin');%,'maxmin');%,'electrodes','labels');
colorbar
subplot(2,1,2)
matnetneu(ndxokp);
topoplot(pch(ndxokp),'/Users/retohuber/e_user/wispic/human/matlab/tmseeg/pas/test.txt','maplimits',[0 0.1],'electrodes','labels');

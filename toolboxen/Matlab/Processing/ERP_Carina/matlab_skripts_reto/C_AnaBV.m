clear all; close all;

[name path] = uigetfile('*.nxf', 'Select data file');
cd (path)

name=name(1:end-4);
filenameraw=[path,name,'.nxf'];
filenameacc=[path,name,'.acc'];

[after,timeAxis]=bvGetExpAve(filenameraw, filenameacc, []);  %ltp
gmfpb(1,:)=sqrt(nanmean(after.^2));


el=37;
figure('Color',[1 1 1])
plot(timeAxis,after(el,:),'r')

figure('Color',[1 1 1])
plot(timeAxis,gmfpb,'LineWidth',2)
% hold on
% plot(timeAxis,gmfpa,'LineWidth',2,'Color',[1 0 0])
xlabel('ms')
ylabel('microV')
yaxis([0 2.5])
line([0 0],[0 2.5],'Color','k')

figure
bvPlotDataS_1(timeAxis,before,[-2 2],after)


topo=sum(after(:,25:50)');
ndxok=isfinite(topo);

figure
% matnetneu(ndxok);
topoplot(topo(ndxok),'C:\MATLAB_AM\MATLAB_AM\ScriptsforTMS_Milano','maplimits','maxmin');%,'maxmin');%,'electrodes','labels');
colorbar


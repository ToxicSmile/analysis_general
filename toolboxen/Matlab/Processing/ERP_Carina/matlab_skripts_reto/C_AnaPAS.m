clear all; close all;


[after,timeAxis]=nxeGetExpAve('cPAS_AB_2006_08_03','2',[]);  %ltp
gmfpb(1,:)=sqrt(nanmean(after.^2));


el=37;
figure('Color',[1 1 1])
plot(timeAxis,after(el,:),'r')

figure('Color',[1 1 1])
plot(timeAxis,gmfpb,'LineWidth',2)
hold on
plot(timeAxis,gmfpa,'LineWidth',2,'Color',[1 0 0])
xlabel('ms')
ylabel('microV')
yaxis([0 2.5])
line([0 0],[0 2.5],'Color','k')

figure
nxaPlotDataS(timeAxis,before,[-2 2],after)


topo=sum(after(:,25:50)');
ndxok=isfinite(topo);

figure
matnetneu(ndxok);
topoplot(topo(ndxok),'E:\neuro\matlab\test.txt','maplimits','maxmin');%,'maxmin');%,'electrodes','labels');
colorbar


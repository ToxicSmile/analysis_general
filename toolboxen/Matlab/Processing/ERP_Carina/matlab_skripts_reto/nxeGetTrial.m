function [data,timeAxis] = nxeGetTrial(filename,filePosition,timeRange,baselineRange)

%function [data,timeAxis] = nxeGetTrial(filename,filePosition,timeRange,baselineRange)
%
%data - data matrix [channels samples]
%taxis - time axis in ms
%filename - eXimia EEG raw data filename
%filePosition - in bytes
%timeRange - in milliseconds e.g. [-100 500]
%baselineRange - in milliseconds e.g. [-100 0]

if nargin<4,end

sfEEG = (1/2000) * (10/65535) * 1000000;        
sfEOG = (1/400) * (10/65535) * 1000000;         
sfTRIG = 10 * (10/65535);                               

numChannels = 64;
timeSf = 1.45; 
tRange = round(timeRange.*timeSf);
bLine = round(baselineRange.*timeSf);
duration = diff(tRange)+1;

fid = fopen(filename,'r','l');                   
fseek(fid,filePosition+2*numChannels*tRange(1),'bof');
data = fread(fid,[numChannels duration],'short');                          
fclose(fid);

blStart = diff([tRange(1) bLine(1)])+1;
blStop = diff([tRange(1) bLine(2)])+1;
for i=4:numChannels,
    data(i,:) = data(i,:)-mean(data(i,blStart:blStop)); 
end

data(1:3,:) = sfTRIG.*data(1:3,:);
data(4,:) = sfEOG.*data(4,:);
data(5:64,:) = sfEEG.*data(5:64,:);
timeAxis = ((0:size(data,2)-1)+tRange(1))/timeSf;


fclose('all');

function [aveF,timeAxis]=nexGetExpAveRaf(name,session,addbad);

% clear all; close all;
% name='PAS_LTP_JR_2006_06_21';
% session='3';

timeRange = [-50 250];
baselineRange = [-50 0];
% timeRange = [-20 100];
% baselineRange = [-20 0];
nch=60;

filenameraf=['F:\data\human\PAS\',name,'\',name,'_',session,'.nxf'];
filenameacc=['F:\data\human\PAS\',name,'\',name,'_',session,'.acc'];

eval(['load ',filenameacc,' -mat']);


goodCh=1:1:nch;

if rejectCh~=0
    rejectCh=union(rejectCh,addbad);  %%%%remove additional bad channels
    goodCh(rejectCh)=[];
elseif ~isempty(addbad)
    rejectCh=addbad;  %%%%remove additional bad channels
    goodCh(rejectCh)=[];
end

fidraf=fopen(filenameraf,'rb');
data=fread(fidraf,[newsamp,nch],'int16=>int16');

timeSf = fsnxf/1000;
tRange = round(timeRange.*timeSf);
bLine = round(baselineRange.*timeSf);

acceptedPos=acceptedPos/(2*64); %%%%correction, see nxeGetTriggers.m

for i=1:length(acceptedPos)
    dataPos=data(acceptedPos(i)+tRange(1):acceptedPos(i)+tRange(2),:);
    dataPos=double(dataPos');
    
    %Bl correct
    blStart = diff([tRange(1) bLine(1)])+1;
    blStop = diff([tRange(1) bLine(2)])+1;
    for j=1:nch
        dataPos(j,:) = dataPos(j,:)-mean(dataPos(j,blStart:blStop));
    end

    if i == 1;
        ave = zeros(size(dataPos));
    end
    %Ave Ref
    aveRef=mean(dataPos(goodCh,:));
    for k=1:nch
        dataPos(k,:)=dataPos(k,:)-aveRef;
    end
    ave = ave + dataPos;
end
aveF=ave(1:60,:);
aveF = aveF / length(acceptedPos);

if rejectCh~=0
    aveF(rejectCh,:)=NaN;
end

timeAxis = ((0:size(aveF,2)-1)+tRange(1))/timeSf;

% figure
% nxaPlotDataS(timeAxis,aveF,[-2 2])



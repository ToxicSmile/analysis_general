function [diffint,beforeint,afterint,gmfpb,gmfpa]=nexGetExpAve(fname,sessions,addbad,startlat,stoplat);
disp('/');
% clear all;
% fname='PAS_LTD_RK_2006_11_30';
% sessions=[1 6];
% startlat=147; stoplat=291;  %%%%50-150ms

session=num2str(sessions(1));
[before,timeAxis]=nxeGetExpAve(fname,session,addbad);
session=num2str(sessions(2));
[after,timeAxis]=nxeGetExpAve(fname,session,addbad);

%%%%integral
afterint=trapz(abs(after(:,startlat:stoplat))');
beforeint=trapz(abs(before(:,startlat:stoplat))');

% diffint=afterint./beforeint;
diffint=afterint-beforeint;

%%%%gmfp
gmfpb=sqrt(nanmean(before(:,1:364).^2));
gmfpa=sqrt(nanmean(after(:,1:364).^2));


%% Script for ICA component rejection and bad epoch rejection
% results in a .mat file with pruned data, and a _add2 .mat file containing
% information about good epochs
% AM 12.3.15
% AM 17.4.15 modifications for ICA
 
%%ICA component rejection

clear all;
eeglab;
clear all; close all;


%Adjust according to file name and path!!
filename='ST20FG_2m201503180549_ICA.set';
datafilepath='D:\Data_AEP\ICA\VP1\ST20FG\baseline\morning';
% locfilename='E:\MatlabScripts\ERP_JUNE_Filtertest2\otherscripts_needed_ERP\ScriptsforTMS_Milano\Brainamp62.elp';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

eeglabpath=datafilepath;
cd(datafilepath);
% [fname path] = uigetfile('*.mat', 'Trial data file');
% load (fname, 'chanlocs');
% % % % chanlocs_109 = chanlocs
EEG = pop_loadset('filename',filename,'filepath',eeglabpath);
% load('D:\Matlab\template_EEG_locfile\EEG_template_TFCE_eeglab')

eeglab redraw;


pause;
%%% identify artifact components by: Plot -> component activations(scroll)
%%% and: Tools -> reject data using ICA ->reject components by maps
 
%%% remove artifact components by: Tools -> remove components
%%% compare raw data with ICA cleared date to see, if signal got worse or
%%% better: plot data channel scroll
%%% press any key to continue after ICA component removal is done and
%%% checked and data set with final components removed is open in EEGlab
 
name= filename(1:end-8);
newfilename=[name,'_ICAcorr', '.mat'];
 
cd (datafilepath)
save (newfilename, 'EEG', '-v7.3')

clear all; close all;

 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 

%%% Splitting ERP data, remove and interpolate bad channels, average
%%% referencing, Baseline Correction, calculation of ICA
% data after BL-correction are stored in an .mat file, after ICA in an
% eeglab setfile --> no components removed or inspected yet!!
% CV 17.11.15


clear all
eeglab
clear all; close all;
%vispath='C:\user\Caroline\HEMF_Daten\EGIprocessed\VP29\'; %output

%%%%%%% CHANGE ACCORDING TO YOUR PATH!!!!!
% locfilename='E:\MatlabScripts\ERP_JUNE_15\otherscripts_needed_ERP\ScriptsforTMS_Milano\Brainamp62.elp';
% locfilepath='E:\MatlabScripts\ERP_JUNE_15\otherscripts_needed_ERP\ScriptsforTMS_Milano\';
datapath='D:\Data_AEP\ICA\VP1\';%where subject folders are stored
% eeglabpathbase='E:\\ERP_DM_VP_data\\NUR_RiskTask_Eingeschlossen\\ScriptTests\\Final\\TEST_FINAL\\';%where subject folders are stored: double backslash important!!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

content_folder1=dir(datapath)
content_folder1(1:2)=[];

for f=1:length(content_folder1)
    cd([datapath,content_folder1(f).name])
    content_folder2=dir([datapath,content_folder1(f).name])
    content_folder2(1:2)=[];
for g=1:length(content_folder2)
    content_folder3=dir([datapath,content_folder1(f).name,'\',content_folder2(g).name,'\'])
    content_folder3(1:2)=[];
for h=1:length(content_folder3)
    
cd([datapath,content_folder1(f).name,'\',content_folder2(g).name,'\',content_folder3(h).name,'\']);

content_mat=dir('*_RawDataInspect.mat');
% content_badchan=dir('');
name=[content_mat(1).name];
% name2=[content_badchan(1).name];
path=[datapath,content_folder1(f).name,'\',content_folder2(g).name,'\',content_folder3(h).name,'\'];

% eeglabpath=[eeglabpathbase,content_folder1(f).name,'\\',content_folder2(g).name];
% name=[content_mat(1).name];
% name2=[content_badchan(1).name];



load(content_mat(1).name);
% load(name2,'badchan');

data=double(EEG.data);
EEG.data=data;

%% ICA (calculate components)
% eeglab

% EEG.chanlocs=readlocs(locfilename);
% EEG.chanlocs=chanlocs;

EEG.ref='common';
eeglab redraw


% allchan=[1:62];
% chanind=setdiff(allchan,badchan);
EEG.bad_channels = badchan;
 
EEG = pop_select(EEG, 'nochannel', EEG.bad_channels);
% 
% EEGICA=pop_runica(EEG,'icatype','runica','pca',61);
% EEGICA=pop_runica(EEG,'icatype','runica','extended',1);

EEGICA=pop_runica(EEG,'icatype','runica','extended',1,'stop',1E-7);
% EEGICA=pop_runica(EEG,'icatype','runica','extended',1,'stop',1E-7,'chanind',chanind);

EEG=EEGICA;
EEG = pop_saveset( EEG, 'filename',[name(1:end-18),'ICA.set'],'filepath',path);
% 
clear EEG* dati* events data datainterp badchan
end
    end
end

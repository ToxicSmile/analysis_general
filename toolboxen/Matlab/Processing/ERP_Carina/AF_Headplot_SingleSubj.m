%%%Interpolate LinRise SWA at transitions%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% alm 19/02/15
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---Input------------------------------------------------------------------

filepath = 'F:\Affoltern\';
%filepath = 'G:\EGI\';
session = 'session1';
subject = '12SH_AF';
%subject = '062AD_Dev';

%----------------------------------------------------------------------
%%%Load files%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

content_Trans = dir([filepath,subject,'\',session,'\','TransitionsSWANight*']);  
load([filepath,subject,'\',session,'\',content_Trans(1).name],'MLinRiseMax_SWA_St3','SWA_NonREM_MNight')

load('C:\Matlab\alm\EEG_Templates\EEG_template_TFCE_eeglab')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%adapt eeglab structure: 1 subject, 128 channels%%%%%%%%%%%%%%%%%%%%%%%%%

EEG.times = [];
EEG.times = 1;
EEG.nbchan = 128;
EEG.pnts = 1;
EEG.xmax = 0;
EEG.chanlocs(129) = [];

EEG.data = MLinRiseMax_SWA_St3';
%EEG.data = SWA_NonREM_MNight';

EEG = readegilocs(EEG); 

SIGTMPAVG = EEG.data';

%%%plot headplot%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
headplot( SIGTMPAVG(:), 'C:\Matlab\alm\Sleep\test_file.spl', 'maplimits', 'maxmin', 'view',[0 90]);
colorbar

%%%save figure%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
saveas(gcf,['F:\Affoltern\Figures\',subject(1:end-4),' S',session(end),' BU headplot'], 'jpg');    



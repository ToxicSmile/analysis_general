clear all;


[avgdata_e]=newGetExpAveRaf('aep_adu\AG\02_AG200804022309010');  %evening
[avgdata_m]=newGetExpAveRaf('aep_adu\AG\01_AG200804030649002');  %morning



%%%%topoplot, all electrodes
timeAxis = ((0:size(data_mn1,2)-1)+(-100));
% figure;newPlotData(timeAxis,avgdata_e, [-5 5]);

%%%%single electrode
el=112;   %%%%electrode of interest
figure
plot(timeAxis,avgdata_e(el,:),timeAxis,avgdata_m(el,:),'r')

%%%%global mean field power
gmfp_e=sqrt(nanmean(avgdata_e(1:128,:).^2));
gmfp_m=sqrt(nanmean(avgdata_m(1:128,:).^2));

figure
plot(timeAxis,gmfp_e,timeAxis,gmfp_m,'r')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%peak detection
f=timeAxis;

%%%%time of interest
f1=0;
f2=600;

peaklat=zeros(1,2);
peakpow=zeros(1,2);

close all;
[df,ds]=PWPEAKSL(gmfp_e,3,f,f1,f2,'evening');
peaklat(1,1)=df;
peakpow(1,1)=ds;
close all;
[df,ds]=PWPEAKSL(gmfp_m,3,f,f1,f2,'morning');
peaklat(1,2)=df;
peakpow(1,2)=ds;
close all;

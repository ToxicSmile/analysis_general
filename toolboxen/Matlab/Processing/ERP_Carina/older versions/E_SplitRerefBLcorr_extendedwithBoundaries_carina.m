%%% Splitting ERP data, remove and interpolate bad channels, average
%%% referencing, Baseline Correction, calculation of ICA
% data after BL-correction are stored in an .mat file, after ICA in an
% eeglab setfile --> no components removed or inspected yet!!
% CV 03.12.15
% AM modifications in ICA precedure
 
clear all
clear all; close all;
 
%%%%%%% CHANGE ACCORDING TO YOUR PATH!!!!!
datapath='D:\Data_AEP\ICA\testinterpol\';%where subject folders are stored
load D:\Matlab\template_EEG_locfile\e_loc_109.mat % load channel location
chanlocs = chanloc_109; %changes variable name of channel file
clear chanloc_109 % clears odl variable 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
preepoch=200; %INSERT VALUES IN CASE THEY ARE FRACTION OF sr!!!!!!!
postepoch=500;
 
content_folder1=dir(datapath)
content_folder1(1:2)=[];


for f=1:length(content_folder1)
    cd([datapath,content_folder1(f).name])
    content_folder2=dir([datapath,content_folder1(f).name])
    content_folder2(1:2)=[];
for g=1:length(content_folder2)
    content_folder3=dir([datapath,content_folder1(f).name,'\',content_folder2(g).name,'\'])
    content_folder3(1:2)=[];
for h=1:length(content_folder3)
cd([datapath,content_folder1(f).name,'\',content_folder2(g).name,'\',content_folder3(h).name,'\'])
 
content_data=dir('*ICAcorr.mat');

name=[content_data(1).name]; 

path=[datapath,content_folder1(f).name,'\',content_folder2(g).name,'\',content_folder3(h).name,'\'];
 
cd (path)

load(content_data(1).name);


%%
 
dati=EEG.data;
sr=EEG.srate;
bad_elec=EEG.bad_channels;
 
allevents=zeros(size(EEG.event));
allevents_code=zeros(size(EEG.event));
%extract location of events of all presented stimuli, but not of Boundaries
%or other marker
 
 
for i=1:length(EEG.event)
    if strcmp(EEG.event(i).type,'chan130')
    allevents(i)=EEG.event(i).latency;
    allevents_code(i)=EEG.event(i).urevent;
    end
end
allevents(allevents == 0) = [];
allevents_code(allevents_code == 0) = [];
 
allboundaries=zeros(size(EEG.event));
for i=1:length(EEG.event)
    if strcmp(EEG.event(i).type,'boundary')
    allboundaries(i)=EEG.event(i).latency;
    end
end
allboundaries(allboundaries == 0) = [];
 
 
for i=1:length(allboundaries)
    for j=1:length(allevents)
        if allevents(j)> allboundaries(i) && allevents(j) < allboundaries(i)+preepoch
            allevents(j)=0;
            allevents_code(j)=0;
        elseif allevents(j) < allboundaries(i) && allevents(j)>allboundaries(i)-postepoch
            allevents(j)=0;
            allevents_code(j)=0;
        end
            
    end
end
 
allevents(allevents == 0) = [];
allevents_code(allevents_code == 0) = [];
 
%% Split
 
epochprepost= preepoch+postepoch;
datisplit=[];

if allevents(end)+sr > length(EEG.data)
    allevents(end)=[];
    allevents_code(end)=[];
end
    
for i=1:length(allevents)
    temp=dati(:,(allevents(i)-preepoch):(allevents(i)+postepoch));
    datisplit=cat(3,datisplit,temp);
end

% else
    
%     for i=1:(length(allevents)-1)
%     temp=dati(:,(allevents(i)-preepoch):(allevents(i)+postepoch));
%     datisplit=cat(3,datisplit,temp);
%     end
% end
    
dati=datisplit;
clear datisplit temp
 
%% Interpolate
 
% cd(locfilepath)
% EEG=eeg_emptyset;
 
EEG.specdata=[];
EEG.icachansind=[];
EEG.specicaact=[];
EEG.reject=[];
EEG.stats=[];

events = EEG.event;     %% save events/urevents and empty in EEG.structure to avoid problem in EEGLAB
urevents = EEG.urevent;
xmax = EEG.xmax
icasphere = EEG.icasphere

EEG.icasphere = []
EEG.event = [];         
EEG.urevent  = [];
EEG.xmax = []


EEG.nbchan=size(dati,1);
EEG.data=dati;
% EEG.chanlocs=chanlocs;
EEG.trials=size(dati,3);
EEG.pnts=size(dati,2);
EEG.srate=sr;
 
method='spherical';
clear dati

if ~isempty(bad_elec)
    EEGOUT = eeg_interp(EEG, chanlocs, method);
%    EEGOUT = eeg_interp_SS(EEG, readlocs(locfilename), method)
   dati=EEGOUT.data;
else
   dati=EEG.data;
end

EEG.events = events; %fill events/urevents into EEG.structure
EEG.urevent  = urevents; 
EEG.icasphere = icasphere;
EEG.xmax = xmax

clear EEGOUT

 
%% AVG Reference
dataICAcorrAVGref=[];
for i=1:size(dati,3)
    tempepoch=squeeze(mean(dati(:,:,i),1));
    tempepoch=repmat(tempepoch,size(dati,1),1);
    epoch=dati(:,:,i)-tempepoch;
    dataICAcorrAVGref=cat(3,dataICAcorrAVGref,epoch);
end
clear tempepoch epoch
 
 
%% Baseline Correction AVG ref
 
dataBLcorrICAAVGref = dataICAcorrAVGref;
baseline = preepoch - 50;
 for i = 1:size(dataICAcorrAVGref, 1)
    for j = 1:size(dataICAcorrAVGref, 3)
        dataBLcorrICAAVGref(i,:,j) = dataICAcorrAVGref(i,:,j) - mean(dataICAcorrAVGref(i, 1:baseline,j));
    end
 end


newfilename=[name(1:end-12),'_afterComponentRejectionInterpAverefBLcorr.mat'];
cd (path)
save (newfilename, 'dataBLcorrICAAVGref', 'allevents_code', 'preepoch', 'postepoch', 'epochprepost','sr');
 
     end
end
end


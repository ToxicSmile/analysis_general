%AM 17.04.15
%modifications for ICA: when inspecting epochs for bad channel
%identification, epochs with large artifacts should be removed and excluded
%from further analysis

clear all;close all;
%%Adjust according to your path:

path='D:\Data_AEP\test_mydata\ST03CR\baseline\evening\';

% % decide if will be resuming or not, then set up file saving
% toResume = 0;
% while min(toResume ~= ['n' 'N' 'r' 'R'])
%     toResume = input('(n)ew, (r)esume: ','s');
% end

%File to get data from, name of save file determined automatically based on
%load file name
%ADJUST PATH!!!!

cd (path)
[fname path] = uigetfile('*.mat', 'Trial data file');
% [fname path] = uigetfile('*.mat');
% [fname, path] = uigetfile('C:\user\Angelina\Daten\TMS_EEG\VP_02\SR1\*.nxf', 'Trial data file');

load(fname)

data=double(data);
%% reverse channels due to reverse amplifier connecting
% data=double(EEG.data);
% data(1:64,:);
% datanew=zeros(size(data));
% datanew(1:32,:)=data(33:64,:);
% datanew(33:64,:)=data(1:32,:);
% data=datanew;

% data_risk=data;
data([43 48 49 56 63 68 73 81 88 94 99 107 113 119 120 125 126 127 128],:)=[]; %remove eye channels from data structure
newsamp=length(data);
% EEG.data=data_risk;

eeglab redraw
pause;

%%%%
%define bad channels:
badchan = input('Channels to reject: ');

newfilename=[fname(1:end-4),'_RawDataInspect','.mat'];

cd(path)
save(newfilename,'EEG','badchan','-v7.3')

close all;
clear all;




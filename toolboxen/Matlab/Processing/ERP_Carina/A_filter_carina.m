%% Loop to filter Risk Task - ERP files, to extract event marker and to write binary EEG file .nxf
% possibility to read in excel sheet to exclude trials which are rejected
% due to behavioral response (too short / long reaction times)
% TO CHECK IN THE MARKER FILE (.vmrk): are stimuli coded as 'Stimulus'?? If
% not adapt line 91

% results in files: .nxf containing binary EEG file, .mat file containing
% EEG matrix and _add.mat containing 'newsamp', 'fsnxf',  'events'); 

% CV 05.11.2015 

clear all; close all;
% eeglab
% close all

%%% Adjust according to your path:
EEGDatapath='D:\Data_AEP\test_mydata\';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


content_folder1=dir(EEGDatapath)
content_folder1(1:2)=[];


for f=1:length(content_folder1)
    cd([EEGDatapath,content_folder1(f).name])
    content_folder2=dir([EEGDatapath,content_folder1(f).name])
    content_folder2(1:2)=[];
for g=1:length(content_folder2)
    content_folder3=dir([EEGDatapath,content_folder1(f).name,'\',content_folder2(g).name,'\'])
    content_folder3(1:2)=[];
for h=1:length(content_folder3)

cd([EEGDatapath,content_folder1(f).name,'\',content_folder2(g).name,'\',content_folder3(h).name,'\'])

% eeglabpath=[EEGDatapath,content_folder1(f).name,'\',content_folder2(g).name,'\']
% content_trial=dir('*.vhdr');
% % EEGDatapath=eeglabpath;
% eeglabfilename=content_trial(1).name
% 
% EEG = pop_loadbv(eeglabpath, eeglabfilename);
% %EEG = pop_loadbv('C:\Documents and Settings\LUSC\Desktop\TMS_EEG_Angelina\', 'VP_37_TEP_screen_05.vhdr', [1 4306900], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64]);
% EEG = eeg_checkset( EEG );

contentraw=dir('*.raw');
filepath=[EEGDatapath,content_folder1(f).name,'\',content_folder2(g).name,'\',content_folder3(h).name,'\'];
rawfilename=[filepath,contentraw(1).name];
[EEGdataFormat, header_array, EventCodes, Samp_Rate, NChan, scale, NSamp, NEvent, data] = loadEGIRaw(rawfilename);

%% EEGData filtering
% EEG = pop_eegfiltnew( EEG, 45, 55, [], [1]); %notch
% EEG = pop_eegfiltnew( EEG, 0.1, 0, [], [0]); %hp
% EEG = pop_eegfiltnew( EEG, 0, 40, [], [0]); %lp
% Samp_Rate=EEG.srate;

%% Data filtering
% data = double(data);
EEGData = data(1:128,:); % EEGData = Data without Events (do not filter events!)
                            % Ch 130 events without response (360)
                            % Ch 131/132 events with response/responses
                            % Ch 133 start/ending event

hp=0.8; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
lp=30; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


w2=2*lp/Samp_Rate;
w=[w2];
[B,A]=butter(4,w,'low');

w2=2*hp/Samp_Rate;
w=[w2];
[C,D]=butter(4,w,'high');
for kk=1:size(EEGData,1)
    a=filtfilt(B,A,EEGData(kk,:));
    a=filtfilt(C,D,a);
    datifilt(kk,:)=a;
end

   
    
clear w w2 C D kk a EEGData
clear w w2 A B C D kk a EEGData
EEGData=datifilt;

EEGData=single(EEGData);
clear datifilt




%% readout events

tPos = egiGetTriggersCorr(rawfilename);
events=tPos;


% newsamp=length(EEGData); 

% allevents=[];

%extract location of events of all presented stimuli, but not of Responses
%or other marker
% for i=1:length(EEG.event)
%     if strcmp(EEG.event(i).code,'Stimulus')
%     allevents(i)=EEG.event(i).latency;
%     else
%     end
% end
% allevents(allevents == 0) = [];
% 
% % define the last 352 stimuli (corresponding to the Risk Task)
% allevents=(allevents)';
% events=allevents((length(allevents)-351):length(allevents));
% 
% 
% 
% % only keep EEGData from risk task
% startrisk=events(1)-Samp_Rate;
% endrisk=(events(length(events)))+(2*Samp_Rate);
% EEGData_risk=EEG.EEGData(:,startrisk:endrisk);
% EEGData_risk([31 32],:)=[]; %remove eye channels from EEGData structure
% newsamp=length(EEGData_risk); 
% 
% EEG.EEGData=EEGData_risk;
% EEG.pnts=length(EEGData_risk);
% EEG.times=0:(length(EEGData_risk)-1);
% EEG.event_ori=EEG.event;
% EEG.nbchan=62;
% 
% EEG.event=[];
% EEG.urevent=[]
% 
% for i=1:length(events)
%     EEG.event(i).latency=events(i)-startrisk;
%     EEG.event(i).duration=1;
%     EEG.event(i).channel=0;
%     EEG.event(i).bvtime=[];
%     EEG.event(i).bvmknum=(i);
%     EEG.event(i).type='S';
%     EEG.event(i).code=num2str(i);
%     EEG.event(i).urevent=(i);
% end
% 
% for i=1:length(events)
%     EEG.urevent(i).latency=events(i)-startrisk;
%     EEG.urevent(i).duration=1;
%     EEG.urevent(i).channel=0;
%     EEG.urevent(i).bvtime=[];
%     EEG.urevent(i).bvmknum=(i);
%     EEG.urevent(i).type='S';
%     EEG.urevent(i).code=num2str(i);
% end


%% save splitted file
Datafilt = NaN(size(data)); %%%Datafilt contains filtered Data & unfiltered events
Datafilt(1:128,:) = EEGData; 
% Datafilt(130:133,:) = data(130:133,:);
Datafilt(129:133,:) = data(129:133,:);


save([rawfilename(1:end-4),'.mat'],'EEGData', 'Datafilt' , 'data', 'EEGdataFormat', 'header_array', 'EventCodes','Samp_Rate', 'NChan', 'scale', 'NSamp', 'NEvent','events','-v7.3')

cd([EEGDatapath,content_folder1(f).name,'\',content_folder2(g).name,'\',content_folder3(h).name,'\'])
% content_mat=dir('*.mat');
% content_xls=dir('*.xlsx');

% filepath=eeglabpath;
% filename_eventEEG=content_mat(1).name;
% filename=[filename_eventEEG(1:end-4),'.nxf'];
% %% remove bad RT EEG events (behav)
%%% read XLS 
% 
%EEGData_xls=xlsread(filename_xls,1)

% if size(EEGData_xls,1)>1
% events_nobadRT=events;
% EEGData_excl=EEGData_xls(:,1);
% excl_RT=EEGData_excl(isfinite(EEGData_excl));
% events_nobadRT(excl_RT)=[];
% 
%     else events_nobadRT=events;
% 
% end
% 
% events=events_nobadRT;


%% Write add info
% AddInfoFname = [EEGDatapath,content_folder1(f).name,'\',content_folder2(g).name,'\',filename_eventEEG(1:end-4),'_add.mat']
%
% save (AddInfoFname,'newsamp', 'fsnxf'); 
%
 clear *event* Event* vis* offset EEGData *risk test* excl* newsamp EEG eeglabfilename  
    end
    end

end

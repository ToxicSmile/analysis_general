clear all; close all;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Artifact rejection
% decide if will be resuming or not, then set up file saving
% toResume = 0;
% while min(toResume ~= ['n' 'N' 'r' 'R'])
%     toResume = input('(n)ew, (r)esume: ','s');
% end

%File to get data from, name of save file determined automatically based on
%load file name
%ADJUST PATH!!!!
path='D:\Data_AEP\ICA\VP\ST31LE\stimulation\morning';
% eeglabpathbase='E:\\ERP_DM_VP_data\\NUR_RiskTask_Eingeschlossen\\ScriptTests\\Final\\TEST_FINAL\\VP_31\\SD1\\';
cd (path)
[fnamebase, path] = uigetfile('*_afterComponentRejectionInterpAverefBLcorr.mat');
load (fnamebase)

fname=[fnamebase(1:end-45),'acceptedPos'];
trialDataFName = sprintf('%s%s.mat',path,fname);
fprintf('Accepted trial data will be put in %s\n\n',trialDataFName);

filename=sprintf('%s%s.mat',path,fnamebase);

timeRange = input('Time Range ([-200 500]): ');
if isempty(timeRange)
    timeRange = [-200 500];
end
baselineRange = input('Baseline Range ([-200 0]): ');
if isempty(baselineRange)
    baselineRange = [-200 0];
end
ySeparation = input('Y Separation (20): ');
if isempty(ySeparation)
    ySeparation = 20;
end
eogLimit = input('Acceptable Eog Limit (50): ');
if isempty(eogLimit)
    eogLimit = 50;
end    

data=dataBLcorrICAAVGref;
tPos=1:size(data,3);
timeSf=sr/1000;
tRange = round(timeRange.*timeSf);
visibleChannels = 1:size(data,1);
timeAxis = ((0:size(data,2)-1)+tRange(1))/timeSf;

% prepare data storage
acceptedPos = [];   %Store accepted trial positions
trialPos = [];      %Store trial positions, regardless of accept/reject
i=1;

fprintf('\nAccept or reject the following trials\n\tMOUSE CLICK:  accept\nb:  go back one trial\n\tq:  quit\n\tANY OTHER KEY: reject');
fprintf('\nPress any key to begin\n');
pause;

more off;
while i<=length(tPos)
    trialPos(length(trialPos)+1) = tPos(i); % keep track of when each trial occurs, whether rejected or accepted

    t=timeAxis;
    d=data(:,(1:size(data,2)),tPos(i));
    msePlotDataERP(t,d,visibleChannels,ySeparation,eogLimit);
% plotdata(t,d,visibleChannels,ySeparation,eogLimit);
    
    [x,y,button] = ginput(1); % get input
    if button == 98; % go back one trial
        trialPos = trialPos(1:length(trialPos) - 1);
        i = find(trialPos(length(trialPos)) == tPos);
        
        if (acceptedPos(length(acceptedPos)) == tPos(i));
            acceptedPos = acceptedPos(1:length(acceptedPos)-1);
        end
        trialPos = trialPos(1:length(trialPos) - 1);
    elseif button==1;
        acceptedPos = [acceptedPos;tPos(i)];
        disp(['Trial ' int2str(i) ', Accepted']);
        i = i + 1;
    elseif button == 'q'
        break        
    else
        disp(['Trial ' int2str(i) ', Rejected']);
        i = i + 1;
    end
    eval(sprintf('save ''%s'' acceptedPos',trialDataFName));
    
end
close all;








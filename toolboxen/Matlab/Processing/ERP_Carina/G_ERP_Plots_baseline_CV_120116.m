%%%%Script to generate a plot per subject of ERP for each condition
%
% adjusted script from AM 12.01.16
%% Bar plot and channel stats
clear all
close all
% time='all' %afternoon evening all (all= SRbase1&2 SR8_1&2 SDbase1&2 SD1&2)
% Cond={'baseline';}; %nur bei oneCond n�tig
% time={'all';'afternoon';'evening'; 'oneCond'};% 'all';'afternoon';'evening'; 'oneCond'

date='04012016'; %define actual executation Date of plotting
Ch='Frontocentral_19_EL';% _define actual Channel of plotting
cluster='Frontocentral 19 EL'
ChannelNb=[3 109 105 100 95 7 13 20 24 23 18 16 10 4 5 6 12 19 11]; % %according to original channel label list
path='D:\Data_AEP\ICA\finished\VP\';
cd(path)

VP={'ST03CR';'ST07MS';'ST17GW';'ST20FG';'ST23GC';'ST26DK';'ST30NH';'ST31LE';'ST19EH'}%
% locfilepath='E:\MatlabScripts\ERP_JUNE_15\otherscripts_needed_ERP\ScriptsforTMS_Milano\';
% locfilename='D:\Matlab\template_EEG_locfile\chanloc_109_num';% CHANGE ACCORDING TO YOUR PATH
% 
% for t=1:size(time,1)
   
for v=1:length(VP)
      
            channel=[1:109];
%         if strcmp(time,'evening')
%         folder=Cond;
%         plotcolor={'g'}%y k
%         elseif strcmp(time{t},'morning')
%         folder={'SRbase1','SR8_1','SDbase1','SD1'}
%         plotcolor={'g','r','c','k'}%y k
%         elseif strcmp(time{t},'evening')
%         folder={'SRbase2','SR8_2','SDbase2','SD2'}
%         plotcolor={'g','r','c','k'}%y k 
%        elseif strcmp(time{t},'all')
%         folder={'SRbase1','SRbase2','SR8_1','SR8_2','SDbase1','SDbase2', 'SD1','SD2'}
%         plotcolor={'g','y','m','r','c','b','k','k--'}%y k 
 folder={'morning','evening'};
    plotcolor={'g','b'}%y k
%        end
       
%         folder={'SRbase2','SR8_2','SDbase2','SD2'}
%         plotcolor={'g','r','c','k'}%y k 
   
cd([path,VP{v},'\baseline\']);
    
    for p=1:length(folder)
    cd([path,VP{v},'\baseline\',folder{p}]);
    clear content_acceptedPos content_dataBLcorr;
    content_acceptedPos=dir('*acceptedPos.mat');
    content_dataBLcorr=dir('*_afterComponentRejectionInterpAverefBLcorrNEW.mat');
    
    load(content_dataBLcorr(1).name,'dataBLcorrICAAVGref','sr','allevents_code');
    data=dataBLcorrICAAVGref;
    clear dataBLcorrICAAVGref
    load(content_acceptedPos(1).name,'acceptedPos');
    datapoints=size(data,2);
    channels=size(data,1);
    gooddata=zeros(channels,datapoints,length(acceptedPos));
    for i=1:length(acceptedPos)
        gooddata(:,:,i)=data(:,:,acceptedPos(i));
    end
    clear data;
data=gooddata;
  clear gooddata;
    
% mean over trials
   data_mn=squeeze(mean(data,3)); %%% data_mn includes all 109 channels atall timepoints
   save([VP{v},'_meanOverTrials_','109ch','.mat'],'data_mn');
%    subjlevelcluster = 
   
%     save('D:\Data_AEP\ICA\finished\VP\results\',[VP{v},'_meanOverTrials_',Ch,'.mat'],'data_mn');
%     save(['D:\Data_AEP\ICA\finished\VP\results\,VP{v},'_meanOverTrials_',Ch,'.mat',['data_mn']);
%    filename = ([VP{v},'_meanOverTrials_',Ch,'test','.mat'])
%    savefile = sprintf ('%s',VP{v},'_',Ch,'.mat')
%    save ('D:\Data_AEP\ICA\finished\VP\results\filename','data_mn')
    
    %%%to do average over subjects:
    if p==1
    data_subjmns_bm(v,:,:)=data_mn;
%     indicluster_bm = squeeze(nanmean(data_mn(ChannelNb,1:700)));
    else
        data_subjmns_be(v,:,:)=data_mn;
%         indicluster_be = squeeze(nanmean(data_mn(ChannelNb,1:700)));
    end
% save ([VP{v},' Baseline_morning ', cluster,'.mat'],'indicluster_bm');
% save ([VP{v},' Baseline_evening ', cluster,'.mat'],'indicluster_be');

%   testsave2 = squeeze(mean(data_mn(ChannelNb,1:700),1));
%   save ([VP{v},' Baseline ', cluster,'.mat'],'testsave');
    %topoplots all channels every cond separately
% if strcmp(time{t},'all')

% 
% test2 = squeeze(mean(data_mn(ChannelNb,1:700),1));
%     indicluster = mean(data_mn(ChannelNb,1:700));


  
%     if p==1
        plot(squeeze(mean(data_mn(ChannelNb,1:700),1)),plotcolor{p})
        hold on
%         grid
        figurename=[VP{v},' Baseline ', cluster];
        title(figurename)
        xlabel('time [ms]')
        ylabel('microV')
        grid on 
%         yaxis([-10 8])
    end
    
%     end
    
    cd(path);
%     cd([path,VP{v},'\']);
    set(gcf, 'Position', get(0,'Screensize')); % Maximize figure
    
%     legend('SRbase2','SR8_2','SDbase2','SD2')
% if strcmp(time{t},'afternoon')
%     legend('SRbase1','SR8_1','SD1')
%     figurename=[VP{v},'_curve_',Ch,'_afternoon_',num2str(date)];
% elseif strcmp(time{t},'evening')
%     legend('SRbase1','SRbase2','SR8_1','SR8_2','SDbase1','SDbase2', 'SD1','SD2')%'SR8_2'
%     figurename=[VP{v},'_curve_all',num2str(date)];
% end
legend('baseline_morning','baseline_evening')

hx = graph2d.constantline(200,'Color',[ 0 0 0 ], 'LineWidth',1);
        changedependvar(hx,'x');
        
figurename=[VP{v},'_Baseline_',Ch,'_',num2str(date)];
 saveas(gcf,figurename,'jpeg');
close all

end

%%Plot average over subjects

data_mn1=squeeze(nanmean(data_subjmns_bm,1));
data_mn2=squeeze(nanmean(data_subjmns_be,1));

% save (['D:\Data_AEP\ICA\finished\VP\results\allVP_bm_',Ch,'.mat'],'data_mn1','ChannelNb')
save (['allVP_bm_',Ch,'.mat'],'data_mn1','ChannelNb')
save (['allVP_be_',Ch,'.mat'],'data_mn2','ChannelNb')

% save ('D:\Data_AEP\ICA\finished\VP\results\'allVP_be_',Ch,'.mat'),'data_mn2','ChannelNb')

 plot(squeeze(mean(data_mn1(ChannelNb,1:700),1)),plotcolor{1})
        hold on
        plot(squeeze(mean(data_mn2(ChannelNb,1:700),1)),plotcolor{2})
        hx = graph2d.constantline(200,'Color',[ 0 0 0 ],'LineWidth',1);
        changedependvar(hx,'x');
        grid
       
        figurename=['allVP Baseline ', cluster];
        title(figurename)
        xlabel('time [ms]')
        ylabel('microV')
%         yaxis([-10 8])
     
    
%     end
    
    cd(path);
%     cd([path,VP{v},'\']);
    set(gcf, 'Position', get(0,'Screensize')); % Maximize figure
    
%     legend('SRbase2','SR8_2','SDbase2','SD2')
% if strcmp(time{t},'afternoon')
%     legend('SRbase1','SR8_1','SD1')
%     figurename=[VP{v},'_curve_',Ch,'_afternoon_',num2str(date)];
% elseif strcmp(time{t},'evening')
%     legend('SRbase1','SRbase2','SR8_1','SR8_2','SDbase1','SDbase2', 'SD1','SD2')%'SR8_2'
%     figurename=[VP{v},'_curve_all',num2str(date)];
% end
legend('morning','evening')
figurename=['AllVP','_Baseline_',Ch,'_',num2str(date)];
 saveas(gcf,figurename,'jpeg');
close all
% end

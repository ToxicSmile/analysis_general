%% A1 Loop File 
% Valid for files using a continuous two-digit sequence in their naming scheme.
% Select the first *.raw data file when prompted, and enter the total number of 
% *.raw data files present in the same directory.  The downsampling will be 
% performed on all *.raw files, with the *.raf file output, downsampled and filtered
% saved in the same directory.
% GL2013


[file1,path1] = uigetfile('*.raw', 'Select the first .raw data file');
numfile = input('HOW MANY .RAW FILES DO YOU WANT TO PROCESS? ');
filebase = file1(3:end);
for k = 1 : numfile;
    K=num2str(k);
    if k<10
        fileloop = strcat(path1,'0',K,filebase);  % concatenates the directory and filename into one string
    else
        fileloop = strcat(path1,K,filebase); 
    end
    A2_downSampFilt_vectorized(fileloop);  % Create the *.raf files from the *.raw, filtered & downsampled.
end
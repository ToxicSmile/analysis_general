%plots Hypnogram, SWA time course,...
%rh, 6.8.2008

clear all; close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
visname='ST17GW_1';
% vispath='O:\Huber\Scoring\check scored MR\';
vispath='C:\ZurichScoring\TMS_scorings_fertig\3_fertigfertig\ST17GW_1\';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%read vis file
offset=0;
track=1;
VisName=[vispath,visname,'.VIS'];
[vistrack,vissymb,offset]=readtrac(VisName,track);
VisNum=numvis(vissymb,offset);
numepo=length(VisNum);


%%%%calculates sleep parameters
wak=length(find(vissymb=='0'))/numepo*100;
sl=find(vissymb=='2' | vissymb=='3' | vissymb=='4' | vissymb=='r'); sl=sl(1)/3;
s1=length(find(vissymb=='1'))/numepo*100;
nr=length(find(vissymb=='2' | vissymb=='3' | vissymb=='4'))/numepo*100;
rem=length(find(vissymb=='r'))/numepo*100;

indexsleep1=find(vissymb=='1' | vissymb=='2' |vissymb=='3' | vissymb=='4' | vissymb=='r')';
tots=length(indexsleep1)/3;

indexNotScor=find(vissymb== ' ');
indexScored=setdiff([1:length(vissymb)],indexNotScor);
totScored=length(indexScored)/3;
seff=(100/totScored)*tots;



%%%%read data file
R09Name=[vispath,visname,'.r09']
fs=128;
numchan=str2num(R09Name(length(R09Name)));

fid = fopen(R09Name,'r');
scordat=fread(fid,'short');
fclose(fid);

scordat=reshape(scordat,numchan,length(scordat)/numchan);

%%%%fft
ffttot=[];
fftblock=zeros(numchan,120,numepo);
fprintf('fft channel  ')
for ch=1:numchan
fprintf('...%d',ch)
    for epoch=1:numepo
        start=1+((epoch-1)*20*fs);
        ending=20*fs+((epoch-1)*20*fs);
        ffte=pwelch(scordat(ch,start:ending),hanning(4*fs),0,4*fs,fs);
        ffte=ffte(1:120);
        fftblock(ch,:,epoch)=ffte;
    end
end
ffttot=[ffttot fftblock];
fprintf('\n');


%%%%artefact index
artndx=find(sum(vistrack')>0);

%%%%time courses
ch=4; %%%%f3a2
swa=mean(squeeze(ffttot(ch,4:18,:))); swa(artndx)=NaN;
the=mean(squeeze(ffttot(ch,20:32,:))); the(artndx)=NaN;
alp=mean(squeeze(ffttot(ch,36:44,:))); alp(artndx)=NaN;
sig=mean(squeeze(ffttot(ch,48:60,:))); sig(artndx)=NaN;

emg=log(sum(squeeze(ffttot(1,80:120,:))));
eog=log(sum(squeeze(ffttot(2,1:4,:))));

%%%%figure
density=10;
[Vis]=plotvis(VisNum,density);

t=0:1/180:(length(VisNum)/180)-1/180;

figure('Color',[1 1 1],'PaperOrientation','landscape','PaperUnits','centimeters','PaperPosition',[1 1 26 20])
subplot(711)
    plot(Vis(:,1),Vis(:,2))
    grid
    title([visname,'                ','SL(min) = ',num2str(sl,'%2.1f'),'  SEF(%) = ',num2str(seff,'%2.1f'),'     W% = ',num2str(wak,'%2.1f'),'     S1% = ',num2str(s1,'%2.1f'),'     NREM% = ',num2str(nr,'%2.1f'),'     REM% = ',num2str(rem,'%2.1f'),'                   ',date])
    xaxis([0 t(numepo)])
    ylabel('Hypnogram')
    yaxis([-3 2])
subplot(712)
    bar(t,swa,'EdgeColor','k')  %,'EdgeColor','none'
    grid
    ylabel('SWA')
    xaxis([0 t(numepo)])
    yaxis([0 max(swa(find(vissymb=='2' | vissymb=='3' | vissymb=='4')))])
%     yaxis([0 1000])
subplot(713)
    bar(t,the) 
    grid
    ylabel('Theta activity')
    xaxis([0 t(numepo)])
    yaxis([0 max(the(find(vissymb=='2' | vissymb=='3' | vissymb=='4')))])
subplot(714)
    bar(t,alp)
    grid
    ylabel('Alpha activity')
    xaxis([0 t(numepo)])
    yaxis([0 max(alp(find(vissymb=='2' | vissymb=='3' | vissymb=='4')))])
subplot(715)
    bar(t,sig)
    grid
    ylabel('Sigma activity')
    xaxis([0 t(numepo)])
    yaxis([0 max(sig(find(vissymb=='2' | vissymb=='3' | vissymb=='4')))])
subplot(716)
    plot(t,emg,'r')
    grid
    ylabel('log EMG')
    xaxis([0 t(numepo)])
subplot(717)
    plot(t,eog)
    grid
    ylabel('log EOG')
    xlabel('hours')
    xaxis([0 t(numepo)])

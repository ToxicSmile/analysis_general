clear all; close all;

%%% Daten von Trigger Kanal neu referenzieren (links und rechts ohr) und
%%% als zusammenhängende Variable abspeichern

%%
% fs=500;
% 
% chTrig=130;
% chEEG=36;
% chEarL=49;
% chEarR=56;
% 
% fname=['E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\',sub(s).name,'\',Session{l},'sleep\']; 
% saveFile=['E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\',sub(s).name,'\',Session{l},'sleep\Analysis\']; 
% Fileraw=dir([fname,'*.raw']);
% numfile=length(Fileraw);
% rawname=Fileraw(1).name(4:end-4);
% 
% tPos_all=[];
% samp=0;
% 
% for i=1:numfile
%     display(i)
%     foldername=[fname,'0',num2str(i),'_',rawname,'.raw'];
%     eval('tPos=egiGetTriggersCorr(foldername,ch);','tPos=[]') 
%     data = loadEGIBigRaw(foldername,1); 
%     tPos_row=tPos+samp;
%     tPos_all=[tPos_all,tPos_row]; 
%     currentsamp=length(data);
%     samp=samp+currentsamp;  
%     savename=[fname,'0',num2str(i),'_',rawname,'_trigger.mat'];
%     save(savename,'fs','tPos');
%     clear data tPos savename tPos_row
% end
% 
% 
% 
% %%% save tPos_all in long .mat File
% %         if numfile<10
% %             numfilstra=['0' num2str(numfile)];
% %         else
% %             numfilstra=num2str(numfile);
% %         end
% 
% eval(['save ',saveFile,rawname(1:8),'_FFT.mat tPos_all -append']);
% 
% clear  Fileraw fname numfile rawname tPos_all samp saveFile

% %% SWS_TMS 10-20
% fs=500;
% 
% %chEEG=[30 105];
% chEEG=sort([24 124 36 104 52 92 70 83 30 105]);
% 
% refCh=[49 56];
% 
% %%% Filter
% Fc1=[0.319825 3.12648]/(fs/2); %fs=500
% N1=3;
% [z1,p1,k1] = butter(N1, Fc1);
% [sos_var1,g1] = zp2sos(z1, p1, k1);
% Hd_but1 = dfilt.df2sos(sos_var1, g1);
% 
% Session={'session1\'};
% sub={'ST02BL' 'ST05TS' 'ST09RE' 'ST13DK' 'ST21JF' 'ST25BU'}; % 
% Folder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\';
% Folder_raw='D:\Daten\SWS\SWS_TMS\EGI\';
%     
% 
% for l=1:length(Session)
%      Folderpath=[Folder,Session{l}];
% 
%      for s=1:length(sub)
%          Rawname=dir([Folder_raw,sub{s},'\',Session{l},'*.raw']); 
%          numfile=length(Rawname);
% 
%           data=[]; 
% 
%         for i=1:numfile
%             display(i)
%             filenameraw=[Folder_raw,sub{s},'\',Session{l},Rawname(i).name];
% 
%             data_ref1 = loadEGIBigRaw(filenameraw,49);
%             data_ref2 = loadEGIBigRaw(filenameraw,56);
% 
%              for ch=1:length(chEEG)
%                  data_ch = loadEGIBigRaw(filenameraw,chEEG(ch));  
%                  data_ref=data_ch-((data_ref1+data_ref2)/2); 
%                  data_ref_fil(ch,:)=filter(Hd_but1,data_ref);
%                  clear data_ch data_ref
%              end
%              
%              data=[data data_ref_fil];
%              clear data_ref_fil  data_ref* foldername filenameraw
%         end
%         
%         save([Folder_raw,sub{s},'\',Session{l},Rawname(i).name(4:11),'_FiltData_allnight_1020.mat'],'data','chEEG','fs','refCh','Hd_but1')
%         clear numfile Rawname data
%      end
% end

%% SWS TMS for Bs new study
% fs=500;
% refCh=[49 56];
% 
% %%% Filter
% Fc1=[0.319825 3.12648]/(fs/2); %fs=500
% N1=3;
% [z1,p1,k1] = butter(N1, Fc1);
% [sos_var1,g1] = zp2sos(z1, p1, k1);
% Hd_but1 = dfilt.df2sos(sos_var1, g1);
% 
% Session_ndx=[3 3 2 3 2 2 2 2 3];
% % Session_ndx_stim=[2 2 3 2 3 3 3 2];
% 
% % Session_ndx=[2];
% % Session_ndx_stim=[3];
% 
% %sub={'ST20FG'}; % 
% sub={'ST03CR' 'ST07MS' 'ST17GW' 'ST19EH' 'ST20FG' 'ST23GC' 'ST26DK' 'ST30NH' 'ST31LE'}; % 
% 
% Folder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\bs\';
% %Folder_stim='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\stim\';
% 
% 
% Folder_raw='D:\Daten\SWS\SWS_TMS\EGI\newStudy\';
% %filename=dir([Folder_stim,'*_FFT.mat']);   
% filename=dir([Folder,'*_FFT.mat']);    
% 
%      
% 
%  for s=1:length(sub)
%      %Folderpath=[Folder,'session',num2str(Session_ndx(s)),'\'];
%      
% 
% %      loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
%      %loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
%      %load(loadfile, 'tPos_all','StimCh');
% 
%      %if exist('tPos_all')
%      if sum(filename(s).name(1:6)==sub{s})==6
% 
%         chEEG=31;
%         clear tPos_all
% 
% 
%          Rawname=dir([Folder_raw,sub{s},'\session',num2str(Session_ndx(s)),'\*.raw']); 
%          numfile=length(Rawname);
% 
%         data=[]; 
% 
%         for i=1:numfile
%             display(i)
%             filenameraw=[Folder_raw,sub{s},'\session',num2str(Session_ndx(s)),'\',Rawname(i).name];
% 
%             eval('tPos=egiGetTriggersCorr(filenameraw,130);','tPos=[]') 
% 
%             if length(tPos)<3
% 
%                 data_ref1 = loadEGIBigRaw(filenameraw,49);
%                 data_ref2 = loadEGIBigRaw(filenameraw,56);
% 
%                  for ch=1:length(chEEG)
%                      data_ch = loadEGIBigRaw(filenameraw,chEEG(ch));  
%                      data_ref=data_ch-((data_ref1+data_ref2)/2); 
%                      data_ref_fil(ch,:)=filter(Hd_but1,data_ref);
%                      clear data_ch data_ref
%                  end
% 
%                  data=[data data_ref_fil];
%                  clear data_ref_fil  data_ref* foldername filenameraw
% 
%             else
%                 error('wrong session')
%             end
%             clear tPos
%         end
% 
%         save([Folder,sub{s},'_',num2str(Session_ndx(s)),'_FiltData_allnight_ch31.mat'],'data','chEEG','fs','refCh','Hd_but1')
%         clear numfile Rawname data chEEG StimCh tPos_all
%      else
%          error('wrong session')
%      end
%  end

%% SWS TMS for Stim new study
% clear all; close all;
% fs=500;
% refCh=[49 56];
% 
% %%% Filter
% Fc1=[0.319825 3.12648]/(fs/2); %fs=500
% N1=3;
% [z1,p1,k1] = butter(N1, Fc1);
% [sos_var1,g1] = zp2sos(z1, p1, k1);
% Hd_but1 = dfilt.df2sos(sos_var1, g1);
% 
% 
% Session_ndx_stim=[2 2 3 2 3 3 3 3 2];
% 
% % Session_ndx=[2];
% % Session_ndx_stim=[3];
% 
% %sub={'ST20FG'}; % 
% sub={'ST03CR' 'ST07MS' 'ST17GW' 'ST19EH' 'ST20FG' 'ST23GC' 'ST26DK' 'ST30NH' 'ST31LE'}; % 
% 
% 
% Folder_stim='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\stim\';
% 
% 
% Folder_raw='D:\Daten\SWS\SWS_TMS\EGI\newStudy\';
% filename=dir([Folder_stim,'*_FFT.mat']);   
% 
%      
% 
%  for s=1:length(sub)
%      %Folderpath=[Folder,'session',num2str(Session_ndx(s)),'\'];
%      
% 
% %      loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
%      %loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
%      %load(loadfile, 'tPos_all','StimCh');
% 
%      %if exist('tPos_all')
%      if sum(filename(s).name(1:6)==sub{s})==6 && filename(s).name(8)==num2str(Session_ndx_stim(s))
%          load([Folder_stim,filename(s).name], 'tPos_all','StimCh');
% 
%         %chEEG=31;
%         chEEG=StimCh;
%         clear tPos_all
% 
% 
%          Rawname=dir([Folder_raw,sub{s},'\session',num2str(Session_ndx_stim(s)),'\*.raw']); 
%          numfile=length(Rawname);
% 
%         data=[]; tPos_all=[]; 
% 
%         for i=1:numfile
%             display(i)
%             filenameraw=[Folder_raw,sub{s},'\session',num2str(Session_ndx_stim(s)),'\',Rawname(i).name];
% 
%             eval('tPos=egiGetTriggersCorr(filenameraw,130);','tPos=[]') 
% 
%                 data_ref1 = loadEGIBigRaw(filenameraw,49);
%                 data_ref2 = loadEGIBigRaw(filenameraw,56);
% 
%                  for ch=1:length(chEEG)
%                      data_ch = loadEGIBigRaw(filenameraw,chEEG(ch));  
%                      data_ref=data_ch-((data_ref1+data_ref2)/2); 
%                      data_ref_fil(ch,:)=filter(Hd_but1,data_ref);
%                      clear data_ch data_ref
%                  end
% 
%                  data=[data data_ref_fil];
%                  clear data_ref_fil  data_ref* foldername filenameraw
%                  tPos_all=[tPos_all tPos];
%                  clear tPos
%    
%         end
%         
%         if length(tPos_all)<3
%             error('wrong session')
%         else
% 
%             save([Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FiltData_allnight_TrigCh.mat'],'data','chEEG','fs','refCh','Hd_but1')
%         end
%         clear numfile Rawname data chEEG StimCh tPos_all
% 
%      end
%      
%  end
% 

 

%% SWS_TMS 
% fs=500;
% refCh=[49 56];
% 
% %%% Filter
% Fc1=[0.319825 3.12648]/(fs/2); %fs=500
% N1=3;
% [z1,p1,k1] = butter(N1, Fc1);
% [sos_var1,g1] = zp2sos(z1, p1, k1);
% Hd_but1 = dfilt.df2sos(sos_var1, g1);
% 
% Session={'session2\' 'session3\'};
% 
% sub={'ST03CR' 'ST07MS' 'ST17GW' 'ST19EH' 'ST23GC' 'ST26DK' 'ST30NH' 'ST31LE'}; % 
% Folder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\';
% Folder_raw='D:\Daten\SWS\SWS_TMS\EGI\';
%     
% 
% for l=1:length(Session)
%      Folderpath=[Folder,Session{l}];
% 
%      for s=1:length(sub)
%          
%          loadfile=[Folderpath,sub{s},'_',num2str(l)+1,'_FFT.mat'];
%          load(loadfile, 'tPos_all','StimCh');
%          
%          if exist('tPos_all')
%              
%             chEEG=StimCh;
%     
%          
%              Rawname=dir([Folder_raw,sub{s},'\',Session{l},'*.raw']); 
%              numfile=length(Rawname);
% 
%             data=[]; 
% 
%             for i=1:numfile
%                 display(i)
%                 filenameraw=[Folder_raw,sub{s},'\',Session{l},Rawname(i).name];
% 
%                 data_ref1 = loadEGIBigRaw(filenameraw,49);
%                 data_ref2 = loadEGIBigRaw(filenameraw,56);
% 
%                  for ch=1:length(chEEG)
%                      data_ch = loadEGIBigRaw(filenameraw,chEEG(ch));  
%                      data_ref=data_ch-((data_ref1+data_ref2)/2); 
%                      data_ref_fil(ch,:)=filter(Hd_but1,data_ref);
%                      clear data_ch data_ref
%                  end
% 
%                  data=[data data_ref_fil];
%                  clear data_ref_fil  data_ref* foldername filenameraw
%             end
%         
%             save([Folder_raw,sub{s},'\',Session{l},Rawname(i).name(4:11),'_FiltData_allnight.mat'],'data','chEEG','fs','refCh','Hd_but1')
%             clear numfile Rawname data chEEG StimCh tPos_all
%          end
%      end
% end


%% Local SWS
% fs=500;
% refCh=[49 56];
% 
% %%% Filter
% Fc1=[0.319825 3.12648]/(fs/2); %fs=500
% N1=3;
% [z1,p1,k1] = butter(N1, Fc1);
% [sos_var1,g1] = zp2sos(z1, p1, k1);
% Hd_but1 = dfilt.df2sos(sos_var1, g1);
% 
% 
% Folder_raw='D:\Daten\SWS\Local_SWS\EGI\LS09AM\session2\';
% load('D:\Daten\SWS\Local_SWS\EGI\LS09AM\session2\Analysis\LS09AM_2_FFT','elec_stim')
%     
% Rawname=dir([Folder_raw,'*.raw']); 
% numfile=length(Rawname);
% 
% data=[]; 
% 
% for i=1:numfile
%     display(i)
%     filenameraw=[Folder_raw,Rawname(i).name];
% 
%     data_ref1 = loadEGIBigRaw(filenameraw,49);
%     data_ref2 = loadEGIBigRaw(filenameraw,56);
% 
%      for ch=1:length(elec_stim)
%          data_ch = loadEGIBigRaw(filenameraw,elec_stim(ch));  
%          data_ref=data_ch-((data_ref1+data_ref2)/2); 
%          data_ref_fil(ch,:)=filter(Hd_but1,data_ref);
%          clear data_ch data_ref
%      end
% 
%      data=[data data_ref_fil];
%      clear data_ref_fil  data_ref* foldername filenameraw
% end
% 
% save([Folder_raw,Rawname(i).name(4:11),'_FiltData_allnight.mat'],'data','elec_stim','fs','refCh','Hd_but1')
% clear numfile Rawname data

%% SWS Local non filtered
% fs=500;
% refCh=[49 56];
% 
% %%% Filter
% 
% locutoff=0.5;
% hicutoff=40;
% 
% Folder_raw='D:\Daten\SWS\Local_SWS\EGI\LS09AM\session2\';
% load('D:\Daten\SWS\Local_SWS\EGI\LS09AM\session2\Analysis\LS09AM_2_FFT','elec_stim')
%     
% Rawname=dir([Folder_raw,'*.raw']); 
% numfile=length(Rawname);
% 
% data=[]; 
% 
% for i=1:numfile
%     display(i)
%     filenameraw=[Folder_raw,Rawname(i).name];
% 
%     data_ref1 = loadEGIBigRaw(filenameraw,49);
%     data_ref2 = loadEGIBigRaw(filenameraw,56);
% 
%      for ch=1:length(elec_stim)
%          data_ch = loadEGIBigRaw(filenameraw,elec_stim(ch));  
%          data_ref=data_ch-((data_ref1+data_ref2)/2); 
%          
%          data_ref_fil=eegfilt(data_ref,fs,0,hicutoff);
%          data_ref_fil2(ch,:)=eegfilt(data_ref_fil,fs,locutoff,0);    
%          clear data_ch data_ref data_ref_fil
%      end
% 
%      data=[data data_ref_fil2];
%      clear data_ref_fil  data_ref* foldername filenameraw
% end
% 
% save([Folder_raw,Rawname(i).name(4:11),'_noFiltData_allnight.mat'],'data','elec_stim','fs','refCh','locutoff','hicutoff')
% clear numfile Rawname data


%% SWS TMS any channel BS

clear all; close all;
fs=500;
refCh=[49 56];
chEEG=97;

%%% Filter
Fc1=[0.319825 3.12648]/(fs/2); %fs=500
N1=3;
[z1,p1,k1] = butter(N1, Fc1);
[sos_var1,g1] = zp2sos(z1, p1, k1);
Hd_but1 = dfilt.df2sos(sos_var1, g1);

Session_ndx=[3 3 2 3 2 2 2 2 3];
% Session_ndx_stim=[2 2 3 2 3 3 3 2];

% Session_ndx=[2];
% Session_ndx_stim=[3];

%sub={'ST20FG'}; % 
sub={'ST03CR' 'ST07MS' 'ST17GW' 'ST19EH' 'ST20FG' 'ST23GC' 'ST26DK' 'ST30NH' 'ST31LE'}; % 

Folder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\bs\';
%Folder_stim='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\stim\';


Folder_raw='D:\Daten\SWS\SWS_TMS\EGI\newStudy\';
%filename=dir([Folder_stim,'*_FFT.mat']);   
filename=dir([Folder,'*_FFT.mat']);    

     

 for s=2:length(sub)
     %Folderpath=[Folder,'session',num2str(Session_ndx(s)),'\'];
     chEEG=97;

%      loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
     %loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
     %load(loadfile, 'tPos_all','StimCh');

     %if exist('tPos_all')
     if sum(filename(s).name(1:6)==sub{s})==6

        
        clear tPos_all


         Rawname=dir([Folder_raw,sub{s},'\session',num2str(Session_ndx(s)),'\*.raw']); 
         numfile=length(Rawname);

        data=[]; 

        for i=1:numfile
            display(i)
            filenameraw=[Folder_raw,sub{s},'\session',num2str(Session_ndx(s)),'\',Rawname(i).name];

            eval('tPos=egiGetTriggersCorr(filenameraw,130);','tPos=[]') 

            if length(tPos)<3

                data_ref1 = loadEGIBigRaw(filenameraw,49);
                data_ref2 = loadEGIBigRaw(filenameraw,56);

                 for ch=1:length(chEEG)
                     data_ch = loadEGIBigRaw(filenameraw,chEEG(ch));  
                     data_ref=data_ch-((data_ref1+data_ref2)/2); 
                     data_ref_fil(ch,:)=filter(Hd_but1,data_ref);
                     clear data_ch data_ref
                 end

                 data=[data data_ref_fil];
                 clear data_ref_fil  data_ref* foldername filenameraw

            else
                error('wrong session')
            end
            clear tPos
        end

        save([Folder,sub{s},'_',num2str(Session_ndx(s)),'_FiltData_allnight_ch97.mat'],'data','chEEG','fs','refCh','Hd_but1')
        clear numfile Rawname data chEEG StimCh tPos_all
     else
         error('wrong session')
     end
 end

%% SWS TMS any channel Stim

clear all; close all;
fs=500;
refCh=[49 56];


%%% Filter
Fc1=[0.319825 3.12648]/(fs/2); %fs=500
N1=3;
[z1,p1,k1] = butter(N1, Fc1);
[sos_var1,g1] = zp2sos(z1, p1, k1);
Hd_but1 = dfilt.df2sos(sos_var1, g1);


Session_ndx_stim=[2 2 3 2 3 3 3 3 2];

% Session_ndx=[2];
% Session_ndx_stim=[3];

%sub={'ST20FG'}; % 
sub={'ST03CR' 'ST07MS' 'ST17GW' 'ST19EH' 'ST20FG' 'ST23GC' 'ST26DK' 'ST30NH' 'ST31LE'}; % 


Folder_stim='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\stim\';


Folder_raw='D:\Daten\SWS\SWS_TMS\EGI\newStudy\';
filename=dir([Folder_stim,'*_FFT.mat']);   

     

 for s=1:length(sub)
     %Folderpath=[Folder,'session',num2str(Session_ndx(s)),'\'];
     chEEG=97;

%      loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
     %loadfile=[Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FFT.mat'];
     %load(loadfile, 'tPos_all','StimCh');

     %if exist('tPos_all')
     if sum(filename(s).name(1:6)==sub{s})==6 && filename(s).name(8)==num2str(Session_ndx_stim(s))
         %load([Folder_stim,filename(s).name], 'tPos_all','StimCh');

        %chEEG=31;
        %chEEG=StimCh;
        %clear tPos_all


         Rawname=dir([Folder_raw,sub{s},'\session',num2str(Session_ndx_stim(s)),'\*.raw']); 
         numfile=length(Rawname);

        data=[]; tPos_all=[]; 

        for i=1:numfile
            display(i)
            filenameraw=[Folder_raw,sub{s},'\session',num2str(Session_ndx_stim(s)),'\',Rawname(i).name];

            eval('tPos=egiGetTriggersCorr(filenameraw,130);','tPos=[]') 

                data_ref1 = loadEGIBigRaw(filenameraw,49);
                data_ref2 = loadEGIBigRaw(filenameraw,56);

                 for ch=1:length(chEEG)
                     data_ch = loadEGIBigRaw(filenameraw,chEEG(ch));  
                     data_ref=data_ch-((data_ref1+data_ref2)/2); 
                     data_ref_fil(ch,:)=filter(Hd_but1,data_ref);
                     clear data_ch data_ref
                 end

                 data=[data data_ref_fil];
                 clear data_ref_fil  data_ref* foldername filenameraw
                 tPos_all=[tPos_all tPos];
                 clear tPos
   
        end
        
        if length(tPos_all)<3
            error('wrong session')
        else

            save([Folder_stim,sub{s},'_',num2str(Session_ndx_stim(s)),'_FiltData_allnight_ch97.mat'],'data','chEEG','fs','refCh','Hd_but1')
        end
        clear numfile Rawname data chEEG StimCh tPos_all

     end
     
 end
% 
     



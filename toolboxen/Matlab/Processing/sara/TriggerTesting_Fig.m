
clear all; close all;
%ch=130;
numfile=8; %% Anpassen
fs=500;
folder='D:\Daten\SWS\SWS_TMS\EGI\ST09RE\session2\';
filename='ST09RE_2_data_ch105_trigger';
Amp=30;
%% Filter
%%%%%%% Butterworth
Fc1=[0.319825 3.12648]/(fs/2); %fs=500
N1=3;
[z1,p1,k1] = butter(N1, Fc1);
[sos_var1,g1] = zp2sos(z1, p1, k1);
Hd_but1 = dfilt.df2sos(sos_var1, g1);

%%%%%%% Cheby 2 (Fs= 500; Fstop1 = 0.2; Fpass1 = 0.5; Fpass2 = 2; Fstop2 = 10; Astop = 50; Apass  = 0.1)

Fc1=[0.230416 4.33914]/(fs/2); %Werte von LV
N=5;
Astop = 50; 
[z,p,k] = cheby2(N,Astop,Fc1);

[sos_var,g] = zp2sos(z, p, k);
Hd_cheb = dfilt.df2sos(sos_var, g);

%%
for i=1:numfile
    load([folder,num2str(i),'_',filename,'.mat']);
    
    %dataOrg=filtfilt(b_cheby2,a_cheby2,data);
    %dataref_L=filtfilt(b_cheby2,a_cheby2,(data-refl));
    %dataref_L_2=filtfilt(b_cheby2,a_cheby2,(data-refl/2));
    dataref_but=filter(Hd_but1,(data-((refl+refr)/2)));
    dataref_cheb=filter(Hd_cheb,(data-((refl+refr)/2)));
    dataref_LR_Roland=rdf_FourierFilter1((data-((refl+refr)/2)),[0.5 4],fs);


    %plotdata2=dataref_L_2;
    %plotdata3=dataOrg;
    startplot=tPos(1);
    endplot=length(data);
    while startplot<endplot-fs*20
            detecwave=find(ismember(tPos,[startplot:startplot+fs*20]));
            %figure('position',[10 350 6250 600]','PaperOrientation','landscape');
            fig=figure
            subplot(3,1,1)
            plot(startplot:startplot+fs*20,dataref_but(startplot:startplot+fs*20),'k','LineWidth',1)
            title(['File: ',num2str(i),'Filter: Butter'])
            hold on
            hline(0)
            hold on
            hline(-Amp,'k')
            hold on
            %hline(+37.5)
            grid minor
            xlim([startplot (startplot+fs*20)])
            %hold on
            %plot(startplot:startplot+fs*20,dataScorf(startplot:startplot+fs*20),'b--','LineWidth',1)
            if ~isempty(detecwave)
                hold on
                plot(tPos(detecwave),dataref_but(tPos(detecwave)),'*r','MarkerSize',10)
                hold on
                vline(tPos(detecwave))
            end
            
            subplot(3,1,2)
            plot(startplot:startplot+fs*20,dataref_cheb(startplot:startplot+fs*20),'k','LineWidth',1)
            title(['File: ',num2str(i),' Cheb2'])
            hold on
            hline(0)
            hold on
            hline(-Amp,'k')
            hold on
            %hline(+37.5)
            grid minor
            xlim([startplot (startplot+fs*20)])
            if ~isempty(detecwave)
                hold on
                plot(tPos(detecwave),dataref_cheb(tPos(detecwave)),'*r','MarkerSize',10)
            end
            
            subplot(3,1,3)
            plot(startplot:startplot+fs*20,dataref_LR_Roland(startplot:startplot+fs*20),'k','LineWidth',1)
            title(['File: ',num2str(i),' Roland'])
            hold on
            hline(0)
            hold on
            hline(-37.5)
            hold on
            hline(+37.5)
            grid minor
            xlim([startplot (startplot+fs*20)])
            %    hold on
            %    plot(startplot:startplot+fs*20,dataScorf(startplot:startplot+fs*20),'b--','LineWidth',1)
            if ~isempty(detecwave)
                hold on
                plot(tPos(detecwave),dataref_LR_Roland(tPos(detecwave)),'*r','MarkerSize',10)
            end
            
           
            maximize(fig)
             pause
             close all
             startplot=(startplot+fs*20)+1;
    end
    clear data ref* tPos
end



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
% % aa=[];
% % fs=500;
% % for i=1:7
% %     load(['C:\User\Sara\AmpServer\Data\009_FH\',num2str(i),'_009_FH_data_trigger.mat']);
% %     passbanddeltat1=0.5;passbanddeltat2=2;stopbanddeltat1=0.2;stopbanddeltat2=10;
% %     Wp=[passbanddeltat1 passbanddeltat2]/(fs/2); Ws=[stopbanddeltat1 stopbanddeltat2]/(fs/2); %convert frequencies to pi/sample?
% %     Rp=0.1; Rs=20;
% %     [n, Wn]=cheb2ord(Wp,Ws,Rp,Rs); [b_cheby2,a_cheby2]=cheby2(n,Rs,Wn); %attenuation < 3db in passband; > 10 db in stopband
% %     %dataOrg=filtfilt(b_cheby2,a_cheby2,data);
% %     %dataref_L=filtfilt(b_cheby2,a_cheby2,(data-refl));
% %     %dataref_L_2=filtfilt(b_cheby2,a_cheby2,(data-refl/2));
% %     dataref_L_R=filtfilt(b_cheby2,a_cheby2,(data-((refl+refr)/2)));
% %     a=dataref_L_R(tPos);
% %     aa=[aa a];
% %     clear a dataref* data tPos
% % end


%%
% 
% tPos=[];
% numsamp=0;
% for i=1:numfile
%     display(i)
%     %[fname, path] = uigetfile('\\Troubadix\transfer\Huber\BNS\AmpServer\*.raw', 'Trial data file');
%     %fname = fname(1:length(fname) - 4);
%     fname='\\Troubadix\transfer\Huber\BNS\AmpServer\';
%     rawname='999FH201312062';
%     filename=[fname,'0',num2str(i),'_',rawname,'.raw'];
%     %filename=sprintf('%s%s.raw',path,fname);
%     tPos1 =egiGetTriggersCorr(filename,ch);  
%     
%     if length(tPos1)<=3
%         error('Falscher Kanal -> channel im egiGetTriggersCorr anpassen')
%     end
%     tPos1=tPos1+numsamp;
%     tPos=[tPos tPos1];
%     ns=length(loadEGIBigRaw(filename,1));
%     numsamp=numsamp+ns;
%     samp_file(i)=ns;
%     clear tPos1 ns
%     
% end

%save('C:\User\Sara\AmpServer\Data\009_FH\009_FH_Trigger_cont.mat','tPos','samp_file','numfile'',fs')






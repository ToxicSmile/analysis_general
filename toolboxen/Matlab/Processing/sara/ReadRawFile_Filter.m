

clear all; close all
folder='E:\User_Sara\AmpServer\Data\Filtertesting\neu\';


%rawFiles=dir([folder,'*.raw']);
rawFiles=dir([folder,'*Filter*.mat']);
tdmsFile=dir([folder,'*tdms.mat']);


% for i=i:length(rawFiles)
%     filenameraw=[folder,rawFiles(i).name];
%     
%     fid = fopen(filenameraw,'rb','b');
%     [segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fid);
%     fclose(fid);
%     
%     
%     for ch=1:128
%         data(ch,:)=loadEGIBigRaw(filenameraw,ch);
%     end
%     
%      
%     Event(1,:)=loadEGIBigRaw(filenameraw,130);
% %    Event(2,:)=loadEGIBigRaw(filenameraw,131);
%     savepath=[filenameraw(1:end-4),'.mat'];
%     save(savepath,'Event','data','Event','dataFormat','Samp_Rate')
%     clear  filenameraw Event* data
% end


%%

% for ch=1:128
%     i_raw=3;
%     i_tdms=4;
%     load([folder,rawFiles(i_raw).name]);
%     load([folder,tdmsFile(i_tdms).name]);
%     sec=5;
% 
%     ndxEvent=find(Event);
%     startplot=startplot+0.66*Samp_Rate;
% 
%     endplot=ndxEvent(2);
%     eval(['plotdata = my_tdms_struct.raw_data.c_',num2str(ch),'.data',';'])
%     fitdata=data(ch,:);
% 
%     fig=figure
%     subplot(3,1,1)
%     plot(fitdata(startplot:endplot))
%     subplot(3,1,2)
%     plot(plotdata)
% 
%     subplot(3,1,3)
%     plot(fitdata(startplot:endplot))
%     hold on
%     plot(plotdata,'r')
%     maximize(fig)
%     pause 
%     close all
% end


% ch=92
% i_raw=3;
% i_tdms=4;
% load([folder,rawFiles(i_raw).name]);
% load([folder,tdmsFile(i_tdms).name]);
% sec=5;
% Fs=Samp_Rate;
% 
% ndxEvent=find(Event);
% startplot=ndxEvent(1);
% endplot=ndxEvent(2);
% %eval(['plotdata = my_tdms_struct.raw_data.c_',num2str(ch),'.data',';'])
% plotdata = my_tdms_struct.EEG_EMG_EOGR_EOGL.EEG.data;
% data_ref=data(92,:)-((data(56,:)+data(49,:))/2);
% 
% Fs = 1000;  % Sampling Frequency
% 
% Fc=[0.319827 3.12663]/(Fs/2);
% N=3;
% 
% 
% [z,p,k] = butter(N, Fc);
% 
% [sos_var,g] = zp2sos(z, p, k);
% Hd          = dfilt.df2sos(sos_var, g);
% 
% fitdata=filter(Hd,data_ref);
% 
% startplot=ndxEvent(1);
% startplot=startplot+0.66*Samp_Rate;
% fig=figure
% subplot(3,1,1)
% plot(fitdata(startplot:endplot))
% subplot(3,1,2)
% plot(plotdata)
% 
% subplot(3,1,3)
% plot(fitdata(startplot:endplot))
% hold on
% plot(plotdata,'r')
% maximize(fig)






i_raw=3;
i_tdms=4;
load([folder,rawFiles(i_raw).name]);
load([folder,tdmsFile(i_tdms).name]);

ndxEvent=find(Event);
startplot=ndxEvent(1);
startplot=startplot+0.66*Samp_Rate;
endplot=length(data);
start_lv=1;
sec=5;

for ch=56%1:128 
    eval(['plotdata = my_tdms_struct.raw_data.c_',num2str(ch),'.data',';'])
    %plotdata = my_tdms_struct.EEG_EMG_EOGR_EOGL.EEG.data;
    figure
%     subplot(2,1,1)
%     plot(data(ch,:))
%     subplot(2,1,2)
%     plot(plotdata)
     
     startplot<endplot-Samp_Rate*sec
            plot([0:Samp_Rate*sec],data(ch,[startplot:startplot+Samp_Rate*sec]))
%         if sum(ismember(ndxEvent,[startplot:startplot+Samp_Rate*sec]))~= 0
%             trig=ndxEvent(find(ismember(ndxEvent,[startplot:startplot+Samp_Rate*sec])));
%             hold on
%             vline(trig,'r')
%         end
            hold on
            plot([0:Samp_Rate*sec],plotdata(start_lv:start_lv+Samp_Rate*sec),'r')
        
         pause
%          startplot=(startplot+Samp_Rate*sec)+1;
%          start_lv=(start_lv+Samp_Rate*sec)+1;
         close all
 
     
end
     





% ch=1
% i_raw=3;
% i_tdms=4
% load([folder,rawFiles(i_raw).name]);
% load([folder,tdmsFile(i_tdms).name]);
% 
% data_ref=data(92,:)-((data(100,:)+data(57,:))/2);
% start=find(Event);
% 
% 
% 
% 
% Fstop1 = 0.1;  % First Stopband Frequency
% Fpass1 = 0.5;  % First Passband Frequency
% Fpass2 = 2;    % Second Passband Frequency
% Fstop2 = 10;    % Second Stopband Frequency
% Astop1 = 20;   % First Stopband Attenuation (dB)
% Apass  = 0.1;  % Passband Ripple (dB)
% Astop2 = 20;   % Second Stopband Attenuation (dB)
% 
% % Calculate the order from the parameters using BUTTORD.
% % [N,Fc] = buttord([Fpass1 Fpass2]/(Samp_Rate/2), [Fstop1 Fstop2]/(Samp_Rate/2), Apass, ...
% %                  max(Astop1, Astop2));
% %              
% % [a,b] = butter(N, Fc);
% 
% [N,Fc] = cheb2ord([Fpass1 Fpass2]/(Samp_Rate/2), [Fstop1 Fstop2]/(Samp_Rate/2), Apass, ...
%                  max(Astop1, Astop2));
%              
% [a,b] = cheby2(N,20, Fc);
% 
% 
% %fitdata=filter(a,b,data_ref);
% fitdata=data(ch,[start(1):length(data(ch,:))]);
% 
% 
% 
%    
% %plotdata = my_tdms_struct.EEG_EMG_EOGR_EOGL.EEG.data;
% eval(['plotdata=my_tdms_struct.raw_data.c_',num2str(ch),'.data',';'])
% figure
% subplot(2,1,1)
% plot(fitdata)
% %YLIM([-400 0])
% subplot(2,1,2)
% plot(plotdata)









%%



            
            



    





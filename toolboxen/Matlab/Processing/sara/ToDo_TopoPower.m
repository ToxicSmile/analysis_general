

clear all; close all;
%%

upperbin=18;%61%18; Frequenz=(bin-1)/4 
lowerbin=5; %49 %5;
nch=128;
time=100;    % 1=FH, 2=360min,usw 100=allNight 200=LH
power='rel'; %'abs'


%% Stim
npower_stim(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_2_FFT.mat',lowerbin,upperbin,nch,time,power);    
npower_stim(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_2_FFT.mat',lowerbin,upperbin,nch,time,power);    
npower_stim(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_2_FFT.mat',lowerbin,upperbin,nch,time,power);    
npower_stim(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_2_FFT.mat',lowerbin,upperbin,nch,time,power);    
npower_stim(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_2_FFT.mat',lowerbin,upperbin,nch,time,power);    
npower_stim(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_2_FFT.mat',lowerbin,upperbin,nch,time,power);    
%% Baseline        
npower_bas(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_1_FFT.mat',lowerbin,upperbin,nch,time,power);    
npower_bas(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_1_FFT.mat',lowerbin,upperbin,nch,time,power);    
npower_bas(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_1_FFT.mat',lowerbin,upperbin,nch,time,power);            
npower_bas(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_1_FFT.mat',lowerbin,upperbin,nch,time,power);  
npower_bas(1,:)=powerindi128time_noVis('E:\User_Sara\AmpServer\Studies\SWS_MTM\Data\SWS_MTM\EGI\SWS_001_AG\session1\sleep\Analysis\001_AG_1_FFT.mat',lowerbin,upperbin,nch,time,power);
            
%% GroupMean

mPower_bas=nanmean(npower_bas);
mPower_stim=nanmean(npower_stim);


ndxscalemax=max([mPower_bas mPower_stim]);
ndxscalemin=min([mPower_bas mPower_stim]);


figure
subplot(3,1,1)
mndxfinite_bas=isfinite(mPower_bas);
matnet128(mndxfinite_bas);
topoplot_sf(mPower_bas(1,mndxfinite_bas),'test128.txt','maplimits',[ndxscalemin,ndxscalemax],'conv','on','intrad',0.5,'electrodes','on');
colorbar
if time==1
    title(['FH Baseline ',num2str(lowerbin/4-0.25),'-',num2str(upperbin/4-0.25)])
elseif time==200
    title(['LH Baseline ',num2str(lowerbin/4-0.25),'-',num2str(upperbin/4-0.25)])
elseif time==100
    title(['all Night Baseline ',num2str(lowerbin/4-0.25),'-',num2str(upperbin/4-0.25)])
end
subplot(3,1,2)
mndxfinite_stim=isfinite(mPower_stim);
matnet128(mndxfinite_stim);
topoplot_sf(mPower_stim(1,mndxfinite_stim),'test128.txt','maplimits',[ndxscalemin,ndxscalemax],'conv','on','intrad',0.5,'electrodes','on');
colorbar
title(['Stim ',num2str(lowerbin/4-0.25),'-',num2str(upperbin/4-0.25)])
subplot(3,1,3)
ratio=mPower_stim./mPower_bas;
rationdxfinit=isfinite(ratio);
matnet128(rationdxfinit);
topoplot_sf(ratio(rationdxfinit),'test128.txt','maplimits','maxmin','conv','on','intrad',0.5,'electrodes','on');
colorbar
title(['Ratio(Stim/Baseline)',num2str(lowerbin/4-0.25),'-',num2str(upperbin/4-0.25)])  




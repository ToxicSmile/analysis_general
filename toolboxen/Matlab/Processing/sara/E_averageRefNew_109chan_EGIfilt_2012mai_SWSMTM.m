%function averageRefRot(filename,nartf)
%calculates average reference files
%rh, 27.3.06
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; close all;


sub=dir('F:\ff\SWS*');

Session={'session1\' 'session2\'};

for s=1:length(sub)
    
    for l=1:2 %% session loop
        
        Folder=['F:\ff\',sub(s).name,'\',Session{l}];
        %Folder=['F:\SWS_MTM\data\EGI\ff\',sub(s).name,'\',Session{l},'sleep\'];
        RafFile=dir([Folder,'*.raf']);
        filename_1=RafFile(1).name;
        filename=filename_1(4:end-4);
        numfile=length(RafFile);
        filepath=Folder;
        

        dataFormat='int16';

        %%%%get good channels
        if numfile<10
            numfilstra=['0' num2str(numfile)];
        else
            numfilstra=num2str(numfile);
        end
        eval(['load ',filepath,'01_',numfilstra,'_',filename,'.mat artndxn nch -mat'])

        
        artndxSum=sum(artndxn');
        ndxgoodch=find(artndxSum>0);
        clear artndxn;

        % filenameavr=['c:\data\human\newnet\',nf,filename,'.avr'];
        % fidavr=fopen(filenameavr,'wb');

        %%%%calculates average reference data for each file
        ffttot=[];
        restdata=[];
        fprintf('processing file ')
        
        for i=1:numfile

            fprintf('%2.0f ',i)
            fftblock=[]; data=[];

            if i<10
                numfilstr=['0' num2str(i)];
            else
                numfilstr=num2str(i);
            end

            eval(['load ',filepath,numfilstr,'_',filename,'.mat newsamp nch fsraf -mat']);
           % eval(['load C:\User\Sara\AmpServer\Data\009_FH\',num2str(i),'_009_FH_data_trigger.mat']);



        %     filenameraf=[filepath,numfilstr,'_',filename,'fil.raf'];                  %%%%entweder Zeile 44 oder 45
            filenameraf=[filepath,numfilstr,'_',filename,'.raf'];
            fidraf=fopen(filenameraf,'rb');

            data=fread(fidraf,[newsamp,nch],dataFormat);

            %triggerndx=floor(tPos*(fsraf/500));

            data=[restdata;data];
            excludedchannels=[43 48 49 56 63 68 73 81 88 94 99 107 113 119 120 125 126 127 128];
            ndx109=setdiff(ndxgoodch,excludedchannels);                                  %%%%selects only electrodes above ears (=109)
            average_ref=mean(data(:,ndx109),2); 

            newnewsamp=size(data,1);
            numepo=floor(newnewsamp/fsraf/20);
            restsamp=newnewsamp-numepo*20*fsraf;

            %%%%spectral analysis, 20s mean
            fftblock=zeros(nch,161,numepo);

            for channel=1:nch
                datax=double(data(:,channel));
                dataref=datax-average_ref;

        %         for j=1:length(triggerndx)
        %             k=triggerndx(j);
        %             dataref(k:k+128)=0;
        %         end


               for epoch=1:numepo
                    start=1+((epoch-1)*20*fsraf);
                    ending=20*fsraf+((epoch-1)*20*fsraf);
                    ffte=pwelch(dataref(start:ending),hanning(4*fsraf),0,4*fsraf,fsraf);
                    ffte=ffte(1:161);
                    fftblock(channel,:,epoch)=ffte;
                end
            end
            fftblock=shiftdim(fftblock,1);
            ffttot=[ffttot fftblock];

            if restsamp>0
                restdata=data(newnewsamp-restsamp:newnewsamp,:);
            else
                restdata=[];
            end
            fclose(fidraf);
             clear k triggerndx tPos
        end
        fprintf('\n')

        reference='average reference';
        ffttot_n=ffttot;

        %eval(['save ',filepath,'01_',numfilstra,'_',filename,'.mat ffttot_n -append']);

        eval(['save ',filepath,'01_',numfilstra,'_',filename,'.mat reference ffttot -append']);
        
    end
    
    clear Folder RafFile filename filename_1 filepath numfile  ffttot* restdata fft* ending start data*...
        average_ref restsamp newnewsamp numepo artndxSum epoch filenameraf ndxgoodch newsamp
end


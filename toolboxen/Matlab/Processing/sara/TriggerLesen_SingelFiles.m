clear all; close all;

%% SWS_TMS
fs=500;
channel=105;
ch=130;

sub=dir('D:\Daten\SWS\SWS_TMS\EGI\ST09RE\session2\');

for s=1%length(sub)
    display(['Sub = ',num2str(s)]);
    
    for l=2%1:2 %% session loop
        %fname=['E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\',sub(s).name,'\',Session{l},'sleep\']; 
        fname='D:\Daten\SWS\SWS_TMS\EGI\ST09RE\session2\'; 
        Fileraw=dir([fname,'*.raw']);
        numfile=length(Fileraw);
        rawname=Fileraw(1).name(4:end-4);
        saveFile=Fileraw(1).name(4:11);

        data=[];refl=[]; refr=[];
        for i=1:numfile
            display(i)
            foldername=[fname,'0',num2str(i),'_',rawname,'.raw'];
            eval('tPos=egiGetTriggersCorr(foldername,ch);','tPos=[]')    
            data = loadEGIBigRaw(foldername,channel);
            refr=loadEGIBigRaw(foldername,49);
            refl=loadEGIBigRaw(foldername,56);
            %data=[data data1]; refr=[refr refr1]; refl=[refl refl1];
            savename=[fname,num2str(i),'_',saveFile,'_data_ch',num2str(channel),'_trigger.mat'];
            save(savename,'data','refr','refl','channel','fs','tPos');
            clear data refr refl tPos savename
        end
        
        clear fname Fileraw numfile rawname saveFile
    end
end



%% SWS MTM
% fs=500;
% channel=92;
% ch=130;
% 
% sub=dir('E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\SWS*');
% Session={'session1\' 'session2\'};
% 
% for s=1:length(sub)
%     display(['Sub = ',num2str(s)]);
%     
%     for l=1:2 %% session loop
%         fname=['E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\',sub(s).name,'\',Session{l},'sleep\']; 
%         Fileraw=dir([fname,'*.raw']);
%         numfile=length(Fileraw);
%         rawname=Fileraw(1).name(4:end-4);
%         saveFile=Fileraw(1).name(4:11);
% 
%         data=[];refl=[]; refr=[];
%         for i=1:numfile
%             display(i)
%             foldername=[fname,'0',num2str(i),'_',rawname,'.raw'];
%             eval('tPos=egiGetTriggersCorr(foldername,ch);','tPos=[]')    
%             data = loadEGIBigRaw(foldername,channel);
%             refr=loadEGIBigRaw(foldername,49);
%             refl=loadEGIBigRaw(foldername,56);
%             %data=[data data1]; refr=[refr refr1]; refl=[refl refl1];
%             savename=[fname,num2str(i),'_',saveFile,'_data_ch',num2str(channel),'_trigger.mat'];
%             save(savename,'data','refr','refl','channel','fs','tPos');
%             clear data refr refl tPos savename
%         end
%         
%         clear fname Fileraw numfile rawname saveFile
%     end
% end

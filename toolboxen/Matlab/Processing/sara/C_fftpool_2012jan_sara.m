%fft all night
%rh, 20.8.07, checked_fpsk_2012jan.


clear all; close all;
%%
Folderpath='E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\';

%%

%%%Nacht1:
%filename='ST_001FW2201407292201';
%subfolder='E:\User_Sara\AmpServer\SWS_TMS\Pilots\001_FW\session2\';
%outputfolder='S:\user\Patrick\Daten_BIOKurs\005_FW\Session2\';
%%%Nacht2:
% filename='014pp201105102128';
% subfolder='014pp_2\';

%numfile=9;

%load('C:\User\Sara\AmpServer\Data\009_FH\DataFormat.mat')

%%
currentfolder=cd;
directory = uigetdir(Folderpath,'select Folder containing .raf files');
cd(directory)
[file_name] = uigetfile('*.raf', 'Select the firts .raf file');
filename=file_name(4:end-4);

numfile=input('HOW MANY .RAW FILES DO YOU WANT TO PROCESS? ');

subfolder=[directory,'\'];
cd(currentfolder);


ffttot=[];
restdata=[];
fprintf('processing file ')
for i=1:numfile
    fprintf('%2.0f ',i)
    fftblock=[]; data=[];

    if i<10
        numfilstr=['0' num2str(i)];
    else
        numfilstr=num2str(i);
    end

    eval(['load ',subfolder,numfilstr,'_',filename,'.mat newsamp nch fsraf']);
    
    

    filenameavr=[subfolder,numfilstr,'_',filename,'.raf'];
    fidref=fopen(filenameavr,'rb');

    data=fread(fidref,[newsamp,nch],'int16=>int16');
    %data=fread(fidref,[newsamp,nch],dataFormat);
    
    data=[restdata;data];
    
    newnewsamp=size(data,1);
    numepo=floor(newnewsamp/fsraf/20);
    numepo4s=numepo*5;
    restsamp=newnewsamp-numepo*20*fsraf;

    %%%%spectral analysis, 20s mean
    fftblock=zeros(nch,161,numepo);
    for channel=1:nch
        
        datax=double(data(:,channel));
        fft=zeros(161,1);
        for epoch=1:numepo
            for ii=1:5
                start=((ii+(epoch-1)*5)-1)*4*fsraf+1;
                ending=(ii+(epoch-1)*5)*4*fsraf;
                ffte=pwelch(datax(start:ending),hanning(4*fsraf),0,4*fsraf,fsraf);
                fft=fft+ffte(1:161);
            end
            fft=fft./5;
            fftblock(channel,:,epoch)=fft;
        end
    end
    fftblock=shiftdim(fftblock,2);
    ffttot=[ffttot;fftblock];
    
    if restsamp>0
        restdata=data(newnewsamp-restsamp:newnewsamp,:);
    else
        restdata=[];
    end

end
fprintf('\n')

ffttot=shiftdim(ffttot,1);

if numfile<10
    numfilstra=['0' num2str(numfile)];
else
    numfilstra=num2str(numfile);
end
fs=fsraf;
reference='Cz referenced';


%eval(['save ','D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\bs\ST31LE_Art_neu\01_',numfilstra,'_',filename,'.mat ffttot numfile fs reference -mat']);
eval(['save ',subfolder,'01_',numfilstra,'_',filename,'.mat ffttot numfile fs reference -mat']);
%eval(['save ',outputfolder,'01_',numfilstra,'_',filename,'.mat ffttot numfile fs reference -mat']);




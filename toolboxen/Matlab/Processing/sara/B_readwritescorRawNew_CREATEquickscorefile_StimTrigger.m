%function readwritescorNew(filename,visid)

clear all; close all;
filename='004_MH_1201408081936';
visname='004_MH_1';
vispath='E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\SWS_004_MH\session1\sleep\'; %output
datapath='E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\SWS_004_MH\session1\sleep\'; %input
numfile=8;
chtrigger=130;

% loadname=['C:\User\Sara\AmpServer\Data\009_FH\',num2str(i),'_009_FH_data_trigger.mat'];
% load(loadname,'tPos');


%%
dataFormat='int16';
locutoff=0.5;
hicutoff=40;
fs=128;
fsraw=500;


scordat=[];
restdata=[];
ffttot=[];

for i=1:numfile
    clear data datar datax 
    scorblock=[];
    
    if i<10
        numfilstr=['0' num2str(i)];
    else
        numfilstr=num2str(i);
    end
    
 
    filenameraw=[datapath,numfilstr,'_',filename,'.raw'];
    
    eval('tPos=egiGetTriggersCorr(filenameraw,chtrigger);','tPos=[]')    
  

    
    fid = fopen(filenameraw,'rb','b');
    [segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fid);
    fclose(fid);
    newsamp = NSamp;
    fsraw=Samp_Rate;


    
    triggerndx=floor(tPos*(fs/fsraw));
    clear tPos
    
        %     f3  f4  c3  c4 o1 o2 a1  a2  eog1  eog2   emg extrachannel(change if necessary!!!!)
%     ndxch=[24 124 36 104 70 83 57 100 128 1 125 32 49 56];
    ndxch=[24 124 36 104 70 83 57 100 128 1 125 32 107 113 92 56 49];
    %      1   2   3  4  5  6   7  8   9 10  11 12 13  14  15 16 17

    
    for ch=1:length(ndxch)
        datax = loadEGIBigRaw(filenameraw,ndxch(ch));
        dataf=eegfilt(datax,fsraw,0,hicutoff);
        dataf2=eegfilt(dataf,fsraw,locutoff,0);
        datar(ch,:)=resample(dataf2,fs,fsraw);
    end     
    
    triggerch=zeros(1,size(datar,2));
    linkedRef=(datar(16,:)+ datar(17,:))./2;
    
    f3a2=(datar(1,:)-datar(8,:));
    f4a1=(datar(2,:)-datar(7,:));
    c3a2=(datar(3,:)-datar(8,:));
    c4a1=(datar(4,:)-datar(7,:));
    o1a2=(datar(5,:)-datar(8,:));
    o2a1=(datar(6,:)-datar(7,:));
    eog1=(datar(9,:)-datar(10,:));
    eog2=(datar(11,:)-datar(12,:));
    emg=(datar(13,:)-datar(14,:));
    Stim=(datar(15,:)-linkedRef);
    triggerch(1,triggerndx)=1;
    
    eog1f=eegfilt(eog1,128,0,40);
    eog2f=eegfilt(eog2,128,0,40);
    emgf=eegfilt(emg,128,20,40);

    scorblock=[emgf;eog1f;eog2f;f3a2;f4a1;c3a2;c4a1;o1a2;o2a1;triggerch;Stim];
    
    scordat=[scordat scorblock];
    
        %%%%calculates spectra for c3a2
    data_spec=[restdata;c3a2'];

    newnewsamp=size(data_spec,1);
    numepo4s=floor(newnewsamp/fs/4);
    restsamp=newnewsamp-numepo4s*4*fs;

    %%%%spectral analysis, 4s
    fftblock=zeros(30,numepo4s);
    datax=double(data_spec);
    for epoch=1:numepo4s
        start=1+((epoch-1)*4*fs);
        ending=4*fs+((epoch-1)*4*fs);
        ffte=pwelch(datax(start:ending),hanning(4*fs),0,4*fs,fs);
        ffte=ffte(1:120);
        fft30=mean(reshape(ffte,4,30));
        fftblock(:,epoch)=fft30;
    end
    ffttot=[ffttot fftblock];

    if restsamp>0
        restdata=data_spec(newnewsamp-restsamp:newnewsamp);
    else
        restdata=[];
    end

end


%%%%write scoring file
filenamescor=[vispath,visname,'.r11']

fid = fopen(filenamescor,'w')
fwrite(fid,scordat,'short')
fclose(fid);

%%%%write sp1 file
filenamespec=[vispath,visname,'.sp1']
fid = fopen(filenamespec,'w')
fwrite(fid,ffttot,'float')
fclose(fid);



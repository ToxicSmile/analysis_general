%% A1 Loop File 
% Valid for files using a continuous two-digit sequence in their naming scheme.
% Select the first *.raw data file when prompted, and enter the total number of 
% *.raw data files present in the same directory.  The downsampling will be 
% performed on all *.raw files, with the *.raf file output, downsampled and filtered
% saved in the same directory.
% GL2013

clear all; close all
%%
% Folderpath='D:\Daten\SWS\SWS_TMS\EGI\';
% %sub=dir([Folderpath,'0*']);
% sub={'ST19EH' 'ST23GC'}
% 
% 
% for s=1:length(sub)
%     foldersub=[Folderpath,sub{s},'\session2\'];
%     rawfiles=dir([foldersub,'*.raw']);
%     numfile=length(rawfiles);
%     filebase=rawfiles(1).name(3:end);
%     
% 
%     for k = 1 : numfile;
%         K=num2str(k);
%         if k<10
%             fileloop = strcat(foldersub,'0',K,filebase);  % concatenates the directory and filename into one string
%         else
%             fileloop = strcat(foldersub,K,filebase); 
%         end
%         A2_downSampFilt_vectorized(fileloop);  % Create the *.raf files from the *.raw, filtered & downsampled.
%     end
%     
%     clear filebase numfile rawfiles foldersub
% end
%%
Folderpath={'D:\Daten\SWS\SWS_TMS\EGI\ST31LE\session2\' 'D:\Daten\SWS\SWS_TMS\EGI\ST31LE\session3\'};



for s=1:length(Folderpath)
    foldersub=Folderpath{s};
    rawfiles=dir([foldersub,'*.raw']);
    numfile=length(rawfiles);
    filebase=rawfiles(1).name(3:end);
    

    for k = 1 : numfile;
        K=num2str(k);
        if k<10
            fileloop = strcat(foldersub,'0',K,filebase);  % concatenates the directory and filename into one string
        else
            fileloop = strcat(foldersub,K,filebase); 
        end
        A2_downSampFilt_vectorized(fileloop);  % Create the *.raf files from the *.raw, filtered & downsampled.
    end
    
    clear filebase numfile rawfiles foldersub
end
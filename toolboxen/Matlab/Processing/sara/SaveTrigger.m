
clear all; close all;

%%
ch=130;
fs=500;

%% SWS_TMS

%sub={'ST03CR' 'ST07MS' 'ST17GW' 'ST19EH' 'ST23GC' 'ST26DK' 'ST30NH' 'ST31LE'}; %  

sub={'ST20FG'}; %  

Session={'session2\' 'session3\'};
%Session={'stim\'};


%SaveFolder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\';

for s=1:length(sub)
    display(['Sub = ',num2str(s)]);
   
    for l=1:length(Session)
        Folder=['D:\Daten\SWS\SWS_TMS\EGI\newStudy\',sub{s},'\',Session{l}];
        %SaveFile=[SaveFolder,Session{l},'newStudy\',sub{s},'_',num2str(l+1),'_FFT.mat'];
        SaveFile=[Folder,sub{s},'_',num2str(l+1),'_FFT.mat'];
        
        RafFile=dir([Folder,'*raf']);
        filename_1=RafFile(1).name;
        rawname=filename_1(4:end-4);
        numfile=length(RafFile);
        
        load(SaveFile,'NStim_LV')
        
        if exist('NStim_LV')
        
            tPos_all=[];
            samp=0;

            for i=1:numfile
                display(i)
                foldername=[Folder,'0',num2str(i),'_',rawname,'.raw'];
                eval('tPos=egiGetTriggersCorr(foldername,ch);','tPos=[]') 
                data = loadEGIBigRaw(foldername,1); 
                tPos_row=tPos+samp;
                tPos_all=[tPos_all,tPos_row]; 
                currentsamp=length(data);
                samp=samp+currentsamp;  
                savename=[Folder,'0',num2str(i),'_',rawname,'_trigger.mat'];
                save(savename,'fs','tPos');
                clear data tPos savename tPos_row
            end




            eval(['save ',SaveFile,' tPos_all -append']);
        
        end
        
        clear  Fileraw fname numfile rawname tPos_all samp SaveFile NStim_LV
    end
end


%% SWS_MTM
% sub=dir('E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\SWS*');
% Session={'session1\' 'session2\'};
% 
% for s=1:length(sub)
%     display(['Sub = ',num2str(s)]);
%     
%     for l=1:2 %% session loop
%         fname=['E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\',sub(s).name,'\',Session{l},'sleep\']; 
%         saveFile=['E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\',sub(s).name,'\',Session{l},'sleep\Analysis\']; 
%         Fileraw=dir([fname,'*.raw']);
%         numfile=length(Fileraw);
%         rawname=Fileraw(1).name(4:end-4);
% 
%         tPos_all=[];
%         samp=0;
% 
%         for i=1:numfile
%             display(i)
%             foldername=[fname,'0',num2str(i),'_',rawname,'.raw'];
%             eval('tPos=egiGetTriggersCorr(foldername,ch);','tPos=[]') 
%             data = loadEGIBigRaw(foldername,1); 
%             tPos_row=tPos+samp;
%             tPos_all=[tPos_all,tPos_row]; 
%             currentsamp=length(data);
%             samp=samp+currentsamp;  
%             savename=[fname,'0',num2str(i),'_',rawname,'_trigger.mat'];
%             save(savename,'fs','tPos');
%             clear data tPos savename tPos_row
%         end
% 
% 
% 
%         %%% save tPos_all in long .mat File
% %         if numfile<10
% %             numfilstra=['0' num2str(numfile)];
% %         else
% %             numfilstra=num2str(numfile);
% %         end
% 
%         eval(['save ',saveFile,rawname(1:8),'_FFT.mat tPos_all -append']);
%         
%         clear  Fileraw fname numfile rawname tPos_all samp saveFile
%     end
% end


%% Local_SWS
% %sub=dir('E:\User_Sara\AmpServer\Studies\Local_SWS\Data\EGI\LS*');
% sub=dir('D:\Daten\SWS\Local_SWS\EGI\LS*');
% Session={'session1\' 'session2\'};
% 
% for s=4:length(sub)
%     display(['Sub = ',num2str(s)]);
%     
%     for l=1:2 %% session loop
%         fname=['D:\Daten\SWS\Local_SWS\EGI\',sub(s).name,'\',Session{l}]; 
%         saveFile=['D:\Daten\SWS\Local_SWS\EGI\',sub(s).name,'\',Session{l},'Analysis\']; 
%         Fileraw=dir([fname,'*.raw']);
%         numfile=length(Fileraw);
%         rawname=Fileraw(1).name(4:end-4);
% 
%         tPos_all=[];
%         samp=0;
% 
%         for i=1:numfile
%             display(i)
%             foldername=[fname,'0',num2str(i),'_',rawname,'.raw'];
%             eval('tPos=egiGetTriggersCorr(foldername,ch);','tPos=[]') 
%             data = loadEGIBigRaw(foldername,1); 
%             tPos_row=tPos+samp;
%             tPos_all=[tPos_all,tPos_row]; 
%             currentsamp=length(data);
%             samp=samp+currentsamp;  
%             savename=[fname,'0',num2str(i),'_',rawname,'_trigger.mat'];
%             save(savename,'fs','tPos');
%             clear data tPos savename tPos_row
%         end
% 
% 
% 
%         %%% save tPos_all in long .mat File
% %         if numfile<10
% %             numfilstra=['0' num2str(numfile)];
% %         else
% %             numfilstra=num2str(numfile);
% %         end
% 
%         eval(['save ',saveFile,rawname(1:8),'_FFT.mat tPos_all']);
%         %eval(['save ',saveFile,rawname(1:8),'_FFT.mat tPos_all -append']);
%         
%         clear  Fileraw fname numfile rawname tPos_all samp saveFile
%     end
% end



%% Save Testing
%sub=dir('E:\User_Sara\AmpServer\Studies\Local_SWS\Data\EGI\LS*');
% Path=('F:\AmpServerProject\Test_Saving\');
% file=dir([Path,'*.raw']);
% 
% for s=1:length(file)
% 
%     fname=[Path,file(s).name]; 
%     saveFile=fname(1:end-4); 
%   
%     foldername=fname;
%     eval('tPos=egiGetTriggersCorr(foldername,ch);','tPos=[]')
%     
%      eval(['save ',saveFile,'.mat tPos']); 
%      clear  Fileraw fname numfile rawname tPos samp saveFile
% end
%     



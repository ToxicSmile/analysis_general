clear all;
close all;

Session={'session2\' 'session3\'};
sub={'ST07MS' 'ST17GW' 'ST19EH' 'ST23GC' 'ST26DK'}; % 

Folder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\';
Folder_Data='D:\Daten\SWS\SWS_TMS\EGI\';

for l=1:length(Session)
     Folderpath=[Folder,Session{l},'newStudy\'];

     for s=1:length(sub)
         
         loadfile=[Folderpath,sub{s},'_',num2str(l)+1,'_FFT.mat'];
         
         load(loadfile, 'tPos_*','StimCh');
         
         if exist('tPos_allCor')
             Folder_raw=dir([Folder_Data,sub{s},'\',Session{l},'*FiltData_allnight.mat']);
             load([Folder_Data,sub{s},'\',Session{l},Folder_raw(1).name],'data','fs');
             
             wrongflags=setdiff(tPos_all,tPos_allCor);
             
%              for i=1:length(wrongflags)
%                  fig=figure
%                  plot([wrongflags(i)-5*fs:wrongflags(i)+5*fs],data(wrongflags(i)-5*fs:wrongflags(i)+5*fs))
%                  hold on
%                  plot(wrongflags(i),data(wrongflags(i)),'r*')
%                  hold on
%                  hline(0);hline(-30)
%                  maximize(fig)
%                  pause 
%                  close all
%              end

                for i=1:length(tPos_allCor)
                     fig=figure
                     plot([tPos_allCor(i)-5*fs:tPos_allCor(i)+5*fs],data(tPos_allCor(i)-5*fs:tPos_allCor(i)+5*fs))
                     hold on
                     plot(tPos_allCor(i),data(tPos_allCor(i)),'r*')
                     hold on
                     hline(0);hline(-30)
                     maximize(fig)
                     pause 
                     close all
                end




             clear data tPos_* StimCh  wrongflags
         end
     end
end
                 
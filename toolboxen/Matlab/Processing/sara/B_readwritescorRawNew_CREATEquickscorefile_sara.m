%function readwritescorNew(filename,visid)

clear all; close all;

Folderpath='D:\Data_BMS\EGI\sleep\BMS_008\session1\sleep';

%%
% filename='002_YE_2 20140314 2332';
% visname='001_YE_1';
% vispath='E:\User_Sara\AmpServer\Data\Pilot\Adelinn\002_YE\Session1\sleep\'; %output
% datapath='E:\User_Sara\AmpServer\Data\Pilot\Adelinn\002_YE\Session1\sleep\EGI\'; %input
% numfile=8;

%%
currentfolder=cd;
directory = uigetdir(Folderpath,'select Folder containing .raw files');
cd(directory)
[file_name] = uigetfile('*.raw', 'Select the firts .raw file');
filename=file_name(4:end-4);

numfile=input('HOW MANY .RAW FILES DO YOU WANT TO PROCESS? ');
visname=input('Type name of vis file as string: ');

vispath=[directory,'\'];
datapath=[directory,'\'];
cd(currentfolder);


dataFormat='int16';
locutoff=0.5;
hicutoff=40;
fs=128;
fsraw=500;


scordat=[];
restdata=[];
ffttot=[];
for i=1:numfile
    clear data datar datax 
    scorblock=[];
    
    if i<10
        numfilstr=['0' num2str(i)];
    else
        numfilstr=num2str(i);
    end
    
    filenameraw=[datapath,numfilstr,'_',filename,'.raw'];
    fid = fopen(filenameraw,'rb','b')
    [segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fid);
    fclose(fid);
    newsamp = NSamp;
    fsraw=Samp_Rate;

   
        %     f3  f4  c3  c4 o1 o2 a1  a2  eog1  eog2   emg(change if necessary!!!!)
%     ndxch=[24 124 36 104 70 83 57 100 128 1 125 32 49 56];
    ndxch=[24 124 36 104 70 83 57 100 128 1 125 32 107 113];
    %      1   2   3  4  5  6   7  8   9 10 11  12 13 14

    
    for ch=1:14
        datax = loadEGIBigRaw(filenameraw,ndxch(ch));
        dataf=eegfilt(datax,fsraw,0,hicutoff);
        dataf2=eegfilt(dataf,fsraw,locutoff,0);
        datar(ch,:)=resample(dataf2,fs,fsraw);
    end     
    
    f3a2=(datar(1,:)-datar(8,:));
    f4a1=(datar(2,:)-datar(7,:));
    c3a2=(datar(3,:)-datar(8,:));
    c4a1=(datar(4,:)-datar(7,:));
    o1a2=(datar(5,:)-datar(8,:));
    o2a1=(datar(6,:)-datar(7,:));
    eog1=(datar(9,:)-datar(10,:));
    eog2=(datar(11,:)-datar(12,:));
    emg=(datar(13,:)-datar(14,:));

    eog1f=eegfilt(eog1,128,0,40);
    eog2f=eegfilt(eog2,128,0,40);
    emgf=eegfilt(emg,128,20,40);

    scorblock=[emgf;eog1f;eog2f;f3a2;f4a1;c3a2;c4a1;o1a2;o2a1];
    
    scordat=[scordat scorblock];
    
        %%%%calculates spectra for c3a2
    data_spec=[restdata;c3a2'];

    newnewsamp=size(data_spec,1);
    numepo4s=floor(newnewsamp/fs/4);
    restsamp=newnewsamp-numepo4s*4*fs;

    %%%%spectral analysis, 4s
    fftblock=zeros(30,numepo4s);
    datax=double(data_spec);
    for epoch=1:numepo4s
        start=1+((epoch-1)*4*fs);
        ending=4*fs+((epoch-1)*4*fs);
        ffte=pwelch(datax(start:ending),hanning(4*fs),0,4*fs,fs);
        ffte=ffte(1:120);
        fft30=mean(reshape(ffte,4,30));
        fftblock(:,epoch)=fft30;
    end
    ffttot=[ffttot fftblock];

    if restsamp>0
        restdata=data_spec(newnewsamp-restsamp:newnewsamp);
    else
        restdata=[];
    end

end


%%%%write scoring file
filenamescor=[vispath,visname,'.r09']

fid = fopen(filenamescor,'w')
fwrite(fid,scordat,'short')
fclose(fid);

%%%%write sp1 file
filenamespec=[vispath,visname,'.sp1']
fid = fopen(filenamespec,'w')
fwrite(fid,ffttot,'float')
fclose(fid);
    

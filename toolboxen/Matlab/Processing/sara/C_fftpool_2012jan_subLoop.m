%fft all night
%rh, 20.8.07, checked_fpsk_2012jan.


clear all; close all;

sub={'F:\SWS_TMS\EGI\ST30NH\session2\' 'F:\SWS_TMS\EGI\ST30NH\session3\' 'F:\SWS_TMS\EGI\ST03CR\session2\' 'F:\SWS_TMS\EGI\ST03CR\session3\' 'D:\Daten\SWS\SWS_TMS\EGI\ST31LE\session2\' 'D:\Daten\SWS\SWS_TMS\EGI\ST31LE\session3\'};

for s=1:length(sub)
   
   
        Folder=sub{s};
        RafFile=dir([Folder,'*raf']);
        filename_1=RafFile(1).name;
        filename=filename_1(4:end-4);
        numfile=length(RafFile);
        filepath=Folder;
        subfolder=Folder;

        ffttot=[];
        restdata=[];
        fprintf('processing file ')
        
        for i=1:numfile
            fprintf('%2.0f ',i)
            fftblock=[]; data=[];

            if i<10
                numfilstr=['0' num2str(i)];
            else
                numfilstr=num2str(i);
            end

            eval(['load ',subfolder,numfilstr,'_',filename,'.mat newsamp nch fsraf']);



            filenameavr=[subfolder,numfilstr,'_',filename,'.raf'];
            fidref=fopen(filenameavr,'rb');

            data=fread(fidref,[newsamp,nch],'int16=>int16');
            %data=fread(fidref,[newsamp,nch],dataFormat);

            data=[restdata;data];

            newnewsamp=size(data,1);
            numepo=floor(newnewsamp/fsraf/20);
            numepo4s=numepo*5;
            restsamp=newnewsamp-numepo*20*fsraf;

            %%%%spectral analysis, 20s mean
            fftblock=zeros(nch,161,numepo);
            for channel=1:nch

                datax=double(data(:,channel));
                fft=zeros(161,1);
                for epoch=1:numepo
                    for ii=1:5
                        start=((ii+(epoch-1)*5)-1)*4*fsraf+1;
                        ending=(ii+(epoch-1)*5)*4*fsraf;
                        ffte=pwelch(datax(start:ending),hanning(4*fsraf),0,4*fsraf,fsraf);
                        fft=fft+ffte(1:161);
                    end
                    fft=fft./5;
                    fftblock(channel,:,epoch)=fft;
                end
            end
            fftblock=shiftdim(fftblock,2);
            ffttot=[ffttot;fftblock];

            if restsamp>0
                restdata=data(newnewsamp-restsamp:newnewsamp,:);
            else
                restdata=[];
            end

        end
        fprintf('\n')

        ffttot=shiftdim(ffttot,1);

        if numfile<10
            numfilstra=['0' num2str(numfile)];
        else
            numfilstra=num2str(numfile);
        end
        fs=fsraf;
        reference='Cz referenced';


        eval(['save ',subfolder,'01_',numfilstra,'_',filename,'.mat ffttot numfile fs reference -mat']);
        clear  Folder RafFile filename_1 filename filepath subfolder  ffttot restdata newsamp nch fsraf...
            filenameavr data newnewsamp numepo restsamp fftblock datax fft
              
 
end

%% SWS TMS
% Session={'session1\'};
% 
% sub={'ST02BL' 'ST13DK' 'ST25BU'}; % 'ST05TS' 'ST09RE'
% 
% for s=1:length(sub)
%    
%     for l=1:length(Session)
%         Folder=['D:\Daten\SWS\SWS_TMS\EGI\',sub{s},'\',Session{l}];
%         RafFile=dir([Folder,'*raf']);
%         filename_1=RafFile(1).name;
%         filename=filename_1(4:end-4);
%         numfile=length(RafFile);
%         filepath=Folder;
%         subfolder=Folder;
% 
%         ffttot=[];
%         restdata=[];
%         fprintf('processing file ')
%         
%         for i=1:numfile
%             fprintf('%2.0f ',i)
%             fftblock=[]; data=[];
% 
%             if i<10
%                 numfilstr=['0' num2str(i)];
%             else
%                 numfilstr=num2str(i);
%             end
% 
%             eval(['load ',subfolder,numfilstr,'_',filename,'.mat newsamp nch fsraf']);
% 
% 
% 
%             filenameavr=[subfolder,numfilstr,'_',filename,'.raf'];
%             fidref=fopen(filenameavr,'rb');
% 
%             data=fread(fidref,[newsamp,nch],'int16=>int16');
%             %data=fread(fidref,[newsamp,nch],dataFormat);
% 
%             data=[restdata;data];
% 
%             newnewsamp=size(data,1);
%             numepo=floor(newnewsamp/fsraf/20);
%             numepo4s=numepo*5;
%             restsamp=newnewsamp-numepo*20*fsraf;
% 
%             %%%%spectral analysis, 20s mean
%             fftblock=zeros(nch,161,numepo);
%             for channel=1:nch
% 
%                 datax=double(data(:,channel));
%                 fft=zeros(161,1);
%                 for epoch=1:numepo
%                     for ii=1:5
%                         start=((ii+(epoch-1)*5)-1)*4*fsraf+1;
%                         ending=(ii+(epoch-1)*5)*4*fsraf;
%                         ffte=pwelch(datax(start:ending),hanning(4*fsraf),0,4*fsraf,fsraf);
%                         fft=fft+ffte(1:161);
%                     end
%                     fft=fft./5;
%                     fftblock(channel,:,epoch)=fft;
%                 end
%             end
%             fftblock=shiftdim(fftblock,2);
%             ffttot=[ffttot;fftblock];
% 
%             if restsamp>0
%                 restdata=data(newnewsamp-restsamp:newnewsamp,:);
%             else
%                 restdata=[];
%             end
% 
%         end
%         fprintf('\n')
% 
%         ffttot=shiftdim(ffttot,1);
% 
%         if numfile<10
%             numfilstra=['0' num2str(numfile)];
%         else
%             numfilstra=num2str(numfile);
%         end
%         fs=fsraf;
%         reference='Cz referenced';
% 
% 
%         eval(['save ',subfolder,'01_',numfilstra,'_',filename,'.mat ffttot numfile fs reference -mat']);
%         clear  Folder RafFile filename_1 filename filepath subfolder  ffttot restdata newsamp nch fsraf...
%             filenameavr data newnewsamp numepo restsamp fftblock datax fft
%               
%     end
% end
       

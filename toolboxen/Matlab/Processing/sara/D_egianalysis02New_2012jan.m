%artefact/bad channel rejection
%rh, 08.23.07, fp 11.11.11

clear all; close all;

filepath='F:\ff\SWS_012_RS\session2\';
vispath='F:\ff\SWS_012_RS\session2\';
visname= '012_RS_2';
filename='01_08_012_RS_2201501202245';
filenameimp='01_012_RS_2201501202245';


nch=128;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load([filepath,filename,'.mat']);

VisName=[vispath,visname,'.VIS'];

offset=0;
[vistrack,vissymb,offset]=readtrac(VisName,1);

%vissymb=vissymb(1:900);                %%Einschränkung auf erste 5h (1h=180)
%visstrack=vistrack(1:900,:);

VisNum=numvis(vissymb,offset);
maxep=length(VisNum);

visgood=find(sum(vistrack')==0);
vissleep=find(vissymb=='1' | vissymb=='2' | vissymb=='3' | vissymb=='4')';

Index_s=intersect(visgood,vissleep);

imp=load([filepath,filenameimp,'.prn']);

%%%%artefact/bad channel rejection
    
    %%%Zwischenspeicherung der Artefaktkorrektur
    save_ndx=[25 50 75 100 125];
    % eval(['load ',filepath,filename,'.mat artndxn -append']);
    
artndxn=zeros(nch,maxep);
for channel=1:nch
    channelimp=num2str(imp(channel,2));
    %channelimp='ok';
    channelstr=num2str(channel);
    fft=squeeze(ffttot(channel,:,:));
    [Index1,Aborted,Upper1,Factor1,NBins1]=Exclude(0.75,4.5,fft,Index_s,12,15,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);   %0:plot/prompting, 2:nothing
    [Index2,Aborted,Upper2,Factor2,NBins2]=Exclude(20,30,fft,Index_s,5,25,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);
    index=intersect(Index1,Index2);
    artndxn(channel,index)=1;
    if ~isempty(intersect(save_ndx,channel))
        eval(['save ',filepath,filename,'.mat artndxn -append']);
    end
end 




% artndxn_ref=artndxn;
% chndx=[49,56];
% ffttot=shiftdim(ffttot,2);
% for channel=1:length(chndx) %nch
%    
%     channelimp=num2str(imp(chndx(channel),2));
%     channelstr=num2str(chndx(channel));
%     fft=squeeze(ffttot(chndx(channel),:,:));
%     [Index1,Aborted,Upper1,Factor1,NBins1]=Exclude(0.75,4.5,fft,Index_s,12,15,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);   %0:plot/prompting, 2:nothing
%     [Index2,Aborted,Upper2,Factor2,NBins2]=Exclude(20,30,fft,Index_s,5,25,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);
%     index=intersect(Index1,Index2);
%     %artndxn(channel,index)=1;
%     artndxn_ref(chndx(channel),index)=1;
%     if ~isempty(intersect(save_ndx,chndx(channel)))
%         eval(['save ',filepath,filename,'.mat artndxn -append']);
%     end
% end    
  


%eval(['save ',filepath,filename,'.mat nch artndxn imp vissymb -append']);
eval(['save ',filepath,filename,'.mat nch artndxn vissymb -append']);
%eval(['save ',filepath,filename,'.mat nch artndxn_ref vissymb -append']);



clear; close all; clc;

%by Haldun Kufluoglu                                       April 2004
%
%            This code helps to read data points from plots,
%            first you need to calibrate this tool so that it 
%            can convert pixel information into useful values
%            from the plots. 
%            It asks you to click on the origin of the plot and
%            enter the (x,y) value as shown on the plot. Then, 
%            it wants the reference points on the x and y axes,
%            click on the division points and enter the values. 
%            
%  | (y)
%  |
%  |              For the x-axis division, enter the x value only,
%  |              for the y-axis division, enter the y value only.
%  |              The code assumes the origin y value for the former
%  |              and origin x value for the latter. The calibration  
%  |              is important, so if you are not satisfied with your
%  |              points, exit the program. Practise makes perfect, 
%  |              you can play with it until you feel comfortable. 
%  |
%  |
%  | 
%  |
% --- division reference of y-axis
%  | 
%  |
%  |
%  |        division reference of x-axis
%  |________|_________________________________________ (x)
%origin     | 
%
%           For the plots : 
%           Open a '.pdf' version of the document, use the 
%           'Image Selection' or 'Snapshot' tool to copy the graph,
%           using an image editing software such as Photoshop or 
%           Microsoft Imaging, save the plot as a '.jpeg', '.tiff'
%           or '.bmp'. '.tiff' gives the best results based on the
%           observations. When the code asks for a filename, enter
%           the full name including the '.' and the extension, 
%           example : IVplot.jpeg
iname = input('Enter the filename of the image :','s')
image(imread(iname));
hold on;

disp('------------------------------');
disp('Plot Types : ');
disp('------------------------------');
disp('1 - y vs x');
disp('2 - log(y) vs x');
disp('3 - y  vs log(x)');
disp('4 - log(y) vs log(x)');
disp('------------------------------');

ptype = input('\nChoose the plot type : ');
cond = 'c';
close; 
image(imread(iname)); hold on;
title(iname);
disp('Click on the origin of the plot');
[x0 y0] = ginput(1);

x0val = input('\nEnter the x value of the plot origin : ');
y0val = input('Enter the y value of the plot origin : ');
plot(x0, y0,'ro','LineWidth',2);

disp('');
disp('Click on the division reference of x-axis');
[xrefx xrefy] = ginput(1);
xref = input('\nEnter the x value of the division reference : ');
plot(xrefx, y0,'ro','LineWidth',2);

disp('');
disp('Click on the division reference of y-axis');
[yrefx yrefy] = ginput(1);
yref = input('\nEnter the y value of the division reference : ');
plot(x0, yrefy,'ro','LineWidth',2);

cont = input('Calibration points are selected, do you want to continue? (y/n) : ','s');
if cont == 'y' | cont == 'Y'
while cond ~= 'q'

dnum = input('How many data points do you want to read? : ');
dpts = zeros(dnum,2);
disp('Data points ( x , y ) are : '); disp('');
for i = 1:dnum
    dpts(i,:) = ginput(1);
    plot(dpts(i,1), dpts(i,2),'ro','LineWidth',2);
    
    switch (ptype)
    case 1
        dvaly = (yref - y0val)/(yrefy - y0)*(dpts(:,2) - y0) + y0val; 
        dvalx = (xref - x0val)/(xrefx - x0)*(dpts(:,1) - x0) + x0val;
    case 2
        dvaly = (log10(yref/y0val)/(yrefy - y0))*(dpts(:,2) - y0) + log10(y0val);
        dvalx = (xref - x0val)/(xrefx - x0)*(dpts(:,1) - x0) + x0val;
        dvaly = 10.^(dvaly);
    case 3
        dvaly = (yref - y0val)/(yrefy - y0)*(dpts(:,2) - y0) + y0val; 
        dvalx = (log10(xref/x0val)/(xrefx - x0))*(dpts(:,1) - x0) + log10(x0val);
        dvalx = 10.^(dvalx);        
    case 4
        dvaly = (log10(yref/y0val)/(yrefy - y0))*(dpts(:,2) - y0) + log10(y0val);
        dvalx = (log10(xref/x0val)/(xrefx - x0))*(dpts(:,1) - x0) + log10(x0val);        
        dvaly = 10.^(dvaly);  dvalx = 10.^(dvalx);
    end
    
    disp(['( ',num2str(dvalx(i)),' , ',num2str(dvaly(i)),' ) ']);
    
end

sy = input('Enter the filename to save the data : ','s');
save(sy,'dvalx','dvaly');
cond = input('Do you want to exit? (c for continue, q for quit) : ','s');

end % while cond    

else
  disp('Program ended');
  
end % if cont

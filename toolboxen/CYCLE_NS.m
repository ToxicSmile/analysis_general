function c=cycle(cyclefile,vissymb,offset)
%      function c=cycle(cyclefile,vissymb,offset)
%
%      CYCLE zykleninformationen aufbereiten
%      VISSYMB is sleep stage array obtained from READTRAC
%      OFFSET (first line in vis file) obtained from READTRAC
%
%      c(i,1): 1. 20-s epoche nrem zylus i
%      c(i,2): letzte 20-s epoche nrem zylus i
%      c(i,3): 1. 20-s epoche rem zylus i
%      c(i,4): letzte 20-s epoche rem zylus i
%
%      27.7.96, pa
%      14.4.99, tg


% lesen zyklenfile
[fpath,fname,fext]=fileparts(cyclefile);
if isempty(fext)
  fext='.cyc';
end
eval(['load ',fpath,'\',fname,fext]) 
eval(['cycf','=',fname,';'])
eval(['clear ',fname]);

cycf(:,1)=[];
for cyc=1:length(cycf(:,1))-1
  if cyc==1
		TmpNdx=find(vissymb=='2' | vissymb=='3' | vissymb=='4' | vissymb=='r');
		c(cyc,1)=TmpNdx(1)-offset;
  else   
     c(cyc,1)=cycf(cyc-1,2)+1;
  end
  c(cyc,2)=cycf(cyc,1);
  c(cyc,3)=cycf(cyc,1)+1;
  c(cyc,4)=cycf(cyc,2);
end
if cycf(cyc+1,1)>0 
  c(cyc+1,1)=cycf(cyc,2)+1;
  c(cyc+1,2)=cycf(cyc+1,1);
end

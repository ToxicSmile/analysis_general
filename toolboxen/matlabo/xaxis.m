function xx=xaxis(x,y)
%XAXIS set x axis
%
% 22:18 23.09.97, pa
% 21.04.99, tg

if nargin < 2
  y=x(2);
end
set(gca,'XLim',[x(1) y])

function [Vis]=PlotVisa(VisNum,density);

%  function [Vis]=PlotVis(VisNum,density);
%  Creates array from VisNum array which can be used to plot sleep stages as a line plot.
%  density indicates the desired number of vertical lines per epoch (default 1)
%
%  obtain VisNum e.g. with
%  [vistrack vissymb offset]=readtrac(FileName,1);
%  
%  VisNum=numvis(vissymb,offset)';
%
%  Plot results with plot(Vis(:,1),Vis(:,2))
%
%  Th.G. 5.8.99

if nargin<2
  density=1;
end;  

tempNdx=(1:1:length(VisNum))';
if VisNum(length(VisNum))==3
  VisNum(length(VisNum))=VisNum(length(VisNum)-1);
end;  

if density==1
	Vis1=zeros(length(VisNum)*3,1);
	Ndx=zeros(length(tempNdx)*3,1);
	for i=1:length(VisNum)
    x=(tempNdx(i)-0.5)/15/60;     %was /1.5/60
    Ndx(3*(i-1)+1)=x;
  	Ndx(3*(i-1)+2)=x;
  	Ndx(3*(i-1)+3)=NaN;
	  Vis1(3*(i-1)+1)=VisNum(i);
   	Vis1(3*(i-1)+2)=VisNum(i)+1;
   	Vis1(3*(i-1)+3)=NaN;
	end;
else
	Vis1=zeros(length(VisNum)*density*2+length(VisNum)-1,1);
	Ndx=zeros(length(tempNdx)*density*2+length(tempNdx)-1,1);
	for i=1:length(VisNum)
    Delta=1/density;
	  for j=1:density
      x=(tempNdx(i)-1+Delta/2+Delta*(j-1))/15/60;         %was /1.5/60
    	Ndx(2*(i-1)*density+(i-1)+2*(j-1)+1)=x;
	  	Ndx(2*(i-1)*density+(i-1)+2*(j-1)+2)=x;
		  Vis1(2*(i-1)*density+(i-1)+2*(j-1)+1)=VisNum(i);
    	Vis1(2*(i-1)*density+(i-1)+2*(j-1)+2)=VisNum(i)+1;
	  end
 		Ndx(2*(i-1)*density+(i-1)+2*density+1)=NaN;
	  Vis1(2*(i-1)*density+(i-1)+2*density+1)=NaN;
	end;
end;
Vis=[Ndx,Vis1];

%plot(Vis(:,1),Vis(:,2))

return;
function eps=readeps(hdrfilename);

% READEPS reads a HDR file and returns the number of 20s
%         epochs in the corresponding EEG file.
%_______________________________________________________
% Luca Finelli - 19.05.1999

% Last Update:   00.00.0000 lf - changes ...

fidhdr=fopen(hdrfilename,'r');
hdrline=fgets(fidhdr);
hdrline=fgets(fidhdr);
epsline=fgets(fidhdr);        % EPS info is on third line of hdr file


i=find(epsline=='(');
j=find(epsline==')');
k=find(epsline(i:j)==' ');

str=epsline(i+1:i+k-2);
eps=str2num(str);

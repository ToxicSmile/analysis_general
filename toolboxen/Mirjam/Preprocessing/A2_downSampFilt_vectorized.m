function A2_downSampFilt_vectorized(filenameraw)
% *.raw file high-and-low-pass filtration and downsampling, vectorized for your convenience.
% GL, 2013.

% [FileName, PathName] = uigetfile('*.raw', 'Select the first of the *.raw files you want to process...');
% To use this function as a stand-alone script, uncomment the above line and comment the others.



% clear all; close all;

fs=128;
locutoff=0.5; %highpass
hicutoff=40;  %lowpass
dataFormat='int16';
nch=128;

% filename='01_HB200708062236';
% datapath='C:\data\';

% filenameraw=[datapath,filename,'.raw']

% filenameraw = strcat(PathName, FileName); % concatenate the file and path into one string
filenameout = strcat(filenameraw(1: end - 4), '.raf'); % remove the ".raw" part, replace with ".raf"
% filenameout=[datapath,filename,'.raf']

if exist(filenameraw)

    fidraw = fopen(filenameraw,'rb','b');
    [segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fidraw); %#ok<NASGU>
    fclose(fidraw);

    fsegi=Samp_Rate;  %#ok<NASGU>

    fidtxt = fopen('checklist.txt','a');
    fprintf(fidtxt,'%s   %s%s',datestr(now),filenameraw(1:end-4));
%     %Commented out to save time.  filename doesn't exist right now, so it
%     gives errors...
    fclose(fidtxt);

    fidout=fopen(filenameout,'wb');

%     for channel=1:nch        
channel = 1 : 1 : 128; 
% for channel=1 : nch
%     datar = zeros(NSamp, nch);
%     channel
            data = loadEGIBigRaw(filenameraw,channel);
    data(channel, :) = eegfilt(data(channel, :),Samp_Rate,locutoff,0);
    data(channel, :) = eegfilt(data(channel, :),Samp_Rate,0,hicutoff);
    data = double(data(channel, :))';
    datar(:, channel) = resample(data(:, channel), fs, Samp_Rate);

%     channel = 65 : 1 : 128; 
%             data = loadEGIBigRaw(filenameraw,channel);
%     data(channel, :) = eegfilt(data(channel, :),Samp_Rate,locutoff,0);
%     data(channel, :) = eegfilt(data(channel, :),Samp_Rate,0,hicutoff);
%     DataResampVec = double(data(channel, :))';
%     datar(:, channel) = resample(DataResampVec(:, channel), fs, Samp_Rate);
% 
%         dataout = zeros(size(datar, 1), nch);


%%
    dataout(:, channel) = int16(datar(:, channel));
    
%         channel
%         data = loadEGIBigRaw(filenameraw,channel);
%   
%         data = eegfilt(data,Samp_Rate,locutoff,0);
%         data = eegfilt(data,Samp_Rate,0,hicutoff);
%         DataResampVec = double(data)';
%         datar = resample(DataResampVec, fs, Samp_Rate);
%         if channel ==1
%             dataout = zeros(size(datar,1),nch);
%         end
% %         fwrite(fidout,dataout,dataFormat);
% 
%         %                 fwrite(fidout,data,dataFormat);  % fwrite if downsampling is removed
%         %         fwrite(fidout,datar,dataFormat);
%         %         dataTEST2(:,channel) = datar;
%         %         fwrite(fidout,datar,dataFormat);
%         dataout(:, channel) = int16(datar(:,1));
%     end
%     data = dataTEST;
%     fwrite(fidout,datar,dataFormat);
        fwrite(fidout,dataout,dataFormat);
    fclose(fidout);
    newsamp=length(datar); %#ok<NASGU>
    fsraf=fs; %#ok<NASGU>

%     newsamp = NSamp;
    
    
    eval(['save ',filenameraw(1 : end - 4),'.mat newsamp nch fsraf fsegi -mat']);

    fidtxt = fopen('checklist.txt','a');
    fprintf(fidtxt,'   %s    Ch# %d   OK\n',datestr(now),channel);
    
    fclose all;

else
%     fidtxt = fopen('checklist.txt','a');
%     fprintf(fidtxt,'%s   %s%s   FILE NOT FOUND\n',datestr(now),datapath,filename);  % I don't know how to rename this/don't want to deal with it right now (since it's an error msg anyway...), so I commented it out...
%     fclose(fidtxt);
end

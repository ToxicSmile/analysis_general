%artefact/bad channel rejection
%rh, 08.23.07, fp 11.11.11

clear all; close all;


%filepath='E:\User_Sara\AmpServer\SWS_TMS\Pilots\001_FW\session2\';
%vispath='E:\User_Sara\AmpServer\SWS_TMS\Pilots\001_FW\session2\';
%visname= '001_FW_2';
%filename='01_09_ST_001FW2201407292201';
%filenameimp='ST_001FW2_m201407300703';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
currentfolder=cd;
Folderpath='D:\SPN\SPN_022\';


directory = uigetdir(Folderpath,'select Folder containing the long .mat File');
cd(directory)
[file_name] = uigetfile('*.mat', 'Select the long .mat File');
filename=file_name(1:end-4);

[file_prn] = uigetfile('*.prn', 'Select the Imp File');
filenameimp = file_prn(1:end-4);
[visname]=uigetfile('*.vis', 'Select the vis file');



vispath=[directory,'\'];
filepath=[directory,'\'];
cd(currentfolder);


nch=128;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load([filepath,filename,'.mat']);

VisName=[vispath,visname];

offset=0;
[vistrack,vissymb,offset]=readtrac(VisName,1);
%vissymb=vissymb(1:900);                %%Einschränkung auf erste 5h (1h=180)
%visstrack=vistrack(1:900,:);
VisNum=numvis(vissymb,offset);
maxep=length(VisNum);

visgood=find(sum(vistrack')==0);
vissleep=find(vissymb=='1' | vissymb=='2' | vissymb=='3' | vissymb=='4')';

Index_s=intersect(visgood,vissleep);

imp=load([filepath,filenameimp,'.prn']);

%%%%artefact/bad channel rejection
    
    %%%Zwischenspeicherung der Artefaktkorrektur
    save_ndx=[25 50 75 100 125];
    % eval(['load ',filepath,filename,'.mat artndxn -append']);

   
artndxn=zeros(nch,maxep);
for channel=1:nch
    channelimp=num2str(imp(channel,2));
    %channelimp='ok';
    channelstr=num2str(channel);
    fft=squeeze(ffttot(channel,:,:));
    [Index1,Aborted,Upper1,Factor1,NBins1]=exclude(0.75,4.5,fft,Index_s,12,15,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);   %0:plot/prompting, 2:nothing
    [Index2,Aborted,Upper2,Factor2,NBins2]=exclude(20,30,fft,Index_s,5,25,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);
    index=intersect(Index1,Index2);
    artndxn(channel,index)=1;
    if ~isempty(intersect(save_ndx,channel))
        %eval(['save ',filepath,filename,'.mat artndxn -append']);
        save([filepath,'artndx.mat'],'artndxn')
    end
end


% artndxn_ref=artndxn;
% chndx=[49,56];
% ffttot=shiftdim(ffttot,2);
% for channel=1:length(chndx) %nch
%    
%     channelimp=num2str(imp(chndx(channel),2));
%     channelstr=num2str(chndx(channel));
%     fft=squeeze(ffttot(chndx(channel),:,:));
%     [Index1,Aborted,Upper1,Factor1,NBins1]=Exclude(0.75,4.5,fft,Index_s,12,15,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);   %0:plot/prompting, 2:nothing
%     [Index2,Aborted,Upper2,Factor2,NBins2]=Exclude(20,30,fft,Index_s,5,25,maxep,VisNum,['channel ',channelstr,'  imp=',channelimp],0);
%     index=intersect(Index1,Index2);
%     %artndxn(channel,index)=1;
%     artndxn_ref(chndx(channel),index)=1;
%     if ~isempty(intersect(save_ndx,chndx(channel)))
%         eval(['save ',filepath,filename,'.mat artndxn -append']);
%     end
% end    
  


%eval(['save ',filepath,filename,'.mat nch artndxn imp vissymb -append']);

save([filepath,filename,'.mat'], 'artndxn','nch','vissymb', '-append');
%eval(['save ',filepath,filename,'.mat nch artndxn vissymb -append']);
%eval(['save ',filepath,filename,'.mat nch artndxn_ref vissymb -append']);


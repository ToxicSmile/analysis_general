% rasterplot of x, sampling frequency f (1/hours), period d (hours), limits for scale
% 9.7.00, pa; nach vorlage hpk
function r=doubleplot(name,x,f,d,lim)
if nargin < 4
	d = 24;
end
if nargin < 3
	f = 1;
end
z=zeros(1,d.*f);
r1=reshape(x(1:floor(length(x)/(d.*f))*d.*f),d.*f,floor(length(x)/(d.*f)))';
r2=r1;
r1=[z; r1];
r2=[r2; z];
r=[r1 r2];
size(r)
floor(length(x)/(d*f))
if nargin < 5
   imagesc([0.5 48.5],[-1 floor(length(x)/(d*f))-1],r);
else
   
   imagesc([0.5 48.5],[-1 floor(length(x)/(d*f))-1],r,lim);
end
title(name,'Fontsize',12);
colormap(gray);
%grid;
set(gca,'XTickLabels','','XGrid','on');
t=[0 23 47];
axeh=gca;
set(axeh,'XTick',t/48*49+0.5);
set(axeh,'XTickLabels',t+1);
set(axeh,'Fontsize',12);

% NUMVISR
%-----------------------------------------------------------------------------
%
% Converts VisName obtained with READTRAC into a numeric array: nREM sleep stages -1,
% REM Sleep -2 Wake 0 Movement 0 others 3
%
%-----------------------------------------------------------------------------
function [VisNum] = NUMVIS(VisSymb,Offset);

VisNum=zeros(1,size(VisSymb,2)-Offset);
VisNum=VisNum+3;
Index=find(VisSymb=='1')-Offset;
VisNum(Index)=-1;
Index=find(VisSymb=='2')-Offset;
VisNum(Index)=-1;
Index=find(VisSymb=='3')-Offset;
VisNum(Index)=-1;
Index=find(VisSymb=='4')-Offset;
VisNum(Index)=-1;
Index=find(VisSymb=='r')-Offset;
VisNum(Index)=-2;
Index=find(VisSymb=='0')-Offset;
VisNum(Index)=0;
Index=find(VisSymb=='m')-Offset;
VisNum(Index)=0;

fname(1,:)='rsdb101';
fname(2,:)='rsdb102';
fname(3,:)='rsdb103';
fname(4,:)='rsdb105';
fname(5,:)='rsdb106';
fname(6,:)='rsdb107';
fname(7,:)='rsdb108';
fname(8,:)='rsdb109';
fname(9,:)='rsdb110';
fname(10,:)='rsdb111';
fname(11,:)='rsdbx12';
fname(12,:)='rsdb114';

fname(13,:)='rsdb201';
fname(14,:)='rsdb202';
fname(15,:)='rsdb203';
fname(16,:)='rsdb205';
fname(17,:)='rsdb206';
fname(18,:)='rsdb207';
fname(19,:)='rsdb208';
fname(20,:)='rsdb209';
fname(21,:)='rsdb210';
fname(22,:)='rsdb211';
fname(23,:)='rsdb212';
fname(24,:)='rsdb214';

fname(25,:)='rsde201';
fname(26,:)='rsde202';
fname(27,:)='rsde203';
fname(28,:)='rsde205';
fname(29,:)='rsde206';
fname(30,:)='rsde207';
fname(31,:)='rsde208';
fname(32,:)='rsde209';
fname(33,:)='rsde210';
fname(34,:)='rsde211';
fname(35,:)='rsde212';
fname(36,:)='rsde214';

fname(37,:)='rsdr101';
fname(38,:)='rsdr102';
fname(39,:)='rsdr103';
fname(40,:)='rsdr105';
fname(41,:)='rsdr106';
fname(42,:)='rsdr107';
fname(43,:)='rsdr108';
fname(44,:)='rsdr109';
fname(45,:)='rsdr110';
fname(46,:)='rsdr111';
fname(47,:)='rsdr112';
fname(48,:)='rsdr114';

fname(49,:)='rsdr201';
fname(50,:)='rsdr202';
fname(51,:)='rsdr203';
fname(52,:)='rsdr205';
fname(53,:)='rsdr206';
fname(54,:)='rsdr207';
fname(55,:)='rsdr208';
fname(56,:)='rsdr209';
fname(57,:)='rsdr210';
fname(58,:)='rsdr211';
fname(59,:)='rsdr212';
fname(60,:)='rsdr214';


ATOPER=zeros(60,101); ATOPER(:,:)=NaN;


for i=1:60
   % parameter
nnrp=7;
nrp=3;
EMGa=zeros(1,1440); EMG(:,:)=NaN;
rpath='p:\rsd\matlab\data\';
trk=1; 									% track artefakte im vis-file
fpath='p:\rsd\matlab\data\';
cpath='p:\rsd\ana\';
fext='.cyc';
atopath='p:\rsd\matlab\data\';

eval(['load ',atopath,fname(i,:),'.ato -mat']) %mit diesem Befehl holt man index_low wieder!!!
EMGa=zeros(1,1440);
EMGa(index_low)=1;

% parameter
spath=['p:\rsd\reg\vp',fname(i,6:7),'\'];
VisName=[spath,fname(i,:),'.vis'];

% stadien und artefakte lesen
offset=getoffs(VisName);
[vistrack vissymb]=readtrac(VisName,trk);
VisNum=numvis(vissymb,offset);
maxep=length(VisNum);

%cycfile lesen
cyclefile=[cpath,fname(i,:),'.cyc'];
cinf=cycle(cyclefile,vissymb,offset);
ncyc=size(cinf,1);


selnr=find(VisNum<-1); %alle NR-Epochen
selr=find(VisNum==0);

nrpc=zeros(1,ncyc);
npc=zeros(1,ncyc);
pwpc=zeros(1,10*(nnrp+nrp)+1);  %10 anstatt das ncyc;
%wapc=zeros(1,ncyc*(nnrp+nrp)+1);
eppc=zeros(1,10*(nnrp+nrp)+1);  %10 anstatt das ncyc;

% sleep onset latency
pc=1;                                    % fortlaufender percentilen index
sopc=cinf(1,1);
sel=intersect(selnr,1:1:cinf(1,1)-1);    % nur s1
pwpc(pc)=nanmean(EMGa(sel));
eppc(pc)=size(sel,1);
%wapc(pc)=length(find(VisNum(1:cinf(1,1)-1)==1));

for c=1:ncyc
   if c<=size(cinf,1)
	  % nonREMS
  	nrpc(c)=(cinf(c,2)-cinf(c,1)+1)/nnrp;  % percentilenbreite
	  int1=cinf(c,1);                        % anfang nonrem episode
  	for perc=1:nnrp                        % mittel pro percentile
	    int2=cinf(c,1)+round(perc*nrpc(c))-1;
  	  pc=pc+1;
    	sel=intersect(selnr,int1:1:int2);
      pwpc(pc)=nansum(EMGa(sel));
      eppc(pc)=size(sel,2);
      %wapc(pc)=length(find(VisNum(int1:int2)==1));
  	  int1=int2+1;
	  end;

	  % REMS
  	rpc(c)=(cinf(c,4)-cinf(c,3)+1)/nrp;    % percentilenbreite
	  int1=cinf(c,3); 											 % anfang rem episode
  	for perc=1:nrp                         % mittel pro percentile
	    int2=cinf(c,3)+round(perc*rpc(c))-1;
  	  pc=pc+1;
    	sel=intersect(selr,int1:1:int2);
	    pwpc(pc)=nansum(EMGa(sel));
       eppc(pc)=size(sel,2);
       %wapc(pc)=length(find(VisNum(int1:int2)==1));
  	  int1=int2+1;
    end;
  else
    nrpc(c)=NaN;
  	for perc=1:nnrp                        % mittel pro percentile
  	  pc=pc+1;
	    pwpc(pc)=NaN;
       eppc(pc)=NaN;
       wapc(pc)=NaN;
	  end;
  	rpc(c)=NaN;
  	for perc=1:nrp                         % mittel pro percentile
  	  pc=pc+1;
	    pwpc(pc)=NaN;
       eppc(pc)=NaN;
	    wapc(pc)=NaN;
    end;
  end
end;
atpc=(pwpc./eppc)*100;
s=size(atpc,2)
ATOPER(i,1:s)=atpc;
end;

ATOPERC1=ATOPER(:,1:11);
atoperr=ATOPER(:,12:101)';
ATOPERr=zeros(540,10)';
ATOPERr(:)=atoperr;
ATOPERr2=ATOPERr';



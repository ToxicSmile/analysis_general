function binary=dec2binlf(decimal);

% DEC2BIN returns a vector with the binary  notation  of  
%         the given integer decimal  number.  The  first
%         vector element is the lowest bit.  A  negative
%         integer is made into positive.
%________________________________________________________
% Luca Finelli - 02.05.1997

% Last Update:   00.00.0000 lf - changes ...


if decimal==0
  nobit=1;
else
  nobit=1+floor(log(round(decimal))/log(2));
end;
binary=zeros(1,nobit);

if sign(decimal)==-1
  fprintf('DEC2BIN: warning, %d is a negative number.\n',decimal);
end

if isint(decimal)~=1
  fprintf('DEC2BIN: cannot put %d in binary form.\n',decimal);
else
  for i=nobit-1:-1:0 
    if abs(decimal)>=2^(i)
      binary(i+1)=1;
      decimal=decimal-2^(i)*sign(decimal);
    end;
  end;      
end;

binary=binary(length(binary):-1:1);

program convert;
var
	f: text;
	g: file;
	i: integer;
	c: string;
	out : array[1..2] of integer;
begin
	if paramcount<2 then halt;
	assign(f,paramstr(1));
	assign(g,paramstr(2));
	reset(f);
	rewrite(g,1);
	for i:=1 to 7 do
	begin
		readln(f,c);
	end;
	while not EOF(f) do
	begin
		read(f,i);
		readln(f,c);
		out[1]:=i;
		if length(c)>0 then
			out[2]:=1
		else
			out[2]:=0;
		blockwrite(g,out,sizeof(out));
	end;
	close(f);
	close(g);
end.
% p=UTTest(X,Y)
%
% Calculates t-Test (equal variances) and returns probability.
%
% X and Y should have equal number of rows. Columns are observations, rows are variables
%
% Thomas 6/29/98

function p=UTTest(X,Y)

N1=size(X,1);
N2=size(Y,1);

% fixed an error 5/10/2001: was N1+N1-2 instead of N1+N2-2
SD=sqrt((var(X).*(N1-1)+var(Y).*(N2-1))./(N1+N2-2).*(1/N1+1/N2));
nu=N1+N2-2;

T=(mean(X)-mean(Y))./SD;

% changed calculation 5/10/2001
%T=nu./(nu+T.^2);
%p=BETAINC(T,nu/2,0.5);

p=1-tcdf(T,nu);
p=2*min(p,1-p);
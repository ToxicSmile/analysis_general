%scoring compliance

clear all; close all;

%CREATE VisNum Person1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
visnameOrg='SWAP_KP6_2';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VisNameOrg=['D:\Programm_und_beispiele\JM\',visnameOrg,'.VIS'];

offset=0;
track=1;
[vistrackNT,vissymbNT,offset]=readtrac(VisNameOrg,track);

VisNumNT=numvis(vissymbNT,offset);

%CREATE VisNum Person2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
visnameRH='SWAP_KP6_2_rh';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VisNameRH=['D:\Programm_und_beispiele\JM\',visnameRH,'.VIS'];

offset=0;
track=1;
[vistrackRA,vissymbRA,offset]=readtrac(VisNameRH,track);

VisNuNTA=numvis(vissymbRA,offset);

%CREATE VisNum SANJA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% visname='presc_032_DC_2nd';
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% 
% VisNameSB=['C:\',visname,'.VIS'];
% 
% offset=0;
% track=1;
% [vistrackSB,vissymbSB,offset]=readtrac(VisNameSB,track);
% 
% VisNumSB=numvis(vissymbSB,offset);
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%EVALUATE COMPLIANCE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%general compliance
compliance= find((VisNumNT-VisNuNTA)~=0);

compliancefactor=(1-length(compliance)/length(VisNumNT))*100; 

%%%%compliance rem
remNT=find(vissymbNT=='r');
reNTA=find(vissymbRA=='r');
rem_compliance=(1-(abs(1-(length(find(vissymbNT=='r'))/length(find(vissymbRA=='r'))))))*100;


%%%%compliance nrem
nremNT=find(vissymbNT=='1' | vissymbNT=='2' | vissymbNT=='3' | vissymbNT=='4');
nreNTA=find(vissymbRA=='1' | vissymbRA=='2' | vissymbRA=='3' | vissymbRA=='4');
nrem_compliance=(1-(abs(1-(length(find(vissymbNT=='1' | vissymbNT=='2' | vissymbNT=='3' | vissymbNT=='4'))/length(find(vissymbRA=='1' | vissymbRA=='2' | vissymbRA=='3' | vissymbRA=='4'))))))*100;

%%%%compliance sleeponset
sleeponsetNT=find(VisNumNT==-2);
sleeponsetNT=sleeponsetNT(1);
sleeponsetRA=find(VisNuNTA==-2);
sleeponsetRA=sleeponsetRA(1);


%%%%figure

%compliance=[1:10 33:50 500:502 900:1000]

VisNumNTcomp=NaN(1,length(VisNumNT));
VisNumNTcomp(compliance)=VisNumNT(compliance);

VisNuNTAcomp=NaN(1,length(VisNuNTA));
VisNuNTAcomp(compliance)=VisNuNTA(compliance);

density=10;
[VisNT]=plotvis(VisNumNT,density);

[Viscomp]=plotvis(VisNumNTcomp,density);

[VisRA]=plotvis(VisNuNTA,density);

[Viscomp]=plotvis(VisNuNTAcomp,density);

figure
subplot(311)
    plot(VisNT(:,1),VisNT(:,2))
    grid
    title(['comp = ',num2str(compliancefactor),'     nrem comp = ',num2str(nrem_compliance),'     ','rem comp = ',num2str(rem_compliance),'     ',date])
    ylabel('Original')
    yaxis([-3 2])
subplot(312)
    plot(VisRA(:,1),VisRA(:,2))
    grid
    ylabel('Reto')
    yaxis([-3 2])
subplot(313)
    plot(VisNT(:,1),VisNT(:,2))
    hold on
    plot(Viscomp(:,1),Viscomp(:,2),'r')
    grid
    yaxis([-3 2])
    xlabel('hours')
    ylabel('compliance')
    

    
%% Load data

%For flip

clear all
close all

filename='ContrSWE_relabsRatioDiff_base_stim_05-Apr-2018_SWE_1_45Hz_all_N=8.mat';
filenamePVT = 'SWE_1_8__1_4_5Hz_ready_for_corr.mat';
path='L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Niklas_Analysis\SWE\group\';
pathPVT = 'L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Niklas_Analysis\SWE\';

% age=xlsread('AgeSWA.xlsx');
% age([3,10],:)=[];
% cd('L:\Risk\')
% Risk=xlsread('RiskPremium.xlsx');
% Risk_BL1=Risk(:,2);
% Risk_SR1=Risk(:,6);
% Risk_SR1Incr=Risk_SR1-Risk_BL1;

cd(path)
load (filename,'relpower')
cd(pathPVT)
load (filenamePVT, 'contrvar1')
pvt = contrvar1;

% For SWE diff
filenameSWE = 'RelabsRatioDiff_base_stim_SWA_1_45Hz_all_04-Apr-2018_N=8.mat';
pathSWE = 'L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Niklas_Analysis\group\';
cd(pathSWE)
load (filenameSWE, 'relpower_diff_all_basestim');

%% Flip

for s=1:8
RelpowNREMbase(s,:)=relpower(s).base;
RelpowNREMstim(s,:)=relpower(s).stim;
end

%RelpowNREMbase([1,8],:)=[];
%RelpowNREMstim([1,8],:)=[];

load L:\Somnus-Data\User\Niklas_Schneider\Matlab\Preprocessing\Angelina\flip.prn -ascii

FlipRelpowNREMbase=RelpowNREMbase(:,flip);
FlipRelpowNREMstim=RelpowNREMstim(:,flip);
for s=1:size(RelpowNREMbase,1)
ai_BL(s)=nansum(abs(RelpowNREMbase(s,:)./FlipRelpowNREMbase(s,:)-1));
ai_SR(s)=nansum(abs(RelpowNREMstim(s,:)./FlipRelpowNREMstim(s,:)-1));
end

ai_ch=ai_BL-ai_SR;
electrodes=(1:128)';
[p_BL,T_BL]=pttest(FlipRelpowNREMbase,RelpowNREMbase);
[p_SR,T_SR]=pttest(FlipRelpowNREMstim,RelpowNREMstim);
AS_BL=(RelpowNREMbase-FlipRelpowNREMbase);
AS_SR=(RelpowNREMstim-FlipRelpowNREMstim);
AS_Change=AS_BL-AS_SR;
%data(1,1,:)=nanmean(AS_BL);
data(1,1,:)=nanmean(AS_SR);
%data(1,3,:)=nanmean(AS_Change);
%data_sig(1,1,:)=p_BL;
data_sig(1,1,:)=p_SR;
%data_sig(1,3,:)=ones(1,128);

% % data_sig(1,2,:)=p_BL;
% for i=1:8
% data_sig(1,i,:)=p_SR;
% end
% 
% for i=1:8
% data_sig(2,i,:)=1;    
% end
% data.flip = AS_SR;
% data.SWEdiff_relpower = relpower_diff_all_basestim;
% data(1,1,:)=AS_SR(1,:);
% data(1,2,:)=AS_SR(2,:);
% data(1,3,:)=AS_SR(3,:);
% data(1,4,:)=AS_SR(4,:);
% data(1,5,:)=AS_SR(5,:);
% data(1,6,:)=AS_SR(6,:);
% data(1,7,:)=AS_SR(7,:);
% data(1,8,:)=AS_SR(8,:);

% data(2,1,:)=relpower_diff_all_basestim(1,:);
% data(2,2,:)=relpower_diff_all_basestim(2,:);
% data(2,3,:)=relpower_diff_all_basestim(3,:);
% data(2,4,:)=relpower_diff_all_basestim(4,:);
% data(2,5,:)=relpower_diff_all_basestim(5,:);
% data(2,6,:)=relpower_diff_all_basestim(6,:);
% data(2,7,:)=relpower_diff_all_basestim(7,:);
% data(2,8,:)=relpower_diff_all_basestim(8,:);

%% Correlations
% Pearson:

for i=1:128
% [p(i), R(i), crit_corr(i,:), est_alpha(i), seed_state(i)]=mult_comp_perm_corr(SWA1(:,i),contrvar2a,5000);
    [R(i),p(i)]=corr(AS_BL(:,i),contrvar1,'type','Pearson');
end

%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,2,:)=R;
data_sig(1,2,:)=p;
clear R p

for i=1:128
% [p(i), R(i), crit_corr(i,:), est_alpha(i), seed_state(i)]=mult_comp_perm_corr(SWA1(:,i),contrvar2a,5000);
    [R(i),p(i)]=corr(AS_SR(:,i),contrvar1,'type','Pearson');
end


%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,3,:)=R;
data_sig(1,3,:)=p;
clear R p


for i=1:128
% [p(i), R(i), crit_corr(i,:), est_alpha(i), seed_state(i)]=mult_comp_perm_corr(SWA1(:,i),contrvar2a,5000);
    [R(i),p(i)]=corr(AS_Change(:,i),contrvar1,'type','Pearson');
end


%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,4,:)=R;
data_sig(1,4,:)=p;
clear R p

% Figures

noc=4;
nor=1;
titlerows1={'Mean all subJ RelpowNREMstim-FlipRelpowNREMstim';'ASBL PVT Pearson';'ASSR PVT Pearson'; 'ASBL-ASSR PVT Pearson'};
titlerows2={'corr(AS_SR(:,i),contrvar1';'x age';'x age'};
% titlerows3={'x RiskIncr';'x RiskIncr';'x RiskIncr'}
% titlerows2={'RelBLSWA x RiskBL contrAge';'RelSRSWA x RiskSR contrAge';'RelSD SWA x RiskSD contrAge','RelBLSWA x RiskSR contrAge'}
fig = figure('position',[0 30 150*noc 150*nor]);
% figure
dx=1/noc; %Achsenbreite, x-Richtung
dy=1/nor; %Achsenhöhe, y-Richtung
a=NaN(nor,noc); %initialisierung; handles
ypos=0;%0.68; %0.8y Start-Position

for row=1:nor
xpos=0; %0x Start-Position
for col=1:noc
a(row,col)=axes('position',[xpos ypos dx dy],'visible','off');
b(row,col)=axes('position',[xpos+0.1 ypos dx dy],'visible','off');
xpos=xpos+dx;
end
ypos=ypos-dy;
end

for col=1:noc
for row=1:nor
%i=i+1
axes(a(row,col)); %select axes
%Zufallszahlen mit topoplot
% form anpassen: randn(128,1)
data_p=data(row,col,:);
data_mark=data_sig(row,col,:);
matnet128AM(isfinite(data_p))
data_p2=isfinite(data_p);
topoplottest3(data_p(isfinite(data_p)),[cd,'\test128.loc'],'maplimits','minmax','conv','on','intrad',0.5,'colormap','jet','style','both','electrodes','on','emarkersize',5,...
'gridscale',200,'emarker2',{[(find(data_mark(isfinite(data_mark))<0.05))],'o','w'});
colorbar
% caxis([-1,1])
text(0.4,0.54,[num2str((max(data_p(isfinite(data_p)))))],'Color',[1 0 0],'FontSize',7.5);
text(0.4,0.46,[num2str((min(data_p(isfinite(data_p)))))],'Color',[0 0 1],'FontSize',7.5);
if row==1
title(titlerows1{col})

else
  caxis([-1,1])  
end
end
end


cd('L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Flip\');
outname3=['Flip_ASBL-ASSR_Pearson'];
set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
img = getframe(gcf);
imwrite(img.cdata, [outname3, '.png']);

close all;

%% Correlations
% Spearman:

for i=1:128
% [p(i), R(i), crit_corr(i,:), est_alpha(i), seed_state(i)]=mult_comp_perm_corr(SWA1(:,i),contrvar2a,5000);
    [R(i),p(i)]=corr(AS_BL(:,i),contrvar1,'type','Spearman');
end

%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,2,:)=R;
data_sig(1,2,:)=p;
clear R p

for i=1:128
% [p(i), R(i), crit_corr(i,:), est_alpha(i), seed_state(i)]=mult_comp_perm_corr(SWA1(:,i),contrvar2a,5000);
    [R(i),p(i)]=corr(AS_SR(:,i),contrvar1,'type','Spearman');
end


%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,3,:)=R;
data_sig(1,3,:)=p;
clear R p


for i=1:128
% [p(i), R(i), crit_corr(i,:), est_alpha(i), seed_state(i)]=mult_comp_perm_corr(SWA1(:,i),contrvar2a,5000);
    [R(i),p(i)]=corr(AS_Change(:,i),contrvar1,'type','Spearman');
end


%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,4,:)=R;
data_sig(1,4,:)=p;
clear R p

% Figures

noc=4;
nor=1;
titlerows1={'Mean all subJ RelpowNREMstim-FlipRelpowNREMstim';'ASBL PVT Spearman';'ASSR PVT Spearman'; 'ASBL-ASSR PVT Spearman'};
titlerows2={'corr(AS_SR(:,i),contrvar1';'x age';'x age'};
% titlerows3={'x RiskIncr';'x RiskIncr';'x RiskIncr'}
% titlerows2={'RelBLSWA x RiskBL contrAge';'RelSRSWA x RiskSR contrAge';'RelSD SWA x RiskSD contrAge','RelBLSWA x RiskSR contrAge'}
fig = figure('position',[0 30 150*noc 150*nor]);
% figure
dx=1/noc; %Achsenbreite, x-Richtung
dy=1/nor; %Achsenhöhe, y-Richtung
a=NaN(nor,noc); %initialisierung; handles
ypos=0;%0.68; %0.8y Start-Position

for row=1:nor
xpos=0; %0x Start-Position
for col=1:noc
a(row,col)=axes('position',[xpos ypos dx dy],'visible','off');
b(row,col)=axes('position',[xpos+0.1 ypos dx dy],'visible','off');
xpos=xpos+dx;
end
ypos=ypos-dy;
end

for col=1:noc
for row=1:nor
%i=i+1
axes(a(row,col)); %select axes
%Zufallszahlen mit topoplot
% form anpassen: randn(128,1)
data_p=data(row,col,:);
data_mark=data_sig(row,col,:);
matnet128AM(isfinite(data_p))
data_p2=isfinite(data_p);
topoplottest3(data_p(isfinite(data_p)),[cd,'\test128.loc'],'maplimits','minmax','conv','on','intrad',0.5,'colormap','jet','style','both','electrodes','on','emarkersize',5,...
'gridscale',200,'emarker2',{[(find(data_mark(isfinite(data_mark))<0.05))],'o','w'});
colorbar
% caxis([-1,1])
text(0.4,0.54,[num2str((max(data_p(isfinite(data_p)))))],'Color',[1 0 0],'FontSize',7.5);
text(0.4,0.46,[num2str((min(data_p(isfinite(data_p)))))],'Color',[0 0 1],'FontSize',7.5);
if row==1
title(titlerows1{col})

else
  caxis([-1,1])  
end
end
end


cd('L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Flip\');
outname3=['Flip_ASBL-ASSR_Spearman'];
set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
img = getframe(gcf);
imwrite(img.cdata, [outname3, '.png']);

close all;

%% Wilcoxon signed rank
AS_BL_wilc = AS_BL;
AS_BL_wilc(isnan(AS_BL_wilc))=0; 
 
clear h p
for i=1:128
    [p(i), h(i)] = signrank(AS_BL_wilc(:,i));
end

%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,2,:)=h;
data_sig(1,2,:)=p;
clear h p

for i=1:128
    [p(i),h(i)] = signrank(AS_SR(:,i));
end


%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,3,:)=h;
data_sig(1,3,:)=p;
clear h p


for i=1:128
    [p(i),h(i)] = signrank(AS_Change(:,i));
end


%p(excludedchannels)=NaN;
%R(excludedchannels)=NaN;
data(1,4,:)=h;
data_sig(1,4,:)=p;
clear h p

%% Figures

noc=4;
nor=1;
titlerows1={'Mean all subJ RelpowNREMstim-FlipRelpowNREMstim';'ASBL PVT Wilc';'ASSR PVT Wilc'; 'ASBL-ASSR PVT Wilc'};
titlerows2={'corr(AS_SR(:,i),contrvar1';'x age';'x age'};
% titlerows3={'x RiskIncr';'x RiskIncr';'x RiskIncr'}
% titlerows2={'RelBLSWA x RiskBL contrAge';'RelSRSWA x RiskSR contrAge';'RelSD SWA x RiskSD contrAge','RelBLSWA x RiskSR contrAge'}
fig = figure('position',[0 30 150*noc 150*nor]);
% figure
dx=1/noc; %Achsenbreite, x-Richtung
dy=1/nor; %Achsenhöhe, y-Richtung
a=NaN(nor,noc); %initialisierung; handles
ypos=0;%0.68; %0.8y Start-Position

for row=1:nor
xpos=0; %0x Start-Position
for col=1:noc
a(row,col)=axes('position',[xpos ypos dx dy],'visible','off');
b(row,col)=axes('position',[xpos+0.1 ypos dx dy],'visible','off');
xpos=xpos+dx;
end
ypos=ypos-dy;
end

for col=1:noc
for row=1:nor
%i=i+1
axes(a(row,col)); %select axes
%Zufallszahlen mit topoplot
% form anpassen: randn(128,1)
data_p=data(row,col,:);
data_mark=data_sig(row,col,:);
matnet128AM(isfinite(data_p))
data_p2=isfinite(data_p);
topoplottest3(data_p(isfinite(data_p)),[cd,'\test128.loc'],'maplimits','minmax','conv','on','intrad',0.5,'colormap','jet','style','both','electrodes','on','emarkersize',5,...
'gridscale',200,'emarker2',{[(find(data_mark(isfinite(data_mark))<0.05))],'o','w'});
colorbar
% caxis([-1,1])
text(0.4,0.54,[num2str((max(data_p(isfinite(data_p)))))],'Color',[1 0 0],'FontSize',7.5);
text(0.4,0.46,[num2str((min(data_p(isfinite(data_p)))))],'Color',[0 0 1],'FontSize',7.5);
if row==1
title(titlerows1{col})

else
  caxis([-1,1])  
end
end
end


cd('L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Flip\');
outname3=['Flip_ASBL-ASSR_Signed_rank'];
set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
img = getframe(gcf);
imwrite(img.cdata, [outname3, '.png']);

close all;

clear all
close all

filename='ContrSWE_relabsRatioDiff_base_stim_05-Apr-2018_SWE_1_45Hz_all_N=8.mat';
filenamePVT = 'SWE_1_8__1_4_5Hz_ready_for_corr.mat';
path='L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Niklas_Analysis\SWE\group\';
pathPVT = 'L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Niklas_Analysis\SWE\';

% age=xlsread('AgeSWA.xlsx');
% age([3,10],:)=[];
% cd('L:\Risk\')
% Risk=xlsread('RiskPremium.xlsx');
% Risk_BL1=Risk(:,2);
% Risk_SR1=Risk(:,6);
% Risk_SR1Incr=Risk_SR1-Risk_BL1;

cd(path)
load (filename,'relpower')
cd(pathPVT)
load (filenamePVT, 'contrvar1')
pvt = contrvar1;
%%

for s=1:8
RelpowNREMbase(s,:)=relpower(s).base;
RelpowNREMstim(s,:)=relpower(s).stim;
end

%RelpowNREMbase([1,8],:)=[];
%RelpowNREMstim([1,8],:)=[];

load L:\Somnus-Data\User\Niklas_Schneider\Matlab\Preprocessing\Angelina\flip.prn -ascii

FlipRelpowNREMbase=RelpowNREMbase(:,flip);
FlipRelpowNREMstim=RelpowNREMstim(:,flip);
for s=1:size(RelpowNREMbase,1)
ai_BL(s)=nansum(abs(RelpowNREMbase(s,:)./FlipRelpowNREMbase(s,:)-1));
ai_SR(s)=nansum(abs(RelpowNREMstim(s,:)./FlipRelpowNREMstim(s,:)-1));
end


ai_ch=ai_BL-ai_SR;
electrodes=(1:128)';
[p_BL,T_BL]=pttest(FlipRelpowNREMbase,RelpowNREMbase);
[p_SR,T_SR]=pttest(FlipRelpowNREMstim,RelpowNREMstim);
AS_BL=(RelpowNREMbase-FlipRelpowNREMbase);
AS_SR=(RelpowNREMstim-FlipRelpowNREMstim);
AS_Change=AS_BL-AS_SR;
data(1,1,:)=nanmean(AS_BL);
data(1,2,:)=nanmean(AS_SR);
data(1,3,:)=nanmean(AS_Change);
data_sig(1,1,:)=p_BL;
data_sig(1,2,:)=p_SR;
data_sig(1,3,:)=ones(1,128);

% Corr of Asymmetry with age
for i=1:128
[R(i),p(i)]=corr(pvt,AS_BL(:,i));
end
data(2,1,:)=R;
data_sig(2,1,:)=p;
clear R p

% SR
for i=1:128
[R(i),p(i)]=corr(pvt,AS_SR(:,i));
end
data(2,2,:)=R;
data_sig(2,2,:)=p;
clear R p

% Change
for i=1:128
[R(i),p(i)]=corr(pvt,AS_Change(:,i));
end
data(2,3,:)=R;
data_sig(2,3,:)=p;
clear R p

%% 
% % Corr of Asymmetry with change in Behavior Risk_SR1Incr
% for i=1:128
% [R(i),p(i)]=corr(Risk_SR1Incr,AS_BL(:,i));
% end
% data(3,1,:)=R;
% data_sig(3,1,:)=p;
% clear R p
% 
% % SR
% for i=1:128
% [R(i),p(i)]=corr(Risk_SR1Incr,AS_SR(:,i));
% end
% data(3,2,:)=R;
% data_sig(3,2,:)=p;
% clear R p
% 
% % Change
% for i=1:128
% [R(i),p(i)]=corr(Risk_SR1Incr,AS_Change(:,i));
% end
% data(3,3,:)=R;
% data_sig(3,3,:)=p;
% clear R p
% excludedele=[43 48 49 56 63 68 73 81 88 94 99 107 113 119 120 125 126 127 128];
% for i=1:128
%  if electrodes(i)==flip(i) && ismember(i,excludedele)==0
%      data_sig(:,:,i)=1;
%      data(2:3,:,i)=0;
%  end
% end

%% Figures

noc=3;
nor=2;
titlerows1={'SWE Difference btw sides BL';'SWE Difference btw sides stim';'SWE Difference BL-STIM'};
% titlerows2={'x age';'x age';'x age'}
% titlerows3={'x RiskIncr';'x RiskIncr';'x RiskIncr'}
% titlerows2={'RelBLSWA x RiskBL contrAge';'RelSRSWA x RiskSR contrAge';'RelSD SWA x RiskSD contrAge','RelBLSWA x RiskSR contrAge'}
fig = figure('position',[0 30 150*noc 150*nor]);
% figure
dx=1/noc; %Achsenbreite, x-Richtung
dy=1/noc; %Achsenhöhe, y-Richtung
a=NaN(nor,noc); %initialisierung; handles
ypos=0.6;%0.68; %0.8y Start-Position

for row=1:nor
xpos=0; %0x Start-Position
for col=1:noc
a(row,col)=axes('position',[xpos ypos dx dy],'visible','off');
b(row,col)=axes('position',[xpos+0.1 ypos dx dy],'visible','off');
xpos=xpos+dx;
end
ypos=ypos-dy;
end

for col=1:noc
for row=1:nor
%i=i+1
axes(a(row,col)); %select axes
%Zufallszahlen mit topoplot
% form anpassen: randn(128,1)
data_p=data(row,col,:);
data_mark=data_sig(row,col,:);
matnet128AM(isfinite(data_p))
data_p2=isfinite(data_p);
topoplottest3(data_p(isfinite(data_p)),[cd,'\test128.loc'],'maplimits','minmax','conv','on','intrad',0.5,'colormap','jet','style','both','electrodes','on','emarkersize',5,...
'gridscale',200,'emarker2',{[(find(data_mark(isfinite(data_mark))<0.05))],'o','w'});
colorbar
% caxis([-1,1])
text(0.4,0.54,[num2str((max(data_p(isfinite(data_p)))))],'Color',[1 0 0],'FontSize',7.5);
text(0.4,0.46,[num2str((min(data_p(isfinite(data_p)))))],'Color',[0 0 1],'FontSize',7.5);
if row==1
title(titlerows1{col})
else
  caxis([-1,1])  
end
end
end

cd('L:\Somnus-Data\Data01\SWAB(Pilot)\data\Filtered_Data_NS\Sym\');
outname3=['AsymmetrySWE'];
set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
img = getframe(gcf);
imwrite(img.cdata, [outname3, '.png']);

close all;




% 
% % ai_BL(s)=nansum(abs(RelpowNREMbase(s,:)./FlipRelpowNREMbase(s,:)-1));
% % ai_SR(s)=nansum(abs(RelpowNREMstim(s,:)./FlipRelpowNREMstim(s,:)-1));
% 
% [R_BL,p_BL]=corr(Risk_SR1Incr,ai_BL');
% scatter(Risk_SR1Incr,ai_BL,'filled')
% title(['RiskIncrxAI BL R=', num2str(R_BL),'p=',num2str(p_BL)]);
% lsline
% cd('F:\Daten_sleep\ToDoTopo\RiskSWA\');
% outname3=['AsymmetryIndexSWEBLxRiskIncr'];
% set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
% img = getframe(gcf);
% imwrite(img.cdata, [outname3, '.png']);
% 
% close all;
% 
% [R_SR,p_SR]=corr(Risk_SR1Incr,ai_SR');
% scatter(Risk_SR1Incr,ai_SR,'filled')
% title(['RiskIncrxAI SR R=', num2str(R_SR),'p=',num2str(p_SR)]);
% lsline
% cd('F:\Daten_sleep\ToDoTopo\RiskSWA\');
% outname3=['AsymmetryIndexSWESRxRiskIncr'];
% set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
% img = getframe(gcf);
% imwrite(img.cdata, [outname3, '.png']);
% 
% close all;
% 
% [R_ch,p_ch]=corr(Risk_SR1Incr,ai_ch');
% scatter(Risk_SR1Incr,ai_ch,'filled')
% title(['RiskIncrxAI BL-SR R=', num2str(R_SR),'p=',num2str(p_SR)]);
% lsline
% cd('F:\Daten_sleep\ToDoTopo\RiskSWA\');
% outname3=['AsymmetryIndexSWEchangexRiskIncr'];
% set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
% img = getframe(gcf);
% imwrite(img.cdata, [outname3, '.png']);